package de.uniks.networkparser.test;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.IOException;
import java.util.concurrent.CopyOnWriteArrayList;

import org.junit.jupiter.api.Test;

import de.uniks.networkparser.ext.petaf.SimpleExecutor;
import de.uniks.networkparser.ext.promise.Promise;

public class PromiseTest {

  @Test
  public void testPromise() {
    SimpleExecutor executor = new SimpleExecutor();
    Promise<String> promise = Promise.newPromise(executor);

    executor.executeTask(() -> {
      promise.resolve("Hello Welt");
    }, 10);

    assertEquals("Hello Welt", promise.sync(1000, null));
  }

  // @Test
  public void testPromiseStream() {
    SimpleExecutor executor = new SimpleExecutor();
    Promise<String> promise = Promise.newPromise(executor);
    promise.map(value -> (Integer.valueOf(value) + 2)).onSuccess(item -> System.out.println(item));

    executor.executeTask(() -> {
      System.out.println("RUN");
      promise.resolve("42");
    }, 10);


    // executor.executeTask(() -> {
    //// System.out.println("RUN");
    // promise.resolve("Hello Welt");
    // }, 10);

    System.out.println("END");
  }


  @Test
  public void testPromiseExecuteOnManyThread() throws IOException {

    SimpleExecutor executorUI = SimpleExecutor.createSimpleExecutor("UI");
    CopyOnWriteArrayList<String> items = new CopyOnWriteArrayList<>();
    SimpleExecutor executorModel = SimpleExecutor.createSimpleExecutor("Model");
    executorUI.executeTask(() -> items.add("Run on " + Thread.currentThread().getName()), 0);
    executorModel.executeTask(() -> items.add("Run on " + Thread.currentThread().getName()), 0);


    // PromiseExecutor mapper;
    // Promise<Void> promise = mapper.execute(() -> {});
    // promise.onSuccess(() -> items.add("Run on "+Thread.currentThread().getName()))
    // promise.changeFactory(executorModel)
    // promise.then(() -> items.add("Run on "+Thread.currentThread().getName()))

    executorUI.close();
    executorModel.close();

  }
}
