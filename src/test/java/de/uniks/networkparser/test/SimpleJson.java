package de.uniks.networkparser.test;

import org.junit.jupiter.api.Test;

import de.uniks.networkparser.buffer.ByteBuffer;
import de.uniks.networkparser.ext.io.FileBufferImpl;
import de.uniks.networkparser.json.JsonObject;
import de.uniks.networkparser.json.SimpleJsonTokener;

public class SimpleJson {

  @Test
  public void testSimple() {
    ByteBuffer buffer = FileBufferImpl.readBinaryResource("citysmall2.json", SimpleJson.class);
    SimpleJsonTokener tokener = new SimpleJsonTokener();
    JsonObject result = new JsonObject();
    tokener.parsingEntity(result, buffer);
  }
}
