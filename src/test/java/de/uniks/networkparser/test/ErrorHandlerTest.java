package de.uniks.networkparser.test;

import de.uniks.networkparser.ext.ErrorHandler;
import java.awt.AWTException;
import java.awt.HeadlessException;
import java.io.IOException;

public class ErrorHandlerTest {

  public static void main(String[] args) throws HeadlessException, AWTException, IOException {
    ErrorHandler errorHandler = new ErrorHandler();
    errorHandler.withURL("error");

    //System.out.println(errorHandler.getJVMStartUp().toString("dd.mm.yyyy HH:MM:SS"));
    errorHandler.saveException(new RuntimeException("Test"));
    
    //errorHandler.saveErrorFile(null, "error.txt", "build", new RuntimeException("Test"));
    //errorHandler.saveScreenShoot(null, null, null, errorHandler)
  }
}
