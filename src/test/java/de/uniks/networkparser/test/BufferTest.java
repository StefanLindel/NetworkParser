package de.uniks.networkparser.test;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import de.uniks.networkparser.buffer.ByteBuffer;

public class BufferTest {

  @Test
  public void SimpleBufferTestReplace() {
    ByteBuffer byteBuffer = ByteBuffer.allocate(10);
    byteBuffer.put("ABCDE".getBytes());
    byteBuffer.replace(1, 2, "D");
    assertEquals("ADCDE", byteBuffer.asString());
  }


  @Test
  public void SimpleBufferTestReplaceWithReduceLength() {
    ByteBuffer byteBuffer = ByteBuffer.allocate(10);
    byteBuffer.put("ABCDE".getBytes());
    byteBuffer.replace(1, 3, "D");
    assertEquals("ADDE", byteBuffer.asString());
  }

  @Test
  public void SimpleBufferTestReplaceWithLength() {
    ByteBuffer byteBuffer = ByteBuffer.allocate(10);
    byteBuffer.put("ABCDE".getBytes());
    byteBuffer.replace(1, 2, "DE");
    assertEquals("ADECDE", byteBuffer.asString());
  }

  @Test
  public void SimpleBufferTestReplaceWithExpandLength() {
    ByteBuffer byteBuffer = ByteBuffer.allocate(5);
    byteBuffer.put("ABCDE".getBytes());
    byteBuffer.replace(1, 2, "DE");
    assertEquals("ADECDE", byteBuffer.asString());
  }
}
