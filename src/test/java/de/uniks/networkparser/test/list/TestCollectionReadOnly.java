package de.uniks.networkparser.test.list;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import de.uniks.networkparser.interfaces.UnmodifierList;
import de.uniks.networkparser.list.SimpleList;

public class TestCollectionReadOnly {
  private UnmodifierList<String> createCollection() {
    SimpleList<String> list = new SimpleList<String>();
    list.add("Hallo", "Welt");
    list.add("Test");
    return list;
  }

  @Test
  public void testCollection() {
    UnmodifierList<String> collection = createCollection();
    assertEquals(3, collection.size());
  }
}
