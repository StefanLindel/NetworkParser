package de.uniks.networkparser.test;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import de.uniks.networkparser.buffer.CharacterBuffer;
import de.uniks.networkparser.interfaces.ObjectCondition;
import de.uniks.networkparser.logic.Equals;
import de.uniks.networkparser.logic.Or;
import org.junit.jupiter.api.Test;


/**
 * RegTest.
 * 
 * @author Stefan
 */
public class RegTest {
  @Test
  public void testRegEx() {
    String reg = "[abc]";
    CharacterBuffer item = new CharacterBuffer();
    item.with(reg);
    ObjectCondition root = parseCurrentChar(item);
    assertNotNull(root);
  }

  /**
   * parseCondition.
   * 
   * @param item item
   * @return Condition
   */
  public ObjectCondition parseCurrentChar(CharacterBuffer item) {
    char ch = item.getCurrentChar();
    if (ch == '[') {
      // OR-Item
      Or or = new Or();
      item.skip();
      while (ch != ']') {
        or.with(parseCurrentChar(item));
        ch = item.getChar();
      }
      return or;
    }

    return new Equals().withValue("" + ch);
  }
}
