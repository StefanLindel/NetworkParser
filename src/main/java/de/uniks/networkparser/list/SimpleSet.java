package de.uniks.networkparser.list;

/*
 * NetworkParser The MIT License Copyright (c) 2010-2016 Stefan Lindel
 * https://www.github.com/fujaba/NetworkParser/
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

import de.uniks.networkparser.SimpleEvent;
import de.uniks.networkparser.buffer.CharacterBuffer;
import de.uniks.networkparser.interfaces.ObjectCondition;

import java.util.Collection;
import java.util.Set;

/**
 * The Class SimpleSet.
 *
 * @param <V> the value type
 * @author Stefan
 */
public class SimpleSet<V> extends AbstractList<V> implements Set<V> {

    /**
     * The Constant PROPERTY.
     */
    public static final String PROPERTY = "items";
    protected ObjectCondition listener;

    /**
     * Instantiates a new simple set.
     *
     * @param objects the objects
     */
    public SimpleSet(Object... objects) {
        if (objects != null && objects.length > 0) {
            init(objects);
        }
    }

    /**
     * Gets the new list.
     *
     * @param keyValue the key value
     * @return the new list
     */
    @Override
    public SimpleSet<V> getNewList(boolean keyValue) {
        return new SimpleSet<V>();
    }

    /**
     * Removes the by object.
     *
     * @param key the key
     * @return the int
     */
    @Override
    public int removeByObject(Object key) {
        if (isReadOnly() || !isVisible()) {
            throw new UnsupportedOperationException("remove(" + key + ")");
        }
        return super.removeByObject(key);
    }

    /**
     * Retain all.
     *
     * @param c the c
     * @return true, if successful
     */
    @Override
    public boolean retainAll(Collection<?> c) {
        if (isReadOnly() || !isVisible()) {
            throw new UnsupportedOperationException("retainAll(" + c + ")");
        }
        return super.retainAll(c);
    }

    /**
     * Clone.
     *
     * @return the simple set
     */
    @Override
    public SimpleSet<V> clone() {
        return getNewList(false).init(this);
    }

    /**
     * Sub list.
     *
     * @param fromIndex the from index
     * @param toIndex   the to index
     * @return the simple set
     */
    @SuppressWarnings("unchecked")
    public SimpleSet<V> subList(int fromIndex, int toIndex) {
        return (SimpleSet<V>) super.subList(fromIndex, toIndex);
    }

    /**
     * To string.
     *
     * @return the string
     */
    /* Add Methods from SDMLib */
    @Override
    public String toString() {
        CharacterBuffer buffer = new CharacterBuffer();
        buffer.with('(');
        return toBuffer(buffer, ", ").with(')').toString();
    }

    /**
     * To string.
     *
     * @param separator the separator
     * @return the string
     */
    public String toString(String separator) {
        CharacterBuffer buffer = new CharacterBuffer();
        return toBuffer(buffer, separator).toString();
    }

    CharacterBuffer toBuffer(CharacterBuffer buffer, String separator) {
        int len = this.size();
        for (V elem : this) {
            buffer.with(elem.toString());
            if (len > 1) {
                buffer.with(separator);
            }
            len--;
        }
        return buffer;
    }

    /**
     * Sets the.
     *
     * @param index   the index
     * @param element the element
     * @return the v
     */
    /* ReadOnly Add all */
    @Override
    public V set(int index, V element) {
        if (isReadOnly() || !isVisible()) {
            throw new UnsupportedOperationException("set(" + index + ")");
        }
        return super.set(index, element);
    }

    /**
     * Adds the.
     *
     * @param index   the index
     * @param element the element
     */
    @Override
    public void add(int index, V element) {
        if (isReadOnly() || !isVisible()) {
            throw new UnsupportedOperationException("add(" + index + ")");
        }
        super.add(index, element);
    }

    /**
     * Clear.
     */
    @Override
    public void clear() {
        if (isReadOnly() || !isVisible()) {
            throw new UnsupportedOperationException("clear()");
        }
        super.clear();
    }

    /**
     * Removes the.
     *
     * @param index the index
     * @return the v
     */
    @Override
    public V remove(int index) {
        if (isReadOnly() || !isVisible()) {
            throw new UnsupportedOperationException("remove(" + index + ")");
        }
        return super.remove(index);
    }

    /**
     * Adds the.
     *
     * @param newValue the new value
     * @return true, if successful
     */
    @Override
    public boolean add(V newValue) {
        if (isReadOnly() || !isVisible()) {
            throw new UnsupportedOperationException("add()");
        }
        return super.add(newValue);
    }

    /**
     * Union.
     *
     * @param <ST>  the generic type
     * @param other the other
     * @return the st
     */
    @SuppressWarnings("unchecked")
    public <ST extends SimpleSet<V>> ST union(Collection<? extends V> other) {
        ST result = (ST) this.getNewList(false);
        result.addAll(this);
        result.addAll(other);
        return result;
    }

    /**
     * Intersection.
     *
     * @param <ST>  the generic type
     * @param other the other
     * @return the st
     */
    @SuppressWarnings("unchecked")
    public <ST extends SimpleSet<V>> ST intersection(Collection<? extends V> other) {
        ST result = (ST) this.getNewList(false);
        result.addAll(this);
        result.retainAll(other);
        return result;
    }

    /**
     * Instance of.
     *
     * @param <ST>   the generic type
     * @param target the target
     * @return the st
     */
    public <ST extends AbstractList<?>> ST instanceOf(ST target) {
        for (Object obj : this) {
            if (obj != null && obj.getClass() == target.getTypClass()) {
                target.with(obj);
            }
        }
        return target;
    }

    /**
     * Minus.
     *
     * @param <ST>  the generic type
     * @param other the other
     * @return the st
     */
    @SuppressWarnings("unchecked")
    public <ST extends SimpleSet<V>> ST minus(Object other) {
        ST result = (ST) this.getNewList(false);
        result.addAll(this);
        if (other instanceof Collection) {
            result.removeAll((Collection<?>) other);
        } else {
            result.remove(other);
        }
        return result;
    }

    /**
     * With listener.
     *
     * @param listener the listener
     * @return the simple set
     */
    public SimpleSet<V> withListener(ObjectCondition listener) {
        this.listener = listener;
        return this;
    }

    @Override
    protected boolean fireProperty(String type, Object oldElement, Object newElement,
                                   Object beforeElement, int index, Object value) {
        if (this.listener != null) {
            return this.listener.update(new SimpleEvent(type, this, PROPERTY, index, oldElement,
                    newElement, value, beforeElement));
        }
        return super.fireProperty(type, oldElement, newElement, beforeElement, index, value);
    }

    /**
     * With list.
   *
   * @param values the values
   * @return the simple set
   */
  @Override
  public SimpleSet<V> withList(Collection<?> values) {
    super.withList(values);
    return this;
  }
}
