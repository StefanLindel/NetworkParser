package de.uniks.networkparser;

import de.uniks.networkparser.interfaces.ObjectCondition;
import de.uniks.networkparser.interfaces.SendableEntityCreator;

/**
 * DeepCondition.
 *
 * @author Stefan Lindel
 */
public class Deep implements SendableEntityCreator, ObjectCondition {
    /**
     * Constant of Deep.
     */
    public static final String DEPTH = "depth";
    /**
     * Variable of Deep.
     */
    private int value;

    /**
     * Create a new DeepFilter and return a new Instance.
     *
     * @param value Value of depth
     * @return a new depth Instance
     */
    public static Deep create(int value) {
        return new Deep().withDepth(value);
    }

    /**
     * With depth.
     *
     * @param value The new Value
     * @return Deep Instance
     */
    public Deep withDepth(int value) {
        this.value = value;
        return this;
    }

    /**
     * Gets the depth.
     *
     * @return The Current Deep Value
     */
    public int getDepth() {
        return value;
    }

    /**
     * Update.
     *
     * @param evt the evt
     * @return true, if successful
     */
    @Override
    public boolean update(Object evt) {
        if (evt instanceof SimpleEvent) {
            return ((SimpleEvent) evt).getDepth() <= value;
        }
        return false;
    }

    /**
     * Gets the properties.
     *
     * @return the properties
     */
    @Override
    public String[] getProperties() {
        return new String[]{DEPTH};
    }

    /**
     * Gets the sendable instance.
     *
     * @param prototype the prototype
     * @return the sendable instance
     */
    @Override
    public Object getSendableInstance(boolean prototype) {
        return new Deep();
    }

    /**
     * Gets the value.
     *
     * @param entity    the entity
     * @param attribute the attribute
     * @return the value
     */
    @Override
    public Object getValue(Object entity, String attribute) {
        if (DEPTH.equalsIgnoreCase(attribute)) {
            return ((Deep) entity).getDepth();
        }
        return null;
    }

    /**
     * Sets the value.
     *
     * @param entity    the entity
     * @param attribute the attribute
     * @param value     the value
     * @param type      the type
     * @return true, if successful
     */
    @Override
    public boolean setValue(Object entity, String attribute, Object value, String type) {
        if (DEPTH.equalsIgnoreCase(attribute)) {
            ((Deep) entity).withDepth(Integer.parseInt("" + value));
      return true;
    }
    return false;
  }
}
