package de.uniks.networkparser.bytes;

import de.uniks.networkparser.buffer.Buffer;
import de.uniks.networkparser.buffer.ByteBuffer;
import de.uniks.networkparser.interfaces.BaseItem;
import de.uniks.networkparser.interfaces.ByteItem;

/**
 * Decoder for CBor Format
 *
 * @author Stefan
 */
public class ByteConverterCBor extends ByteConverter {
    private static final int UINT8_FOLLOWS = 0x18;
    private static final int UINT16_FOLLOWS = 0x19;
    private static final int UINT32_FOLLOWS = 0x1A;
    private static final int UINT64_FOLLOWS = 0x1B;
    private static final int VAR_FOLLOWS = 0x1F;

    private static final int BREAK = 0x1F;
    private static final int RAW_TAG = 0x18;

    private static final int TYPE_MASK = 0xE0;
    private static final int INFO_BITS = 0x1F;

    private static final int UNSIGNED_INT = (0x00);
    private static final int NEGATIVE_INTEGER = (0x01);
    private static final int BYTE_STRING = (0x40);
    private static final int UNICODE_STRING = (0x60);
    private static final int ARRAY = (0x80);
    private static final int MAP = (0xA0);
    private static final int TAG = (0xC0);
    private static final int PRIMITIVE = (0xE0);

    private Integer getLength(Buffer buffer, long aux) {
        if (aux == UINT8_FOLLOWS) {
            return (int) buffer.getByte();
        } else if (aux == UINT16_FOLLOWS) {
            return (int) buffer.getShort();
        } else if (aux == UINT32_FOLLOWS) {
            return buffer.getInt();
        } else if (aux == UINT64_FOLLOWS) {
            return (int) buffer.getLong();
        }
        return (int) aux;
    }

    /**
     * Decode a ByteArray
     *
     * @param cbor ByteArray
     * @return a new ByteItem
     */
    public ByteItem decode(byte[] cbor) {
        ByteBuffer buffer = new ByteBuffer().with(cbor);
        ByteItem result = decode(buffer);
        if (buffer.remaining() < 1) {
            return result;
        }
        ByteList list = new ByteList();
        list.add(result);
        while (buffer.remaining() > 0) {
            list.add(decode(buffer));
        }
        return list;
    }

    /**
     * Decode a ByteStream
     *
     * @param buffer The ByteBuffer
     * @return a new ByteItem
     */
    public ByteItem decode(Buffer buffer) {
        long firstByte;
        if (buffer.position() < 2) {
            firstByte = (byte) buffer.getCurrentChar();
        } else {
            firstByte = buffer.getByte();
        }
        long fb_type = firstByte & TYPE_MASK;
        long fb_aux = firstByte & INFO_BITS;
        if (fb_type == UNICODE_STRING || fb_type == BYTE_STRING) {
            Integer length = getLength(buffer, fb_aux);
            byte[] value = buffer.array(length, false);
            return ByteEntity.create(ByteTokener.DATATYPE_STRING, value);
        } else if (fb_type == UNSIGNED_INT) {
            if (fb_aux == UINT8_FOLLOWS) {
                return ByteEntity.create(ByteTokener.DATATYPE_INTEGER, buffer.getByte());
            }
            if (fb_aux == UINT16_FOLLOWS) {
                return ByteEntity.create(ByteTokener.DATATYPE_INTEGER, buffer.array(2, false));
            }
            if (fb_aux == UINT32_FOLLOWS) {
                return ByteEntity.create(ByteTokener.DATATYPE_INTEGER, buffer.array(4, false));
            }
            if (fb_aux == UINT64_FOLLOWS) {
                return ByteEntity.create(ByteTokener.DATATYPE_INTEGER, buffer.array(8, false));
            }
            return ByteEntity.create(ByteTokener.DATATYPE_INTEGER, fb_aux);

        } else if (fb_type == PRIMITIVE) {
            return ByteEntity.create(ByteTokener.DATATYPE_INTEGER, buffer.getByte());
        } else if (fb_type == TAG) {
            byte type = buffer.getByte();
            if (type == RAW_TAG) {
                Integer length = getLength(buffer, fb_aux);
                byte[] value = buffer.array(length, false);
                return ByteEntity.create(ByteTokener.DATATYPE_STRING, value);
            }
            return null;
        } else if (fb_type == ARRAY) {
            if (fb_aux == VAR_FOLLOWS) {
                ByteList result = new ByteList();
                while (buffer.remaining() > 0) {
                    ByteItem subElement = decode(buffer);
                    if (subElement.getType() == ByteTokener.DATATYPE_INTEGER) {
                        int value = (int) subElement.asObject() % 256;
                        if (value == PRIMITIVE + BREAK) {
                            break;
                        }
                    }
                    result.add(buffer);
                }
                return result;
            } else {
                int len = getLength(buffer, fb_aux);
                ByteList result = new ByteList();
                for (int i = 0; i < len; i++) {
                    result.add(decode(buffer));
                }
                return result;
            }
        } else if (fb_type == MAP) {
            Integer length = getLength(buffer, fb_aux);
            buffer.skip();
            ByteList map = new ByteList();
            for (int e = 0; e < length; e++) {
                ByteList entity = new ByteList();
                map.with(entity);
                for (int i = 0; i < 2; i++) {
                    entity.add(decode(buffer));
                }
            }
            return map;
        }
        return null;
    }

    /**
     * @param entity the new entity
     * @return a new ByteBuffer
     */
    public ByteBuffer encoding(BaseItem entity) {
        ByteBuffer buffer = new ByteBuffer();
        if (entity == null) {
            return buffer;
        }
        if (entity instanceof ByteList) {
            // if(entity instanceof ByteList) {
        }
        if (entity instanceof ByteEntity) {
            ByteEntity item = (ByteEntity) entity;
            // item.getType()
        }
        // if (fb_type == UNICODE_STRING || fb_type == BYTE_STRING) {
        // } else if (fb_type == UNSIGNED_INT) {
        // if(fb_aux == UINT8_FOLLOWS) {
        // if(fb_aux == UINT16_FOLLOWS) {
        // if(fb_aux == UINT32_FOLLOWS) {
        // if(fb_aux == UINT64_FOLLOWS) {
        // } else if (fb_type == PRIMITIVE) {
        // } else if (fb_type == TAG) {
        // } else if (fb_type == ARRAY) {
        // } else if (fb_type == MAP) {
        return buffer;
    }

    // protected void encodeTypeAndLength(int majorType, BigInteger length) throws CborException {
    // boolean negative = majorType == NEGATIVE_INTEGER;
    // int symbol = majorType << 5;
    // if (length.compareTo(BigInteger.valueOf(24)) == -1) {
    // write(symbol | length.intValue());
    // } else if (length.compareTo(BigInteger.valueOf(256)) == -1) {
    // symbol |= AdditionalInformation.ONE_BYTE.getValue();
    // write((byte) symbol, (byte) length.intValue());
    // } else if (length.compareTo(BigInteger.valueOf(65536L)) == -1) {
    // symbol |= AdditionalInformation.TWO_BYTES.getValue();
    // long twoByteValue = length.longValue();
    // write((byte) symbol, (byte) (twoByteValue >> 8), (byte) (twoByteValue & 0xFF));
    // } else if (length.compareTo(BigInteger.valueOf(4294967296L)) == -1) {
    // symbol |= AdditionalInformation.FOUR_BYTES.getValue();
    // long fourByteValue = length.longValue();
    // write((byte) symbol, (byte) ((fourByteValue >> 24) & 0xFF), (byte) ((fourByteValue >> 16) &
    // 0xFF),
    // (byte) ((fourByteValue >> 8) & 0xFF), (byte) (fourByteValue & 0xFF));
    // } else if (length.compareTo(new BigInteger("18446744073709551616")) == -1) {
    // symbol |= AdditionalInformation.EIGHT_BYTES.getValue();
    // BigInteger mask = BigInteger.valueOf(0xFF);
    // write((byte) symbol, length.shiftRight(56).and(mask).byteValue(),
    // length.shiftRight(48).and(mask).byteValue(), length.shiftRight(40).and(mask).byteValue(),
    // length.shiftRight(32).and(mask).byteValue(), length.shiftRight(24).and(mask).byteValue(),
    // length.shiftRight(16).and(mask).byteValue(), length.shiftRight(8).and(mask).byteValue(),
    // length.and(mask).byteValue());
    // } else {
    // if (negative) {
    // encoder.encode(new Tag(3));
    // } else {
    // encoder.encode(new Tag(2));
    // }
    // encoder.encode(new ByteString(length.toByteArray()));
  // }
  // }
}
