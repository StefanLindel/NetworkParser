package de.uniks.networkparser.bytes;

/**
 * HMAC implements HMAC-SHA256 message authentication algorithm.
 *
 * @author Stefan Lindel
 */
public class HMAC {
  private final SHA2 inner = new SHA2();
  private final SHA2 outer = new SHA2();

  private int[] istate;
  private int[] ostate;

  /**
   * Instantiates a new hmac.
   *
   * @param key the key
   */
  public HMAC(String key) {
    if (key == null) {
      return;
    }
    byte[] keyBytes = key.getBytes();
    byte[] pad = new byte[inner.blockSize];
    if (keyBytes.length > inner.blockSize) {
      (new SHA2()).update(keyBytes, keyBytes.length).finish(pad).clean();
    } else {
      for (int i = 0; i < key.length(); i++) {
        pad[i] = keyBytes[i];
      }
    }
    for (int i = 0; i < pad.length; i++) {
      pad[i] ^= 0x36;
    }
    inner.update(pad, pad.length);

    for (int i = 0; i < pad.length; i++) {
      pad[i] ^= 0x36 ^ 0x5c;
    }
    outer.update(pad, pad.length);

    istate = new int[8];
    ostate = new int[8];

    inner._saveState(istate);
    outer._saveState(ostate);

    for (int i = 0; i < pad.length; i++) {
      pad[i] = 0;
    }
  }

  /**
   * Returns HMAC state to the state initialized with key to make it possible to run HMAC over the
   * other data with the same key without creating a new instance.
   *
   * @return ThisComponent
   */
  public HMAC reset() {
    inner._restoreState(istate, inner.blockSize);
    outer._restoreState(ostate, outer.blockSize);
    return this;
  }

  /**
   * Clean.
   */
  /* Cleans HMAC state. */
  public void clean() {
    if (istate != null) {
      for (int i = 0; i < istate.length; i++) {
        ostate[i] = istate[i] = 0;
      }
    }
    inner.clean();
    outer.clean();
  }

  /**
   * Updates state with provided data.
   *
   * @param data new Data to Encode
   * @return ThisComponent
   */
  public HMAC update(String data) {
    if (data != null) {
      update(data.getBytes());
    }
    return this;
  }

  /**
   * Update.
   *
   * @param data the data
   * @return the hmac
   */
  public HMAC update(byte[] data) {
    if (data != null) {
      inner.update(data, data.length);
    }
    return this;
  }

  /**
   * Finalizes HMAC and puts the result in out.
   *
   * @param out byteArray to set Encoding Data
   * @return ThisComponent
   */
  public HMAC finish(byte[] out) {
    if (outer.finished) {
      outer.finish(out);
    } else {
      inner.finish(out);
      outer.update(out, inner.digestLength).finish(out);
    }
    return this;
  }

  /**
   * Digest.
   *
   * @return the byte[]
   */
  /* Returns message authentication code. */
  public byte[] digest() {
    byte[] out = new byte[inner.digestLength];
    finish(out);
    return out;
  }
}
