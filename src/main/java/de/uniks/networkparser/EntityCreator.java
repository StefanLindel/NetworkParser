package de.uniks.networkparser;

import de.uniks.networkparser.interfaces.BaseItem;
import de.uniks.networkparser.interfaces.Entity;
import de.uniks.networkparser.interfaces.EntityList;
import de.uniks.networkparser.interfaces.SendableEntityCreator;
import de.uniks.networkparser.interfaces.SendableEntityCreatorNoIndex;
import de.uniks.networkparser.interfaces.SendableEntityCreatorTag;
import de.uniks.networkparser.json.JsonArray;
import de.uniks.networkparser.json.JsonObject;
import de.uniks.networkparser.xml.XMLEntity;
import de.uniks.networkparser.xml.XMLTokener;

/**
 * The Class EntityCreator.
 *
 * @author Stefan
 */
public class EntityCreator
        implements SendableEntityCreator, SendableEntityCreatorNoIndex, SendableEntityCreatorTag {
    private static final String VALUE = "VALUE";
    private BaseItem factory;
    /**
     * The properties.
     */
    private String[] properties = new String[]{VALUE};
    private boolean keyValue;
    /**
     * NameSpace of XML.
     */
    private String nameSpace = "";

    /**
     * Creates the json.
     *
     * @param keyValue the key value
     * @return the entity creator
     */
    public static final EntityCreator createJson(boolean keyValue) {
        EntityCreator entity = new EntityCreator();
        entity.factory = new JsonArray();
        entity.keyValue = keyValue;
        return entity;
    }

    /**
     * Creates the XML.
     *
     * @return the entity creator
     */
    public static final EntityCreator createXML() {
        EntityCreator entity = new EntityCreator();
        entity.factory = new XMLEntity();
        entity.properties = new String[]{XMLEntity.PROPERTY_TAG, XMLEntity.PROPERTY_VALUE};
        return entity;
    }

    /**
     * Gets the properties.
     *
     * @return the properties
     */
    @Override
    public String[] getProperties() {
        return properties;
    }

    /**
     * Set a Namespace for XSD Element.
     *
     * @param namespace the NameSpace for XSD-Element
     * @return Itself
     */
    public EntityCreator withNameSpace(String namespace) {
        this.nameSpace = namespace;
        return this;
    }

    /**
     * Gets the sendable instance.
     *
     * @param prototyp the prototyp
     * @return the sendable instance
     */
    @Override
    public Object getSendableInstance(boolean prototyp) {
        if (factory != null) {
            return factory.getNewList(keyValue);
        }
        return null;
    }

    /**
     * Gets the value.
     *
     * @param entity    the entity
     * @param attribute the attribute
     * @return the value
     */
    @Override
    public Object getValue(Object entity, String attribute) {
        if (entity == null) {
            return null;
        }
        if (entity instanceof XMLEntity) {
            XMLEntity item = (XMLEntity) entity;
            if (XMLEntity.PROPERTY_TAG.equalsIgnoreCase(attribute)) {
                return item.getTag();
            }
            if (XMLEntity.PROPERTY_VALUE.equalsIgnoreCase(attribute)) {
                return item.getValue();
            }
        }
        if (entity instanceof Entity) {
            return ((Entity) entity).getValue(attribute);
        }
        return entity.toString();
    }

    /**
     * Sets the value.
     *
     * @param entity    the entity
     * @param attribute the attribute
     * @param value     the value
     * @param type      the type
     * @return true, if successful
     */
    @Override
    public boolean setValue(Object entity, String attribute, Object value, String type) {
        if (SendableEntityCreator.REMOVE_YOU.equalsIgnoreCase(type) || entity == null) {
            return false;
        }
        if (entity instanceof XMLEntity) {
            XMLEntity item = (XMLEntity) entity;
            if (XMLEntity.PROPERTY_TAG.equalsIgnoreCase(attribute)) {
                item.withType("" + value);
                return true;
            }
            if (XMLEntity.PROPERTY_VALUE.equalsIgnoreCase(attribute)) {
                String newValue = "" + value;
                if (newValue.length() > 0) {
                    item.withValueItem(newValue);
                }
                return true;
            }
            if (XMLTokener.CHILDREN.equals(type) && value instanceof EntityList) {
                item.withChild((EntityList) value);
                return true;
            }
            if (attribute != null && !attribute.isEmpty()) {
                item.add(attribute, value);
            }
            return true;
        }
        if (entity instanceof JsonArray) {
            JsonArray item = (JsonArray) entity;
            item.withValue((String) value);
            return true;
        }
        if (entity instanceof JsonObject) {
            JsonObject json = (JsonObject) entity;
            if (VALUE.equals(attribute)) {
                json.withValue((String) value);
            } else {
                json.withKeyValue(attribute, value);
            }
        }
        return false;
    }

    /**
     * Gets the tag.
     *
     * @return the tag
     */
    @Override
    public String getTag() {
    if (nameSpace != null) {
      return nameSpace + ":element";
    }
    return null;
  }
}
