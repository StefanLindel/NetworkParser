package de.uniks.networkparser.graph;

import de.uniks.networkparser.list.SimpleSet;

import java.util.Comparator;

/**
 * The Class GraphSimpleSet.
 *
 * @author Stefan
 */
public class GraphSimpleSet extends SimpleSet<GraphMember> implements Comparator<Object> {
    private boolean comparator = true;

    /**
     * Creates the.
     *
     * @param comparator the comparator
     * @return the graph simple set
     */
    public static GraphSimpleSet create(boolean comparator) {
        GraphSimpleSet set = new GraphSimpleSet();
        set.comparator = comparator;
        return set;
    }

    @Override
    protected boolean checkValue(Object a, Object b) {
        if (!(a instanceof GraphMember)) {
            return a.equals(b);
        }

        String idA = ((GraphMember) a).getFullId();
        if (idA == null) {
            return a.equals(b);
        }
        String idB;
        if (b instanceof String) {
            idB = (String) b;
        } else {
            idB = ((GraphMember) b).getFullId();
        }
        return idA.equalsIgnoreCase(idB);
    }

    /**
     * Comparator.
     *
     * @return the comparator
     */
    @Override
    public Comparator<Object> comparator() {
        return this;
    }

    /**
     * Checks if is comparator.
     *
     * @return true, if is comparator
     */
    @Override
    public boolean isComparator() {
        return comparator;
    }

    /**
     * Compare.
     *
     * @param o1 the o 1
     * @param o2 the o 2
     * @return the int
     */
    public int compare(Object o1, Object o2) {
        if (!(o1 instanceof GraphMember) || !(o2 instanceof GraphMember)) {
            return 0;
        }
        String id1 = ((GraphMember) o1).getFullId();
        String id2 = ((GraphMember) o2).getFullId();
        if (id1 == id2) {
            return 0;
        }
        if (id1 == null) {
            return 1;
        }
        if (id2 == null) {
            return -1;
        }
        return id1.compareTo(id2);
  }
}
