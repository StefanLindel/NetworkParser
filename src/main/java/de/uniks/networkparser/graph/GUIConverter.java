package de.uniks.networkparser.graph;

import de.uniks.networkparser.buffer.Buffer;
import de.uniks.networkparser.gui.controls.*;
import de.uniks.networkparser.interfaces.BaseItem;
import de.uniks.networkparser.interfaces.Converter;
import de.uniks.networkparser.interfaces.Entity;
import de.uniks.networkparser.list.SimpleKeyValueList;
import de.uniks.networkparser.xml.XMLEntity;

/**
 * The Class GUIConverter.
 *
 * @author Stefan
 */
public class GUIConverter implements Converter {
    private final SimpleKeyValueList<String, Control> factory;

    /**
     * Instantiates a new GUI converter.
     */
    public GUIConverter() {
        factory = new SimpleKeyValueList<String, Control>();
        factory.add("textArea", new TextField());
        factory.withGroup(new Group(), "vbox", "hbox");
        factory.add("label", new Label());
        factory.add("spinner", new NumberField());
        factory.add("button", new Button());
    }

    /**
     * Encode.
     *
     * @param entity the entity
     * @return the string
     */
    @Override
    public String encode(BaseItem entity) {
        if (entity instanceof Entity) {
            return convert((Entity) entity).toString();
        }
        return null;
    }

    /**
     * Convert.
     *
     * @param value the value
     * @return the control
     */
    public Control convert(String value) {
        return convert(new XMLEntity().withValue(value));
    }

    /**
     * Convert.
     *
     * @param value the value
     * @return the control
     */
    public Control convert(Buffer value) {
        return convert(new XMLEntity().withValue(value));
    }

    /**
     * Convert.
     *
     * @param value the value
     * @return the control
     */
    public Control convert(Entity value) {
        if (value instanceof XMLEntity) {
            Group group = new Group();
            group.addElement(parsingXMLEntity((XMLEntity) value));
            return group;
        }
        return null;
    }

    /**
     * Parsing XML entity.
     *
     * @param element the element
     * @return the control
     */
    public Control parsingXMLEntity(XMLEntity element) {
        String tag = null;
        if (element != null) {
            tag = element.getTag();
        }
        if (tag == null) {
            return null;
        }

        Control factory = this.factory.get(tag.toLowerCase());
        if (factory != null) {
            Control child = factory.newInstance();
            XMLEntity children = (XMLEntity) element.getElementBy(XMLEntity.PROPERTY_TAG, "children");
            if (children != null) {
                boolean add = false;
                for (int i = 0; i < children.sizeChildren(); i++) {
                    if (child.setValue(Control.PROPERTY_ELEMENTS,
                            parsingXMLEntity((XMLEntity) children.getChild(i)))) {
                        add = true;
                    }
                }
                if (add) {
                    return child;
                }
                return null;
            }
      return child;
    }
    return null;
  }
}
