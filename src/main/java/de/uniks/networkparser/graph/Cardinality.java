package de.uniks.networkparser.graph;

/**
 * The Class Cardinality.
 *
 * @author Stefan
 */
public class Cardinality {

    /**
     * The Constant ONE.
     */
    public static final int ONE = 1;

    /**
     * The Constant MANY.
     */
    public static final int MANY = 42;
}
