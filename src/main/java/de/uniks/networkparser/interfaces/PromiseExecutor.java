package de.uniks.networkparser.interfaces;

import de.uniks.networkparser.ext.promise.Promise;

import java.util.concurrent.Callable;

/**
 * The Interface PromiseExecutor. For Execution
 *
 * @author Stefan
 */
public interface PromiseExecutor {
    /**
     * Execute asynchron on another Thread.
     *
     * @param runnable the runnable
     * @return the promise for waiting
     */
    Promise<Void> execute(Runnable runnable);

    /**
     * Execute synchron on another Thread.
     *
     * @param runnable the runnable
     */
    void executeBlocking(Runnable runnable);

    /**
     * Run asynchron on another Thread for get a value
     *
     * @param <T>      the generic type
     * @param callable the callable
     * @return the promise with answer
     */
    <T> Promise<T> ask(Callable<T> callable);

    /**
     * Run synchron on another Thread for anser
     *
     * @param <T>      the generic type
     * @param callable the callable
     * @return the Answer
     */
    <T> T askBlocking(Callable<T> callable);
}
