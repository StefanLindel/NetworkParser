package de.uniks.networkparser.interfaces;

import java.util.concurrent.Executor;

import de.uniks.networkparser.NetworkParserLog;

/**
 * A factory for Execution of Promise.
 * 
 * @author Stefan
 */
public interface PromiseFactory {
  /**
   * Allow current thread for Execution.
   *
   * @return true, if successful
   */
  boolean allowCurrentThread();

  /**
   * Returns the executor to use for callbacks.
   *
   * @return The executor to use for callbacks. This will be the default callback executor if
   *         {@code null} was specified for the callback executor when this PromiseFactory was
   *         created.
   */
  Executor executor();

  /**
   * Logger for Promise
   * 
   * @return a Logger
   */
  NetworkParserLog getLogger();
}
