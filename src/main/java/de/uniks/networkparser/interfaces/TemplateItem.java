package de.uniks.networkparser.interfaces;

// TODO: Auto-generated Javadoc

/**
 * The Interface TemplateItem.
 *
 * @author Stefan
 */
public interface TemplateItem {

    /**
     * Gets the value.
     *
     * @param attribute the attribute
     * @return the value
     */
    Object getValue(String attribute);

    /**
     * Gets the name.
     *
     * @return the name
     */
    String getName();
}
