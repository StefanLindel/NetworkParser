package de.uniks.networkparser.interfaces;

// TODO: Auto-generated Javadoc
/*
 * NetworkParser The MIT License Copyright (c) 2010-2016 Stefan Lindel
 * https://www.github.com/fujaba/NetworkParser/
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

/**
 * The Interface SendableEntityCreator. This is the Basic Interface for the Creator for
 * Serialization
 *
 * @author Stefan
 */
public interface SendableEntityCreator {
    /**
     * The Constant REMOVE_YOU.
     */
    String REMOVE_YOU = "REMOVE_YOU";

    /**
     * The Constant REMOVE.
     */
    String REMOVE = "rem";

    /**
     * The Constant UPDATE.
     */
    String UPDATE = "upd";

    /**
     * The Constant NEW.
     */
    String NEW = "new";

    /**
     * The Constant SimpleFormat.
     */
    String SIMPLE = "simple";

    /**
     * The Constant Dynamic for save additional values.
     */
    String DYNAMIC = "dynamic";

    /**
     * Gets the properties.
     *
     * @return the properties
     */
    String[] getProperties();

    /**
     * Gets the value.
     *
     * @param entity    the entity
     * @param attribute the attribute
     * @return the value
     */
    Object getValue(Object entity, String attribute);

    /**
     * Sets the value.
     *
     * @param entity    the entity
     * @param attribute the attribute
     * @param value     the value
     * @param type      new, edit, update or remove operation
     * @return true, if successful
     */
    boolean setValue(Object entity, String attribute, Object value, String type);

    /**
     * Gets the sendable instance.
     *
     * @param prototyp the prototyp
     * @return the sendable instance
   */
  Object getSendableInstance(boolean prototyp);
}
