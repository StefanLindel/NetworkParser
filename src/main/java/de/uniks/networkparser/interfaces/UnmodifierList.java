package de.uniks.networkparser.interfaces;

import java.util.Collection;

/**
 * Interface for UnmodifierList. It is a Simple way to declared Returned-List as UnModifier For
 * Useage: You can use all Method but, if you use a method for changing the IDE highlight that
 * method.
 *
 * @param <E> Generic Type
 * @author Stefan
 */
public interface UnmodifierList<E> extends Collection<E> {
    @Deprecated
    @Override
    boolean add(E entity);

    @Deprecated
    @Override
    boolean remove(Object entity);

    @Deprecated
    @Override
    boolean addAll(Collection<? extends E> collection);

    @Deprecated
    @Override
    boolean removeAll(Collection<?> collection);

    @Deprecated
    @Override
    boolean retainAll(Collection<?> collection);

    @Deprecated
    @Override
    void clear();
}
