package de.uniks.networkparser.ext.petaf;

import static java.nio.file.StandardWatchEventKinds.ENTRY_CREATE;
import static java.nio.file.StandardWatchEventKinds.ENTRY_DELETE;
import static java.nio.file.StandardWatchEventKinds.ENTRY_MODIFY;
import static java.nio.file.StandardWatchEventKinds.OVERFLOW;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;

import de.uniks.networkparser.NetworkParserLog;
import de.uniks.networkparser.buffer.CharacterBuffer;
import de.uniks.networkparser.ext.Os;
import de.uniks.networkparser.ext.io.FileBuffer;
import de.uniks.networkparser.ext.io.FileBufferImpl;
import de.uniks.networkparser.ext.petaf.proxy.NodeProxyFileSystem;

/**
 * FileWatcher for Changes on Storage .
 *
 * @author Stefan Lindel
 */
public class FileWatcher implements Runnable {
    protected NodeProxy proxy;
    protected String fileName;
    private boolean runTask = true;
    private WatchService watcher;
    private NetworkParserLog logger;
    private long lastChange = -1;
    private Space space;

    /**
     * Inits the.
     *
     * @param owner    the owner
     * @param fileName the file name
     * @return the file watcher
     */
    public FileWatcher init(NodeProxyFileSystem owner, String fileName) {
        this.proxy = owner;
        this.fileName = fileName;

        return this;
    }

    /**
     * Run.
     */
    public void run() {
        if (space == null) {
            return;
        }
        if (Os.isReflectionTest()) {
            return;
        }
        while (runTask) {
            if (watcher != null) {
                searchNIO();
            } else {
                File file = new File(fileName);
                long last = file.lastModified();
                if (lastChange < 1) {
                    lastChange = last;
                } else {
                    if (last != lastChange) {
                        lastChange = last;
                        if (logger != null) {
                            logger.info(this, "run", "New (version of) file " + fileName + " detected");
                        }
                        CharacterBuffer buffer = getFileBuffer().readFile(fileName);
                        space.getMap().decode(buffer);
                    }
                }
            }
        }
    }
    
    private static FileBuffer getFileBuffer() {
		return new FileBufferImpl();
	}

    private boolean searchNIO() {
        WatchKey watchKey = null;
        if (Os.isReflectionTest()) {
            return false;
        }
        try {
            watchKey = watcher.take();
        } catch (InterruptedException e) {
        }
        if (watchKey == null) {
            return true;
        }
        for (WatchEvent<?> event : watchKey.pollEvents()) {
            WatchEvent.Kind<?> kind = event.kind();
            if (kind == OVERFLOW) {
                continue;
            }
            if (kind == ENTRY_CREATE) {
                /* if its a new json file, read it */
                Path filepath = (Path) event.context();
                if (logger != null) {
                    logger.info(this, "run", "New (version of) file " + filepath.toFile() + " detected");
                }
                CharacterBuffer buffer = FileBufferImpl.readFile(filepath.toFile());
                space.getMap().decode(buffer);
            }
            if (kind == ENTRY_MODIFY) {
                /* do I have a buf for this one, then read */
                Path filepath = (Path) event.context();
                CharacterBuffer buffer = FileBufferImpl.readFile(filepath.toFile());
                space.getMap().decode(buffer);
            }
            if (kind == ENTRY_DELETE) {
                continue;
            }
        }
        return true;
    }

    /**
     * Inits the NIO file watcher.
     *
     * @return true, if successful
     */
    public boolean initNIOFileWatcher() {
        try {
            watcher = FileSystems.getDefault().newWatchService();
            File file = new File(fileName);
            if (!file.exists() || !file.isDirectory()) {
                return false;
            }
            Path dirPath = file.toPath();
            dirPath.register(watcher, ENTRY_CREATE, ENTRY_DELETE, ENTRY_MODIFY);
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    /**
     * Close.
     */
    public void close() {
        runTask = false;
    }
}
