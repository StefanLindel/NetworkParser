package de.uniks.networkparser.ext.petaf;

/*
The MIT License

Copyright (c) 2010-2016 Stefan Lindel https://www.github.com/fujaba/NetworkParser/

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;

import de.uniks.networkparser.ext.petaf.proxy.NodeProxyTCP;
import de.uniks.networkparser.interfaces.Condition;
import de.uniks.networkparser.interfaces.Server;

/**
 * Server for TCP-Connection.
 *
 * @author Stefan Lindel
 */
public class Server_TCP extends Thread implements Server {
    private final NodeProxyTCP proxy;
    protected boolean run = true;
    protected ServerSocket serverSocket;
    private boolean searchFreePort = true;
    private Condition<Socket> handler;

    /**
     * Fallback for simple Creating a Server without proxy.
     *
     * @param port Port of TCP-Server
     */
    public Server_TCP(int port) {
        this(NodeProxyTCP.createServer(port));
    }

    /**
     * Instantiates a new server TCP.
     *
     * @param proxy the proxy
     */
    public Server_TCP(NodeProxyTCP proxy) {
       this.proxy = proxy;
        if (handler == null) {
            this.handler = proxy;
        }
        if (init()) {
            start();
        }
    }

    /**
     * Checks if is run.
     *
     * @return true, if is run
     */
    public boolean isRun() {
        return run;
    }

    /**
     * Close.
     *
     * @return true, if successful
     */
    public boolean close() {
        run = false;
        try {
            if (serverSocket != null) {
                serverSocket.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    /**
     * Run.
     */
    @Override
    public void run() {
        if (proxy.getUrl() != null) {
            Thread.currentThread().setName(proxy.getUrl() + ":" + proxy.getPort() + " com server");
        } else {
            Thread.currentThread().setName("localhost:" + proxy.getPort() + " com server");
        }
        while (!isInterrupted() && run) {
            Socket requestSocket = null;
            try {
                requestSocket = serverSocket.accept();
                MessageRequest task = new MessageRequest(requestSocket, handler);
                TaskExecutor executor = proxy.getExecutor();
                if (executor != null) {
                    executor.executeTask(task, 0);
                }
            } catch (Exception ex) {
                if (proxy != null) {
                    proxy.logException(this, "run", ex);
                }
            } finally {
            }
        }
    }

    private boolean init() {
        if (proxy == null || proxy.getPort() < 1) {
            return false;
        }
        try {
            serverSocket = new ServerSocket(proxy.getPort(), 10, null);
            return true;
        } catch (UnknownHostException e) {
            return false;
        } catch (IOException e) {
            if (searchFreePort) {
                /* Wrong PORT */
                try {
                    serverSocket = new ServerSocket(0, 10, null);
                    proxy.withPort(serverSocket.getLocalPort());
                    return true;
                } catch (Exception exception) {
                }
            }
        }
        return false;
    }

    /**
     * Checks if is search free port.
     *
     * @return the searchFreePort
     */
    public boolean isSearchFreePort() {
        return searchFreePort;
    }

    /**
     * With search free port.
     *
     * @param searchFreePort the searchFreePort to set
     * @return ThisComponent
     */
    public Server_TCP withSearchFreePort(boolean searchFreePort) {
        this.searchFreePort = searchFreePort;
        return this;
    }

    /**
     * With handler.
     *
     * @param handler the handler
     * @return the server
     */
    public Server withHandler(Condition<Socket> handler) {
        this.handler = handler;
        return this;
    }
}
