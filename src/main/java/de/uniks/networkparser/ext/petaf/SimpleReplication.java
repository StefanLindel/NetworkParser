package de.uniks.networkparser.ext.petaf;

import java.util.Collection;

import de.uniks.networkparser.IdMap;
import de.uniks.networkparser.ext.generic.ReflectionLoader;
import de.uniks.networkparser.ext.gui.GUIEvent;
import de.uniks.networkparser.interfaces.ObjectCondition;
import de.uniks.networkparser.interfaces.SendableEntityCreator;
import de.uniks.networkparser.interfaces.SendableEntityCreatorTag;

/**
 * Simple Replication.
 *
 * @author Stefan Lindel
 */
public class SimpleReplication implements ObjectCondition {
    protected Object item;
    protected ObjectCondition listener;
    private Space space;
    private Object creator;
    private Object tag;

    /**
     * Bind.
     *
     * @param creator the creator
     * @param root    the root
     * @param tag     the tag
     * @return the simple replication
     */
    public static SimpleReplication bind(SendableEntityCreator creator, Object root, String... tag) {
        Object item = ReflectionLoader.call(creator, "createIdMap", "");
        SimpleReplication binder = new SimpleReplication();
        if (!(item instanceof IdMap)) {
            return binder;
        }
        IdMap map = (IdMap) item;
        binder.getSpace().withMap(map);

        String myTag = null;
        if (creator instanceof SendableEntityCreatorTag) {
            myTag = ((SendableEntityCreatorTag) creator).getTag();
        } else if (tag != null && tag.length > 0) {
            myTag = tag[0];
        }
        if (myTag != null) {
            binder.binding(creator, root, myTag);
        }
        return binder;
    }

    /**
     * Binding.
     *
     * @param creator the creator
     * @param root    the root
     * @param tag     the tag
     * @return the simple replication
     */
    public SimpleReplication binding(SendableEntityCreator creator, Object root, String tag) {
        /* Bind all Children to */
        this.creator = creator;
        this.item = creator.getSendableInstance(false);
        this.tag = tag;

        Collection<?> items = (Collection<?>) ReflectionLoader.call(root, "getChildrenUnmodifiable");
        String[] properties = creator.getProperties();
        for (Object child : items) {
            int i = 0;
            String id = (String) ReflectionLoader.call(child, "getId");
            for (; i < properties.length; i++) {
                if (properties[i] == null) {
                    continue;
                }
                if (properties[i].equals(id)) {
                    break;
                }
            }
            if (i < properties.length) {
                /* Found */
                GUIEvent javaFXEvent = new GUIEvent();
                javaFXEvent.withListener(this);
                Object proxy = ReflectionLoader.createProxy(javaFXEvent, ReflectionLoader.EVENTHANDLER);

                ReflectionLoader.call(child, "setOnKeyReleased", ReflectionLoader.EVENTHANDLER, proxy);
                if (id.equals(tag)) {
                    /* It is Primary Key */
                    Object focusProperty = ReflectionLoader.call(child, "focusedProperty");
                    ReflectionLoader.call(focusProperty, "addListener", ReflectionLoader.CHANGELISTENER, proxy);
                }
            }
        }
        return this;
    }

    /**
     * Checks if is valid.
     *
     * @return true, if is valid
     */
    public boolean isValid() {
        if (creator == null) {
            return false;
        }
        if (tag == null) {
            return false;
        }
        return getSpace().getMap() != null;
    }

    /**
     * With map.
     *
     * @param map the map
     * @return the simple replication
     */
    public SimpleReplication withMap(IdMap map) {
        getSpace().withMap(map);
        return this;
    }

    /**
     * Gets the space.
     *
     * @return the space
     */
    public Space getSpace() {
        if (space == null) {
            space = new Space();
        }
        return space;
    }

    /**
     * Update.
     *
     * @param value the value
     * @return true, if successful
     */
    @Override
    public boolean update(Object value) {
        return false;
    }
}
