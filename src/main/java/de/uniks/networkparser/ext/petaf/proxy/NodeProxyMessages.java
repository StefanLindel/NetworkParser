package de.uniks.networkparser.ext.petaf.proxy;

import de.uniks.networkparser.NetworkParserLog;
import de.uniks.networkparser.ext.io.MessageSession;
import de.uniks.networkparser.ext.io.SocketMessage;
import de.uniks.networkparser.ext.petaf.Message;
import de.uniks.networkparser.ext.petaf.NodeProxy;

/**
 * The Class NodeProxyMessages.
 *
 * @author Stefan
 */
public class NodeProxyMessages extends NodeProxy {
    /** The Constant EVENT_CONNECTION. */
    public static final String EVENT_CONNECTION = "connection";
    /** The Constant BODY. */
    public static final String BODY = "body";
    /** The Constant MESSAGE. */
    public static final String MESSAGE = "message";
    /** The Constant PROPERTY_URL. */
    public static final String PROPERTY_URL = "url";
    /** The Constant PROPERTY_PORT. */
    public static final String PROPERTY_PORT = "port";
    /** The Constant PROPERTY_ACCOUNT. */
    public static final String PROPERTY_ACCOUNT = "account";
    /** The Constant PROPERTY_PASSWORD. */
    public static final String PROPERTY_PASSWORD = "password";
    /** The Constant PROPERTY_MESSAGETYPE. */
    public static final String PROPERTY_MESSAGETYPE = "msgtype";

    private MessageSession connection = null;
    private String password;
    private String msgType = MessageSession.TYPE_EMAIL;

    /**
     * Instantiates a new node proxy messages.
     */
    public NodeProxyMessages() {
        this.property.addAll(PROPERTY_URL, PROPERTY_PORT, PROPERTY_ACCOUNT, PROPERTY_MESSAGETYPE);
        this.propertyUpdate.addAll(PROPERTY_URL, PROPERTY_PORT, PROPERTY_MESSAGETYPE);
        this.propertyInfo.addAll(PROPERTY_URL, PROPERTY_PORT, PROPERTY_ACCOUNT, PROPERTY_MESSAGETYPE);
    }

    /**
     * Compare to.
     *
     * @param o the o
     * @return the int
     */
    @Override
    public int compareTo(NodeProxy o) {
        return 0;
    }

    /**
     * Gets the value.
     * @param element  the element
     * @param attrName the attr name
     * @return the value
     */
    @Override
    public Object getValue(Object element, String attrName) {
        if (element instanceof NodeProxyMessages) {
            NodeProxyMessages nodeProxy = (NodeProxyMessages) element;
            if (PROPERTY_URL.equals(attrName)) {
                return nodeProxy.getUrl();
            }
            if (PROPERTY_PORT.equals(attrName)) {
                return nodeProxy.getPort();
            }
            if (PROPERTY_ACCOUNT.equals(attrName)) {
                return getKey();
            }
            if (PROPERTY_PASSWORD.equals(attrName)) {
                return nodeProxy.getPort();
            }
            if (PROPERTY_MESSAGETYPE.equals(attrName)) {
                return nodeProxy.getMessageType();
            }
        }
        return super.getValue(element, attrName);
    }

    /**
     * Sets the value.
     * @param element  the element
     * @param attrName the attr name
     * @param value    the value
     * @param type     the type
     * @return true, if successful
     */
    @Override
    public boolean setValue(Object element, String attrName, Object value, String type) {
        if (element instanceof NodeProxyMessages) {
            NodeProxyMessages nodeProxy = (NodeProxyMessages) element;
            if (PROPERTY_URL.equals(attrName)) {
                nodeProxy.withUrl((String) value);
                return true;
            }
            if (PROPERTY_PORT.equals(attrName)) {
                nodeProxy.withPort((Integer) value);
                return true;
            }
            if (PROPERTY_ACCOUNT.equals(attrName)) {
                nodeProxy.withSender((String) value);
                return true;
            }
            if (PROPERTY_PASSWORD.equals(attrName)) {
                nodeProxy.withPassword((String) value);
                return true;
            }
            if (PROPERTY_MESSAGETYPE.equals(attrName)) {
                nodeProxy.withMessageType((String) value);
                return true;
            }
        }
        return super.setValue(element, attrName, value, type);
    }

    private NodeProxyMessages withMessageType(String value) {
        this.msgType = value;
        return this;
    }

    private String getMessageType() {
        return msgType;
    }

    /**
     * Gets the key.
     * @return the key
     */
    @Override
    public String getKey() {
        if (connection != null) {
            return connection.getSender();
        }
        return null;
    }

    /**
     * Close.
     * @return true, if successful
     */
    @Override
    public boolean close() {
        if (connection != null) {
            MessageSession conn = connection;
            connection = null;
            return conn.close();
        }
        return true;
    }

    @Override
    protected boolean startProxy() {
        withType(NodeProxy.TYPE_INOUT);
        return true;
    }

    /**
     * Checks if is sendable.
     * @return true, if is sendable
     */
    @Override
    public boolean isSendable() {
        if (this.connection != null) {
            return this.connection.getSender() != null;
        }
        return false;
    }

    /**
     * Gets the sendable instance.
     * @param prototyp the prototyp
     * @return the sendable instance
     */
    @Override
    public Object getSendableInstance(boolean prototyp) {
        return new NodeProxyMessages();
    }

    /**
     * Connect.
     * @return true, if successful
     */
    public boolean connect() {
        if (this.connection != null) {
            this.connection.withType(this.msgType);
            return this.connection.connect(password);
            /* TYPE */
        }
        return false;
    }

    /**
     * With sender.
     * @param name the name
     * @return the node proxy messages
     */
    public NodeProxyMessages withSender(String name) {
        if (this.connection == null) {
            this.connection = getNewConnection();
        }
        this.connection.setSender(name);
        return this;
    }

    protected MessageSession getNewConnection() {
        return new MessageSession().withAttachments();
    }

    protected SocketMessage getNewEMailMessage() {
        return new SocketMessage(this.name).withSubject("Message from PetaF");
    }

    /**
     * add attachments.
     * @param values the values
     * @return the node proxy messages
     */
    public NodeProxyMessages withAttachments(String... values) {
        if (connection != null) {
            connection.withAttachments(values);
        }
        return this;
    }

    /**
     * With port.
     *
     * @param value the value
     * @return the node proxy messages
     */
    public NodeProxyMessages withPort(Integer value) {
        if (this.connection == null) {
            this.connection = getNewConnection();
        }
        this.connection.withPort(value);
        return this;
    }

    /**
     * Gets the port.
     *
     * @return the port
     */
    public int getPort() {
        if (this.connection != null) {
            return this.connection.getPort();
        }
        return -1;
    }

    /**
     * With url.
     *
     * @param value the value
     * @return the node proxy messages
     */
    public NodeProxyMessages withUrl(String value) {
        if (this.connection == null) {
            this.connection = getNewConnection();
        }
        this.connection.withHost(value);
        return this;
    }

    /**
     * Gets the url.
     *
     * @return the url
     */
    public String getUrl() {
        if (this.connection != null) {
            return this.connection.getUrl();
        }
        return null;

    }

    /**
     * With password.
     *
     * @param password the password
     * @return the node proxy messages
     */
    public NodeProxyMessages withPassword(String password) {
        this.password = password;
        return this;
    }

    @Override
    protected boolean sending(Message msg) {
        if (super.sending(msg)) {
            return true;
        }
        if (this.connection == null) {
            return false;
        }
        SocketMessage message = getNewEMailMessage();

        String buffer;
        if (this.space != null) {
            buffer = this.space.convertMessage(msg);
        } else {
            buffer = msg.toString();
        }
        message.withMessage(buffer);
        boolean success = this.connection.sending(message, this.password);
        if (success) {
            setSendTime(buffer.length());
        }
        return success;
    }

    /**
     * With TLS.
     *
     * @param value the value
     * @return the node proxy messages
     */
    public NodeProxyMessages withTLS(boolean value) {
        if (this.connection != null) {
            this.connection.withTLS(value);
        }
        return this;
    }

    /**
     * Send E mail.
     *
     * @param msg the msg
     * @return true, if successful
     */
    public boolean sendEMail(SocketMessage msg) {
        boolean success = connection.sending(msg, this.password);
        if (success) {
            String buffer = msg.toString();
            setSendTime(buffer.length());
        }
        return success;
    }

    /**
     * With logger.
     *
     * @param value the value
     * @return the node proxy messages
     */
    public NodeProxyMessages withLogger(NetworkParserLog value) {
        if (this.connection != null) {
            this.connection.withLogger(value);
        }
        return this;
    }
}
