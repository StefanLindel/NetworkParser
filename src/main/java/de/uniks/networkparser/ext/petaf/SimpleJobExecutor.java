package de.uniks.networkparser.ext.petaf;

import java.util.Timer;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;

import de.uniks.networkparser.DateTimeEntity;
import de.uniks.networkparser.ext.promise.Promise;
import de.uniks.networkparser.interfaces.PromiseExecutor;

/** Execute a Promise 
 * @author Stefan
 */
public class SimpleJobExecutor implements PromiseExecutor {
   private final DateTimeEntity lastRun = new DateTimeEntity();
   private boolean isCancel;
   private Space space;
   private Timer executor;
   private ExecutorService executorService;
   /**
    * Checks if is cancel.
    * @return true, if is cancel
    */
   public boolean isCancel() {
       return isCancel;
   }
   
   /** Executing a new Task
    * @param task a new Task
    * @return a new Promis of Executing
    */
   public Promise<Object> excute(Runnable task) {
      Promise<Object> promise=new Promise<>();
      if (isCancel()) {
         return promise.fail(new IllegalStateException());
     }
     try {
         lastRun.withValue(System.currentTimeMillis());
         if (task != null) {
           task.run();
             SimpleTimerTask newTask;
             if (task instanceof SimpleTimerTask) {
                 newTask = (SimpleTimerTask) task;
             } else {
                 newTask = new SimpleTimerTask(space).withTask(task);
             }
             newTask.withDateTime(lastRun);
             
         }
     } catch (Exception e) {
         if (space != null) {
             space.handleException(e);
         }
     }
     return promise;
   }

   @Override
   public Promise<Void> execute(Runnable runnable) {
      // TODO Auto-generated method stub
      return null;
   }

   @Override
   public void executeBlocking(Runnable runnable) {
      // TODO Auto-generated method stub
      
   }

   @Override
   public <T> Promise<T> ask(Callable<T> callable) {
      // TODO Auto-generated method stub
      return null;
   }

   @Override
   public <T> T askBlocking(Callable<T> callable) {
      // TODO Auto-generated method stub
      return null;
   }

}
