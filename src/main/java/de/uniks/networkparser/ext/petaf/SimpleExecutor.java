package de.uniks.networkparser.ext.petaf;

import java.util.Timer;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import de.uniks.networkparser.DateTimeEntity;
import de.uniks.networkparser.NetworkParserLog;
import de.uniks.networkparser.interfaces.PromiseFactory;

/**
 * Simple Execution.
 * @author Stefan Lindel
 */
public class SimpleExecutor implements TaskExecutor, PromiseFactory {
    private final DateTimeEntity lastRun = new DateTimeEntity();
    private boolean isCancel;
    private Space space;
    private Timer executor;
    private ExecutorService executorService;
    private boolean allowCurrentThread = true;

    /**
     * Allow current thread for Execution.
     * @return true, if successful
     */
    public boolean allowCurrentThread() {
        return allowCurrentThread;
    }
    
    /**
     * Flag for allow use current Thread
     * @param value flag to set
     * @return ThisComponent
     */
    public SimpleExecutor withAllowCurrentThread(boolean value) {
       allowCurrentThread = value;
       return this;
    }

    /**
     * Get Executor.
     * @return the executor
     */
    @Override
    public Executor executor() {
        return executorService;
    }

    /**
     * Creates the simple executor.
     * @param name the name
     * @return the simple executor
     */
    public static SimpleExecutor createSimpleExecutor(String... name) {
        SimpleExecutor executor = new SimpleExecutor();
        if (name != null && name.length > 0) {
            executor.executor = new Timer(name[0]);
        } else {
            executor.executor = new Timer("TimerExecutor");
        }
        return executor;
    }

    /**
     * Cancel.
     */
    public void cancel() {
        isCancel = true;
        if (executor != null) {
            executor.cancel();
            executor = null;
        }
    }
    
   /** Close the Executor. */
   public void close(){
       this.cancel();
   }

    /**
     * Checks if is cancel.
     *
     * @return true, if is cancel
     */
    public boolean isCancel() {
        return isCancel;
    }

    /**
     * Execute task.
     *
     * @param task     the task
     * @param delay    the delay
     * @param interval the interval
     * @return the object
     */
    @Override
    public SimpleTimerTask executeTask(Runnable task, int delay, int interval) {
        if (isCancel()) {
            return null;
        }
        try {
            lastRun.withValue(System.currentTimeMillis());
            SimpleTimerTask newTask;
            if (task instanceof SimpleTimerTask) {
                newTask = (SimpleTimerTask) task;
            } else {
                newTask = new SimpleTimerTask(space).withTask(task);
            }
            newTask.withDateTime(lastRun);
            if (executor != null) {
                if (interval > 0) {
                    executor.schedule(newTask, delay, interval);
                } else {
                    executor.schedule(newTask, delay);
                }
            }
            if (executorService != null) {
                executorService.execute(newTask);
            }
            if (task != null) {
                task.run();
            }
            return newTask;

        } catch (Exception e) {
            if (space != null) {
                space.handleException(e);
            }
        }
        return null;
    }

    /**
     * Execute task.
     *
     * @param task     the task
     * @param delay    the delay
     * @param interval the interval
     * @param unit     the TimeUnit of Delay
     * @return the object
     */
    public Object executeTaskAtFixedRate(Runnable task, int delay, int interval, TimeUnit unit) {
        try {
            lastRun.withValue(System.currentTimeMillis());
            if (executor instanceof ScheduledExecutorService) {
                return ((ScheduledExecutorService) executor).scheduleAtFixedRate(task, delay, interval, unit);
            }
            if (executorService instanceof ScheduledExecutorService) {
              return ((ScheduledExecutorService) executorService).scheduleAtFixedRate(task, delay, interval, unit);
            }
            if (task != null) {
                task.run();
            }

        } catch (Exception e) {
            if (space != null) {
                space.handleException(e);
            }
        }
        return null;
    }

    /**
     * Execute task.
     *
     * @param task  the task
     * @param delay the delay
     * @return the object
     */
    @Override
    public SimpleTimerTask executeTask(Runnable task, int delay) {
        if (isCancel()) {
            return null;
        }
        try {
            lastRun.withValue(System.currentTimeMillis());
            if (task != null) {
                SimpleTimerTask newTask;
                if (task instanceof SimpleTimerTask) {
                    newTask = (SimpleTimerTask) task;
                } else {
                    newTask = new SimpleTimerTask(space).withTask(task);
                }
                newTask.withDateTime(lastRun);
                if (executor != null) {
                    executor.schedule(newTask, delay);
                    return newTask;
                }
                if (executorService != null) {
                    executorService.execute(task);
                    return newTask;
                }
                task.run();
                return newTask;
            }
        } catch (Exception e) {
            if (space != null) {
                space.handleException(e);
            }
        }
        return null;
    }
    
    
    /**
     * Handle msg.
     *
     * @param message the message
     * @return true, if successful
     */
    @Override
    public boolean handleMsg(Message message) {
        if (space != null) {
            return space.handleMsg(message);
        }
        return false;
    }

    /**
     * Shutdown.
     */
    @Override
    public void shutdown() {
        cancel();
    }

    /**
     * With space.
     *
     * @param space the space
     * @return the simple executor
     */
    @Override
    public SimpleExecutor withSpace(Space space) {
        this.space = space;
        return this;
    }

    /**
     * Gets the space.
     *
     * @return the space
     */
    @Override
    public Space getSpace() {
        return space;
    }

    /**
     * Gets the last run.
     *
     * @return the last run
     */
    @Override
    public DateTimeEntity getLastRun() {
        return lastRun;
    }

    /**
     * With executor service.
     *
     * @param executor the executor
     * @return the task executor
     */
    public SimpleExecutor withExecutorService(ExecutorService executor) {
        executorService = executor;
        return this;
    }

   @Override
   public NetworkParserLog getLogger() {
      return space.getLogger();
   }
}
