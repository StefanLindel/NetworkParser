package de.uniks.networkparser.ext.promise;

import java.util.Queue;
import java.util.function.Consumer;
import java.util.function.Function;

/**
 * The Class PromiseConsumer.
 *
 * @author Stefan
 * @param <T> the generic type
 * @param <R> the generic type
 */
public class PromiseConsumer<T, R> implements Consumer<T> {
    private Promise<Object> promise;
    private Function<? super T, R> mapper;
    private Queue<?> callBacks;
    private Class<?>[] filterClazz;

    /**
     * Instantiates a new promise consumer.
     *
     * @param promise the promise
     * @param mapper the mapper
     * @param filterClazz Filter for Filtering execution on Promiseresult
     */
    @SuppressWarnings("unchecked")
    public PromiseConsumer(Promise<?> promise, Function<? super T, R> mapper, Class<?>... filterClazz) {
        this.promise = (Promise<Object>) promise;
        this.mapper = mapper;
        this.filterClazz = filterClazz;
    }

    /**
     * Instantiates a new promise consumer.
     *
     * @param callBacks the call backs
     * @param mapper the mapper
     */
    public PromiseConsumer(Queue<?> callBacks, Function<? super T, R> mapper) {
        this.callBacks = callBacks;
        this.mapper = mapper;
    }

    /**
     * Accept Case.
     *
     * @param t the t
     */
    @Override
    public void accept(T t) {
        if (callBacks != null) {
            if (Boolean.FALSE.equals(t)) {
                this.callBacks.clear();
            }
            return;
        }
        if(filterClazz != null) {
           for(Class<?> clazz : filterClazz) {
              if (t.getClass().equals(clazz)) {
                 return;
              }
           }
        }
        promise.resolving(mapper.apply(t));
    }
}
