package de.uniks.networkparser.ext.promise;

import java.lang.reflect.InvocationTargetException;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Executor;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.function.Consumer;
import java.util.function.Function;

import de.uniks.networkparser.NetworkParserLog;
import de.uniks.networkparser.interfaces.PromiseFactory;

/**
 * The Class Promise.
 * 
 * @author Stefan
 * @param <T> the generic type
 */
public class Promise<T> implements Executor {
    /** The default amount to wait for a promise to resolve. */
    public static final int DEFAULT_WAIT_TIMEOUT_MS = 60_000;

    /**
     * A CountDownLatch to manage the resolved state of this Promise.
     * <p>
     * This object is used as the synchronizing object to provide a critical section
     * in {@link #tryResolve(Object, Throwable)} so that only a single thread can
     * write the resolved state variables and open the latch.
     * <p>
     * The resolved state variables, {@link #value} and {@link #fail}, must only be
     * written when the latch is closed (getCount() != 0) and must only be read when
     * the latch is open (getCount() == 0). The latch state must always be checked
     * before writing or reading since the resolved state variables' memory
     * consistency is guarded by the latch.
     */
    private final CountDownLatch resolved = new CountDownLatch(1);

    /**
     * A ConcurrentLinkedQueue to hold the callbacks for this Promise, so no
     * additional synchronization is required to write to or read from the queue.
     */
    private Queue<CallBackPromise<?>> callBacks;

    /** The value of this Promise if successfully resolved. */
    private T value;

    /** The failure of this Promise if resolved with a failure or {@code null} if successfully resolved. */
    private Throwable fail;

    /** The ExecutorFactory. */
    private PromiseFactory factory;
    
    /**
     * Returns whether this Promise has been resolved.
     *
     * This Promise may be successfully resolved or resolved with a failure.
     *
     * @return {@code true} if this Promise was resolved either successfully or with
     *         a failure; {@code false} if this Promise is unresolved.
     */
    public boolean isDone() {
        return resolved.getCount() == 0;
    }

    /**
     * Returns the failure of this Promise.
     *
     * If this Promise is not {@link #isDone() resolved}, this method must block and wait for this Promise to be resolved before completing.
     *
     * If this Promise was resolved with a failure, this method returns with the
     * failure of this Promise. If this Promise was successfully resolved, this
     * method must return {@code null}.
     *
     * @return The failure of this resolved Promise or {@code null} if this Promise was successfully resolved.
     * @throws InterruptedException If the current thread was interrupted while waiting.
     */
    public Throwable getFailure() throws InterruptedException {
        return getFailure(DEFAULT_WAIT_TIMEOUT_MS);
    }
    
    /**
     * Returns the failure of this Promise. Don't throw any Exception
     * @return The failure of this resolved Promise or {@code null} if this Promise was successfully resolved.
     */
    public Throwable getFailureSafe() {
       try {
         return getFailure(DEFAULT_WAIT_TIMEOUT_MS);
      } catch (InterruptedException ex) {
         Thread.currentThread().interrupt();
         return ex;
      }
   }

    /**
     * The same as {@link #getFailure()}.
     *
     * @param timeoutMs Timeout
     * @return The failure of this resolved Promise or {@code null} if this Promise
     *         was successfully resolved.
     * @throws InterruptedException If the current thread was interrupted while
     *                              waiting.
     */
    public Throwable getFailure(long timeoutMs) throws InterruptedException {
        if (!resolved.await(timeoutMs, TimeUnit.MILLISECONDS)) {
            return new TimeoutException();
        }
        return fail;
    }

    /**
     * Gets a value of Promise.
     * 
     * @return the value
     * @throws InvocationTargetException the invocation target exception
     * @throws InterruptedException      the interrupted exception
     * @throws TimeoutException          the timeout exception
     */
    public T getValue() throws InvocationTargetException, InterruptedException, TimeoutException {
        return getValue(DEFAULT_WAIT_TIMEOUT_MS);
    }

    /**
     * The same as {@link #getValue()}, but waits only the given amount of time for
     * the promise to resolve.
     *
     * @param timeoutMs amount of time to wait for the result.
     * @return The value of this resolved Promise.
     * @throws InvocationTargetException If this Promise was resolved with a
     *                                   failure. The cause of the
     *                                   {@code InvocationTargetException} is the
     *                                   failure exception.
     * @throws InterruptedException      If the current thread was interrupted while
     *                                   waiting.
     * @throws TimeoutException          if the given timeout is exceeded.
     */
    public T getValue(long timeoutMs) throws InvocationTargetException, InterruptedException, TimeoutException {
        boolean success = resolved.await(timeoutMs, TimeUnit.MILLISECONDS);
        if (!success) {
            throw new TimeoutException();
        }

        if (fail == null) {
            return value;
        }
        throw new InvocationTargetException(fail);
    }

    /**
     * Register a callback to be called when this Promise is resolved.
     * <p>
     * The specified callback is called when this Promise is resolved either
     * successfully or with a failure.
     * <p>
     * This method may be called at any time including before and after this Promise
     * has been resolved.
     * <p>
     * Resolving this Promise <i>happens-before</i> any registered callback is
     * called. That is, in a registered callback, {@link #isDone()} must return
     * {@code true} and {@link #getValue()} and {@link #getFailure()} must not
     * block.
     * <p>
     * A callback may be called on a different thread than the thread which
     * registered the callback. So the callback must be thread safe but can rely
     * upon that the registration of the callback <i>happens-before</i> the
     * registered callback is called.
     *
     * @param callback The callback to be called when this Promise is resolved. Must
     *                 not be {@code null}.
     * @return This Promise.
     */
    public Promise<T> onResolve(Runnable callback) {
        return onResolving(new CallBackPromise<T>(callback, this));
    }

    /**
     * On resolving.
     * 
     * @param callback the callback
     * @return the promise
     */
    private Promise<T> onResolving(CallBackPromise<?> callback) {
        if (callback == null) {
            return this;
        }
        if (factory.allowCurrentThread() && isDone()) {
            callback.run();
            return this;
        }
        if (callBacks == null) {
            callBacks = new ConcurrentLinkedQueue<>();
        }
        callBacks.offer(callback);
        return notifyCallbacks();
    }

    /**
     * Sync Promise.
     * 
     * @param timeoutMs the timeout ms
     * @param logger    the logger
     * @return the t
     */
    public T sync(long timeoutMs, NetworkParserLog logger) {
        try {
            return getValue(timeoutMs);
        } catch (Exception ex) {
            if (ex instanceof InterruptedException) {
                Thread.currentThread().interrupt(); // set interrupt flag
            }
            if (logger != null) {
                logger.error(this, "sync", "Failed to getValue", ex);
            }
        }
        return null;
    }

    /**
     * Call any registered callbacks if this Promise is resolved.
     * 
     * @return ThisComponent
     */
    public Promise<T> notifyCallbacks() {
        if (!isDone() || this.fail != null) {
            return this;
        }
        try {
            Executor executor = getExecutor();
            if (executor == null) {
                return this;
            }
            for (CallBackPromise<?> callback = callBacks.poll(); callback != null; callback = callBacks.poll()) {
                executor.execute(callback);
            }
        } catch (RejectedExecutionException e) {
            this.fail(e);
        }
        return this;
    }

    private Executor getExecutor() {
        Executor executor = factory.executor();
        if (executor == null && factory.allowCurrentThread()) {
            return this;
        }
        return executor;
    }

    @Override
    public void execute(Runnable command) {
        command.run();
    }

    /**
     * Register a callback to be called with the result of this Promise when this
     * Promise is resolved successfully. The callback will not be called if this
     * Promise is resolved with a failure.
     * <p>
     * This method may be called at any time including before and after this Promise
     * has been resolved.
     * <p>
     * Resolving this Promise <i>happens-before</i> any registered callback is
     * called. That is, in a registered callback, {@link #isDone()} must return
     * {@code true} and {@link #getValue()} and {@link #getFailure()} must not
     * block.
     * <p>
     * A callback may be called on a different thread than the thread which
     * registered the callback. So the callback must be thread safe but can rely
     * upon that the registration of the callback <i>happens-before</i> the
     * registered callback is called.
     *
     * @param success The Consumer callback that receives the value of this Promise.
     *                Must not be {@code null}.
     * @return This Promise.
     * @since 1.1
     */
    public Promise<T> onSuccess(Consumer<? super T> success) {
        return onResolving(new CallBackPromise<T>(success, this, CallBackPromise.SUCCESS));
    }

    /**
     * Register a callback to be called with the failure for this Promise when this
     * Promise is resolved with a failure. The callback will not be called if this
     * Promise is resolved successfully.
     * <p>
     * This method may be called at any time including before and after this Promise
     * has been resolved.
     * <p>
     * Resolving this Promise <i>happens-before</i> any registered callback is
     * called. That is, in a registered callback, {@link #isDone()} must return
     * {@code true} and {@link #getValue()} and {@link #getFailure()} must not
     * block.
     * <p>
     * A callback may be called on a different thread than the thread which
     * registered the callback. So the callback must be thread safe but can rely
     * upon that the registration of the callback <i>happens-before</i> the
     * registered callback is called.
     *
     * @param failure The Consumer callback that receives the failure of this
     *                Promise. Must not be {@code null}.
     * @return This Promise.
     * @since 1.1
     */
    public Promise<T> onFailure(Consumer<? super Throwable> failure) {
        return onResolve(new CallBackPromise<T>(failure, this, CallBackPromise.FAILURE));
    }

    /**
     * Fail a Promise.
     * 
     * @param e the e
     * @return the promise
     */
    public Promise<T> fail(Throwable e) {
        this.fail = e;
        return notifyCallbacks();
    }

    /**
     * Resolve a Promise.
     * 
     * @param value the value
     * @return the promise
     */
    public Promise<T> resolve(T value) {
        resolving(value);
        this.resolved.countDown();
        notifyCallbacks();
        return this;
    }

    /**
     * Resolving the Promise.
     * 
     * @param value the value
     */
    void resolving(T value) {
        this.value = value;
    }

    /**
     * Change PromiseFactory
     * 
     * @param factory the factory
     * @return the promise
     */
    public Promise<T> withFactory(PromiseFactory factory) {
        this.factory = factory;
        return this;
    }
    
    /**
     * Gets the ExecuteFactory.
     * @return the factory
     */
    public PromiseFactory getFactory() {
      return factory;
   }

    /**
     * Create new promise.
     *
     * @param <T>     the generic type
     * @param factory the factory
     * @return the promise
     */
    public static <T> Promise<T> newPromise(PromiseFactory factory) {
        return new Promise<T>().withFactory(factory);
    }

    /**
     * Map Worker for Promise.
     *
     * @param <R>    the generic type
     * @param mapper the mapper
     * @return the promise
     */
    public <R> Promise<R> map(final Function<? super T, R> mapper) {
        final Promise<R> promise = new Promise<R>().withFactory(factory);
        onSuccess(new PromiseConsumer<T, R>(promise, mapper));
        return promise;
    }

    /**
     * Recover from a failure of this Promise with a recovery value.
     *
     * <p>
     * If this Promise is resolved with a failure, the specified Function is applied
     * to this Promise to produce a recovery value.
     * <ul>
     * <li>If the recovery value is not {@code null}, the returned Promise must be
     * resolved with the recovery value.</li>
     * <li>If the recovery value is {@code null}, the returned Promise must be
     * failed with the failure of this Promise.</li>
     * <li>If the specified Function throws an exception, the returned Promise must
     * be failed with that exception.</li>
     * </ul>
     *
     * <p>
     * This method may be called at any time including before and after this Promise
     * has been resolved.
     *
     * @param recovery If this Promise resolves with a failure, the specified
     *                 Function is called to produce a recovery value to be used to
     *                 resolve the returned Promise. Must not be {@code null}.
     * @param filterThrowable Filter for Exceptions
     * @return A Promise that resolves with the value of this Promise or recovers
     *         from the failure of this Promise.
     */
    public Promise<T> recover(Function<Throwable, T> recovery, Class<?>... filterThrowable) {
        onFailure(new PromiseConsumer<Throwable, T>(this, recovery, filterThrowable));
        return this;
    }

    /**
     * Filter for Promise.
     *
     * @param predicate the predicate
     * @return the promise
     */
    public Promise<T> filter(Function<? super T, Boolean> predicate) {
        PromiseConsumer<T, Boolean> promiseConsumer = new PromiseConsumer<T, Boolean>(this.callBacks, predicate);
        onSuccess(promiseConsumer);
        return this;
    }
}
