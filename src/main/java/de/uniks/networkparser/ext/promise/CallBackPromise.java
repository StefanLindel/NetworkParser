package de.uniks.networkparser.ext.promise;

import java.util.function.Consumer;
import java.util.function.Supplier;

import de.uniks.networkparser.interfaces.PromiseFactory;

/**
 * The Class CallBackPromise for Run after resolving Promise.
 *
 * @author Stefan
 * @param <T> the generic type
 */
public class CallBackPromise<T> implements Runnable, Supplier<T> {
    /** The Constant SUCCESS. */
    public static final String SUCCESS = "SUCCESS";
    /** The Constant FAILURE. */
    public static final String FAILURE = "FAILURE";
    private final Object callback;
    private final Promise<T> promise;
    private final String type;

    /**
     * Instantiates a new call back promise.
     *
     * @param callback the callback
     * @param promise the promise
     * @param type the type
     */
    public CallBackPromise(Consumer<?> callback, Promise<T> promise, String type) {
        this.callback = callback;
        this.promise = promise;
        this.type = type;
    }

    /**
     * Instantiates a new call back promise.
     *
     * @param callback the callback
     * @param promise the promise
     */
    public CallBackPromise(Object callback, Promise<T> promise) {
        this.callback = callback;
        this.promise = promise;
        this.type = null;
    }

    /** The Execution. */
    @SuppressWarnings("unchecked")
    @Override
    public void run() {
        try {
            if (callback instanceof Consumer<?>) {
                if (promise.getFailure() == null && SUCCESS.equalsIgnoreCase(type)) {
                    ((Consumer<T>) callback).accept(promise.getValue());
                } else if (FAILURE.equalsIgnoreCase(type)) {
                    ((Consumer<? super Throwable>) callback).accept(promise.getFailure());
                }
            } else if (callback instanceof Runnable) {
                ((Runnable) callback).run();
            }
        } catch (Throwable e) {
            promise.fail(e);
        }
    }

    /**
     * Gets the type.
     * @return the type
     */
    public String getType() {
        return type;
    }

    @Override
    public T get() {
        try {
            return promise.getValue();
        } catch (InterruptedException ex) {
           Thread.currentThread().interrupt();
           PromiseFactory factory = promise.getFactory();
           if(factory!= null && factory.getLogger() != null) {
              factory.getLogger().error(this, "get", ex);
           }
        } catch (Exception ex) {
           PromiseFactory factory = promise.getFactory();
           if(factory!= null && factory.getLogger() != null) {
              factory.getLogger().error(this, "get", ex);
           }
        }
        return null;
    }

    @Override
    public String toString() {
        return this.type;
    }
}
