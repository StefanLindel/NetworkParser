package de.uniks.networkparser.ext;

import java.util.Map;

import de.uniks.networkparser.ext.generic.ReflectionLoader;
import de.uniks.networkparser.list.SimpleKeyValueList;

/**
 * The Class BeanClasses.
 *
 * @author Stefan
 */
public class BeanClasses {
    /**
     * Singleton of Bean.
     */
    public static BeanClasses instance = new BeanClasses();

    /**
     * The entities.
     */
    public static Map<Class<?>, SimpleKeyValueList<Class<?>, Integer>> entities =
            new SimpleKeyValueList<>();

    /**
     * Adds the sub class.
     *
     * @param clazz    the clazz
     * @param priority the priority
     * @return the int
     */
    public static int addSubClass(String clazz, int priority) {
        Class<?> simpleClazz = ReflectionLoader.getClass(clazz);
        if (simpleClazz != null) {
            for (Class<?> interfaze : simpleClazz.getInterfaces()) {
                addSubClass(interfaze, simpleClazz, 1);
            }
            addSuperClass(simpleClazz.getSuperclass(), simpleClazz, priority);
        }
        return priority;
    }

    /**
     * Adds the super class.
     *
     * @param clazz    the clazz
     * @param instance the instance
     * @param priority the priority
     * @return the int
     */
    public static int addSuperClass(Class<?> clazz, Class<?> instance, int priority) {
        if (clazz != null) {
            for (Class<?> interfaze : clazz.getInterfaces()) {
                addSubClass(interfaze, instance, priority);
            }
            addSuperClass(clazz.getSuperclass(), instance, priority);
        }
        return 1;
    }

    /**
     * Adds the sub class.
     *
     * @param clazz the clazz
     * @return the int
     */
    public static int addSubClass(Class<?> clazz) {
        if (clazz != null) {
            for (Class<?> interfaze : clazz.getInterfaces()) {
                addSubClass(interfaze, clazz, 1);
            }
        }
        return 1;
    }

    /**
     * Adds the sub class.
     *
     * @param interfaze the interfaze
     * @param clazz     the clazz
     * @return the int
     */
    public static int addSubClass(Class<?> interfaze, Class<?> clazz) {
        return addSubClass(interfaze, clazz, 1);
    }

    /**
     * Adds the sub class.
     *
     * @param interfaze the interfaze
     * @param clazz     the clazz
     * @param priority  the priority
     * @return the int
     */
    public static int addSubClass(Class<?> interfaze, Class<?> clazz, int priority) {
        SimpleKeyValueList<Class<?>, Integer> entity = entities.get(interfaze);
        if (entity == null) {
            entity = new SimpleKeyValueList<>();
            entities.put(interfaze, entity);
        }
        for (int i = 0; i < entity.size(); i++) {
            if (entity.getValueByIndex(i) < priority) {
                entity.addKeyValue(i, clazz, priority);
                return priority;
            }
        }
        entity.put(clazz, priority);
        return priority;
    }

    /**
     * New instance.
     *
     * @param <T>       the generic type
     * @param interfaze the interfaze
     * @param arguments the arguments
     * @return the t
     */
    @SuppressWarnings("unchecked")
    public static <T> T newInstance(Class<T> interfaze, Object... arguments) {
        SimpleKeyValueList<Class<?>, Integer> entity = entities.get(interfaze);
        if (entity != null) {
            Class<?> instanze = entity.first();
      return (T) ReflectionLoader.newInstance(instanze, arguments);
    }
    return null;
  }
}
