package de.uniks.networkparser.ext.generic;

import java.util.List;
import java.util.Map.Entry;

import de.uniks.networkparser.list.SimpleKeyValueList;
import de.uniks.networkparser.list.SimpleList;

/**
 * A factory for creating SimpleValueConverter objects.
 * @author Stefan Lindel
 * @param <T> the generic type
 * @param <P> the generic type
 */
public class SimpleValueConverterFactory<T, P> {
  private final SimpleKeyValueList<T, P> map = new SimpleKeyValueList<T, P>();
  private boolean ignoreLengthValidator;
  
  /**
   * Adds a pair to mapping.
   *
   * @param key the key
   * @param value the value
   * @return the simple value converter factory
   */
  public SimpleValueConverterFactory<T, P> add(T key, P value) {
     this.map.put(key, value);
     return this;
  }
      
  /**
   * Unsafe value length.
   *
   * @return the simple value converter factory
   */
  public SimpleValueConverterFactory<T, P> unsafeValueLength() {
     this.ignoreLengthValidator = true;
     return this;
  }

  /**
   * Builds the Mapping.
   *
   * @return the simple key value list
   */
  public SimpleKeyValueList<T, P> build() {
     if(!validate()) {
        return null;
        }
     return map;
  }

  /**
   * Builds the uni directional Mappping. 
   *
   * @return the simple key value list
   */
  public SimpleKeyValueList<T, P> buildUniDirectional() {
     if(map.isEmpty()) {
        return null;
     }
     return map;
  }

  /**
   * Validate.
   *
   * @return true, if successful
   */
  public boolean validate() {
     if (this.ignoreLengthValidator) {
        return true;
     }
     if (map.isEmpty()) {
      return false;
     }
     Entry<T, P> firstEntry = map.entrySet().iterator().next();
     return validateEnumClass(firstEntry.getKey().getClass()) 
      && validateEnumClass(firstEntry.getValue().getClass())
      &&  map.values().size() == map.size();
  }

  private boolean validateEnumClass(Class<?> enumClass) {
     if (!enumClass.isEnum()) {
      return true;
     }
     return enumClass.getEnumConstants().length == map.size();
  }
      
  /**
   * autobuild Of Enum.
   *
   * @param enumA the enum A
   * @param values the values
   * @return the simple value converter factory
   */
  @SuppressWarnings("unchecked") 
  public SimpleValueConverterFactory<T, P> of(Class<T> enumA, P... values) {
     T[] enumConstantsA = enumA.getEnumConstants();
     if (enumConstantsA.length != values.length) {
        return null;
     }
     for (int i = 0; i < enumConstantsA.length; i++) {
        add(enumConstantsA[i], values[i]);
     }
     return this;
  }

  /**
   * Auto build Mapping.
   *
   * @param <T> the generic type
   * @param <P> the generic type
   * @param enumA the enum A
   * @param enumB the enum B
   * @return the simple key value list
   */
  @SuppressWarnings("unchecked")
  public static <T,P> SimpleKeyValueList<T, P> autoBuild(Class<T> enumA, Class<P> enumB) {
     T[] enumConstantsA = enumA.getEnumConstants();
     P[] enumConstantsB = enumB.getEnumConstants();
     if (enumConstantsA.length != enumConstantsB.length) {
        return null;
     }
     SimpleValueConverterFactory<T,P> builder = new SimpleValueConverterFactory<>();
     if(enumConstantsA.length != enumConstantsA.length || enumConstantsB.length != enumConstantsB.length) {
        return null;
     }
     List<P> enumBList = new SimpleList<P>().with(enumConstantsB);
     for (int i = 0; i < enumConstantsA.length; i++) {
        String enumString = enumConstantsA[i].toString();
        Object element = removeFromEnum(enumBList, enumString);
        if (element == null) {
           return null;
        }
        builder.add(enumConstantsA[i], (P) element);
     }
     return builder.build();
  }
      
  private static Object removeFromEnum(List<?> enumBList, String search) {
     for (int i = 0; i < enumBList.size(); i++) {
        if (enumBList.get(i).toString().equalsIgnoreCase(search)) {
           return enumBList.remove(i);
        }
     }
     return null;
  }
}
