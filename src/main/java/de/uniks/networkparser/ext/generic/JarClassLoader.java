package de.uniks.networkparser.ext.generic;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;

/**
 * The Class JarClassLoader.
 *
 * @author Stefan
 */
public class JarClassLoader extends URLClassLoader {
    /**
     * Instantiates a new jar class loader.
     */
    public JarClassLoader() {
        super(new URL[] {});
    }

    /**
     * Instantiates a new jar class loader.
     *
     * @param parentClassloader the parent classloader
     * @param urls              the urls
     */
    public JarClassLoader(ClassLoader parentClassloader, URL... urls) {
        super(urls, parentClassloader);
    }

    /**
     * Instantiates a new jar class loader.
     *
     * @param urls the urls
     */
    public JarClassLoader(URL... urls) {
        super(urls);
    }

    /**
     * Adds the URL.
     *
     * @param url the url
     */
    @Override
    public void addURL(URL url) {
        super.addURL(url);
    }
    
    /**
     * Add File to Classloader
     * @param fileName add new File to Classloader
     * @return success
     */
    public boolean addFile(String fileName) {
	File file = new File(fileName);
	if(!file.exists()) {
	    return false;
	}
	try {
	    super.addURL(file.toURI().toURL());
	    return true;
	} catch (MalformedURLException e) {
	}
	return false;
    }
}
