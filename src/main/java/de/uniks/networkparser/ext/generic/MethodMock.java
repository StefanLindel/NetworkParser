package de.uniks.networkparser.ext.generic;

/**
 * Mock for a Simple Method
 *
 * @author LINDEL
 */
public class MethodMock {
    /**
     * Call Super Method (Method of Spy-Instance)
     */
    public void callSuper() {
    }

}
