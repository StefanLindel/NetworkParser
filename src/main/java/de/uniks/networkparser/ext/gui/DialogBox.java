package de.uniks.networkparser.ext.gui;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.swing.*;

import de.uniks.networkparser.ext.Os;
import de.uniks.networkparser.ext.generic.ReflectionLoader;
import de.uniks.networkparser.ext.io.StringPrintStream;
import de.uniks.networkparser.gui.controls.Button;
import de.uniks.networkparser.gui.controls.Control;
import de.uniks.networkparser.gui.controls.Label;
import de.uniks.networkparser.gui.controls.TextField;
import de.uniks.networkparser.interfaces.ObjectCondition;
import de.uniks.networkparser.list.SimpleSet;

/**
 * The Class DialogBox.
 *
 * @author Stefan
 */
public class DialogBox implements ObjectCondition {

    /**
     * The Constant TOOLKIT_JAVAFX.
     */
    public static final String TOOLKIT_JAVAFX = "JAVAFX";

    /**
     * The Constant TOOLKIT_WEB.
     */
    public static final String TOOLKIT_WEB = "WEB";

    /**
     * The Constant TOOLKIT_AWT.
     */
    public static final String TOOLKIT_AWT = "AWT";

    /**
     * The Constant TimedWindowEvent.
     */
    public static final String TimedWindowEvent = "sun.awt.TimedWindowEvent";

    protected static final int HEADER_HEIGHT = 28;
    protected static final URL DIALOGS_CSS_URL = DialogBox.class.getResource("dialogs.css");
    protected static final Object ACTIVE_PSEUDO_CLASS = ReflectionLoader.call(ReflectionLoader.PSEUDOCLASS,
            "getPseudoClass", "active");
    private final SimpleSet<Control> titleElements = new SimpleSet<Control>();
    private final SimpleSet<Control> actionElements = new SimpleSet<Control>();
    private final Label titleElement = new Label().withType(Label.TITLE);
    protected Object dialogTitleBar;
    boolean modal = true;
    Button action;
    private String toolkit = TOOLKIT_JAVAFX;
    private double mouseDragDeltaY;
    private double mouseDragDeltaX;
    private Object root;
    private Object stage;
    private Object originalParent;
    private Object owner;
    /* Inline Show */
    private boolean isInline;

    private boolean iconified;
    private Object center;

    /**
     * Instantiates a new dialog box.
     */
    public DialogBox() {
        titleElements.add(titleElement);
        titleElements.add(new Label().withType(Label.SPACER));
        titleElements.add(new Button().withActionType(Button.CLOSE, this));
        /*
         * minButton = new DialogButton().withGrafik(Grafik.minimize).withStage(stage);
         */
        /*
         * maxButton = new DialogButton().withGrafik(Grafik.maximize).withStage(stage);
         */
    }

    /**
     * Show info.
     *
     * @param parent the parent
     * @param title  the title
     * @param text   the text
     * @param inLine the in line
     * @return the button
     */
    public static Button showInfo(Object parent, String title, String text, boolean inLine) {
        DialogBox dialogBox = new DialogBox().withTitle(title).withCenterInfo(text).withInline(inLine);
        return dialogBox.withActionButton(new Button().withActionType(Button.CLOSE, dialogBox).withValue("OK"))
                .show(parent);
    }

    /**
     * Show info.
     *
     * @param title the title
     * @param text  the text
     * @return the button
     */
    public static Button showInfo(String title, String text) {
        DialogBox dialogBox = new DialogBox().withTitle(title).withCenterInfo(text);
        return dialogBox.withActionButton(new Button().withValue("OK").withActionType(Button.CLOSE, dialogBox))
                .show(null);
    }

    /**
     * Show question.
     *
     * @param parent the parent
     * @param title  the title
     * @param text   the text
     * @return the button
     */
    public static Button showQuestion(Object parent, String title, String text) {
        DialogBox dialogBox = new DialogBox().withTitle(title).withCenterInfo(text);
        return dialogBox.withActionButton(new Button().withValue("Yes").withActionType(Button.CLOSE, dialogBox),
                new Button().withValue("No").withActionType(Button.CLOSE, dialogBox)).show(parent);
    }

    /**
     * Show question check.
     *
     * @param parent the parent
     * @param title  the title
     * @param text   the text
     * @param check  the check
     * @return true, if successful
     */
    public static boolean showQuestionCheck(Object parent, String title, String text, String... check) {
        Button action = showQuestion(parent, title, text);
        if (action == null) {
            return false;
        }
        for (String item : check) {
            if (item != null) {
                if (item.equalsIgnoreCase(action.getValue())) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Show file save chooser.
     *
     * @param caption       the caption
     * @param defaultValue  the default value
     * @param typeName      the type name
     * @param typeExtension the type extension
     * @param parent        the parent
     * @return the string
     */
    public static String showFileSaveChooser(String caption, String defaultValue, String typeName, String typeExtension,
            Object... parent) {
        return showFileChooser("save", caption, defaultValue, typeName, typeExtension, parent);
    }

    /**
     * Show file chooser.
     *
     * @param art          the art
     * @param caption      the caption
     * @param defaultValue the default value
     * @param typeName     the type name
     * @param extensions   the extensions
     * @param parent       the parent
     * @return the string
     */
    @SuppressWarnings("unchecked")
    public static String showFileChooser(String art, String caption, String defaultValue, String typeName,
            String extensions, Object... parent) {
        Object parentObj = null;
        if (parent != null && parent.length > 0) {
            parentObj = parent[0];
        }
        if (typeName != null) {
            typeName += " (*." + extensions + ")";
        }
        File result;
        if (ReflectionLoader.FILECHOOSERFX != null) {
            /* try JavaFX Dialog */
            Object fileChooser = ReflectionLoader.newInstance(ReflectionLoader.FILECHOOSERFX);
            ReflectionLoader.call(fileChooser, "setTitle", caption);
            ReflectionLoader.call(fileChooser, "setInitialFileName", defaultValue);
            if (typeName != null) {
                Class<?> filterClass = ReflectionLoader.getClass("javafx.stage.FileChooser$ExtensionFilter");
                Object filter = ReflectionLoader.newInstance(filterClass, String.class, typeName, String[].class,
                        new String[] { "*." + extensions });
                List<Object> list = (List<Object>) ReflectionLoader.call(fileChooser, "getExtensionFilters");
                list.add(filter);
            }
            Class<?> windowClass = ReflectionLoader.getClass("javafx.stage.Window");

            if ("save".equals(art)) {
                result = (File) ReflectionLoader.call(fileChooser, "showSaveDialog", windowClass, parentObj);
            } else {
                result = (File) ReflectionLoader.call(fileChooser, "showOpenDialog", windowClass, parentObj);
            }
            if (result != null) {
                return result.getAbsolutePath();
            }
        } else {
            /* SWING??? */
            if (ReflectionLoader.JFRAME == null || Os.isHeadless()) {
                return null;
            }
            ReflectionLoader.logger = new StringPrintStream();
            if (parentObj == null || !ReflectionLoader.JFRAME.isAssignableFrom(parentObj.getClass())) {
                parentObj = ReflectionLoader.newInstance(ReflectionLoader.JFRAME);
            }
            Object fileChooser = ReflectionLoader.newInstance(ReflectionLoader.JFILECHOOSER);
            ReflectionLoader.call(fileChooser, "setDialogTitle", caption);
            int userSelection = -1;
            if (defaultValue != null) {
                ReflectionLoader.call(fileChooser, "setSelectedFile", new File(defaultValue));
            }
            if (typeName != null) {
                Class<?> filterClass = ReflectionLoader.getClass("javax.swing.filechooser.FileNameExtensionFilter");
                Class<?> fileFilter = ReflectionLoader.getClass("javax.swing.filechooser.FileFilter");
                Object filter = ReflectionLoader.newInstance(filterClass, String.class, typeName, String[].class,
                        new String[] { extensions });
                ReflectionLoader.call(fileChooser, "setFileFilter", fileFilter, filter);
            }
            Class<?> componentClass = ReflectionLoader.getClass("java.awt.Component");
            if (componentClass == null) {
                return null;
            }
            if ("save".equals(art)) {
                userSelection = (Integer) ReflectionLoader.call(fileChooser, "showSaveDialog", componentClass,
                        parentObj);
            } else {
                userSelection = (Integer) ReflectionLoader.call(fileChooser, "showOpenDialog", componentClass,
                        parentObj);
            }
            if (userSelection == 0) {
                File fileToSave = (File) ReflectionLoader.call(fileChooser, "getSelectedFile");
                return fileToSave.getAbsolutePath();
            }
        }
        return null;
    }

    /**
     * Gets the input string.
     *
     * @param param the param
     * @return the input string
     */
    public static String getInputString(String... param) {
        String title = "Eingabe";
        String info = null;
        if (param != null) {
            if (param.length > 1) {
                title = param[1];
            }
            if (param.length > 0) {
                info = param[0];
            }
        }

        TextField textField = new TextField();
        DialogBox dialogBox = new DialogBox().withTitle(title).withCenterInfo(info).withCenter(textField);
        dialogBox.toolkit = TOOLKIT_AWT;
        Button actionButton = dialogBox
                .withActionButton(new Button().withValue("OK").withActionType(Button.CLOSE, dialogBox),
                        new Button().withValue("Abbrechen").withActionType(Button.CLOSE, dialogBox))
                .show(null);
        if (actionButton != null && actionButton.getValue().equalsIgnoreCase("OK")) {
            return textField.getValue();
        }
        return null;
    }

    /**
     * The main method.
     *
     * @param args the arguments
     */
    public static void main(String[] args) {
        DialogBox.getInputString();
    }

    /**
     * With title.
     *
     * @param value the value
     * @return the dialog box
     */
    public DialogBox withTitle(String value) {
        if (value != null) {
            titleElement.setValue(value);
        }
        return this;
    }

    /**
     * Show.
     *
     * @param owner the owner
     * @return the button
     */
    public Button show(Object owner) {
        if (titleElement.length() < 1) {
            Object title = ReflectionLoader.call(owner, "getTitle");
            if (title != null) {
                titleElement.setValue((String) title);
            }
        }
        if (isInline) {
            return showIntern(owner);
        }
        return showExtern(owner);

    }

    @SuppressWarnings("unchecked")
    private Button showIntern(Object parent) {
        Object scene;
        this.stage = parent;
        scene = ReflectionLoader.call(stage, "getScene");
        configScene();

        /* modify scene root to install opaque layer and the dialog */
        originalParent = ReflectionLoader.call(scene, "getRoot");

        createContent();

        DialogPane myPane = new DialogPane(this, originalParent);

        ReflectionLoader.call(root, "pseudoClassStateChanged", ReflectionLoader.PSEUDOCLASS, ACTIVE_PSEUDO_CLASS,
                boolean.class, true);

        /* add to originalParent */
        ReflectionLoader.call(scene, "setRoot", ReflectionLoader.PARENT, myPane.getPane());

        ReflectionLoader.call(root, "setVisible", boolean.class, true);
        if (originalParent != null) {
            JavaBridgeFX.addChildren(myPane.getPane(), 0, originalParent);
            Map<Object, Object> properties = (Map<Object, Object>) ReflectionLoader.call(originalParent,
                    "getProperties");

            Map<Object, Object> dialogProperties = (Map<Object, Object>) ReflectionLoader.call(myPane.getPane(),
                    "getProperties");
            dialogProperties.putAll(properties);
        }
        ReflectionLoader.call(root, "requestFocus");

        JavaAdapter.execute(myPane);
        return null;
    }

    /**
     * Hide.
     *
     * @param value the value
     */
    public void hide(Button value) {
        if (this.action == null) {
            this.setAction(value);
        }
    }

    /**
     * Minimize.
     */
    public void minimize() {
        if (stage != null) {
            ReflectionLoader.call(stage, "setIconified", this.iconified);
        }
    }

    /**
     * Maximize.
     */
    public void maximize() {
        if (stage != null) {
            ReflectionLoader.call(stage, "setFullScreen", true);
        }
    }

    protected double getOverlayWidth() {
        if (owner != null) {
            return (Double) ReflectionLoader.callChain(owner, "getLayoutBounds", "getWidth");
        } else if (stage != null) {
            return (Double) ReflectionLoader.callChain(stage, "getScene", "getWidth");
        }

        return 0;
    }

    protected double getOverlayHeight() {
        if (owner != null) {
            return (Double) ReflectionLoader.callChain(owner, "getLayoutBounds", "getHeight");
        } else if (stage != null) {
            return (Double) ReflectionLoader.callChain(stage, "getScene", "getHeight");
        }

        return 0;
    }

    private Button showExtern(Object owner) {
        Object toolKit = ReflectionLoader.call(ReflectionLoader.TOOLKITFX, "getToolkit");
        Object isFX = ReflectionLoader.call(toolKit, "isFxUserThread");
        if (isFX != null && (Boolean) isFX) {
            new DialogStage(this, owner).run();
            return action;
        }
        if (TOOLKIT_AWT.equals(this.toolkit)) {
            return showExternAWT(owner);
        }
        JavaAdapter.execute(new DialogStage(this, owner));
        return null;
    }

    private Button showExternAWT(Object owner) {
        java.awt.Dialog dialog = new java.awt.Dialog(null, java.awt.Dialog.ModalityType.APPLICATION_MODAL);
        JPanel contentPane = new JPanel();
        root = dialog;
        dialog.add(contentPane);

        Object layout = ReflectionLoader.newInstance("java.awt.BorderLayout");
        GUIEvent event = new GUIEvent();
        event.withListener(this);

        ReflectionLoader.call(contentPane, "setLayout", ReflectionLoader.getSimpleClass("java.awt.LayoutManager"),
                layout);

        dialog.setTitle(this.titleElement.getValue());
        dialog.setSize(400, 100);
        java.awt.TextField field = new java.awt.TextField();

        JPanel actionPane = new JPanel();
        Class<?> actionListenerClazz = ReflectionLoader.getClass("java.awt.event.ActionListener");
        Class<?> windowListenerClazz = ReflectionLoader.getClass("java.awt.event.WindowListener");
        Object listener = ReflectionLoader.createProxy(event, actionListenerClazz, windowListenerClazz);

        for (Control control : this.actionElements) {
            Object btn = ReflectionLoader.newInstance(ReflectionLoader.AWTBUTTON, "" + control.getValue());
            ReflectionLoader.call(btn, "addActionListener", actionListenerClazz, listener);
            ReflectionLoader.call(actionPane, "add", String.class, java.awt.BorderLayout.EAST,
                    ReflectionLoader.AWTCOMPONENT, btn);
        }
        contentPane.add(java.awt.BorderLayout.CENTER, field);
        contentPane.add(java.awt.BorderLayout.SOUTH, actionPane);
        ReflectionLoader.call(dialog, "addWindowListener", windowListenerClazz, listener);

        dialog.setVisible(true);
        return action;
    }

    /**
     * With inline.
     *
     * @param value the value
     * @return the dialog box
     */
    public DialogBox withInline(boolean value) {
        this.isInline = value;
        return this;
    }

    /**
     * With modal.
     *
     * @param modal the modal
     * @return the dialog box
     */
    public DialogBox withModal(boolean modal) {
        this.modal = modal;
        return this;
    }

    @SuppressWarnings("unchecked")
    void configScene() {
        if (stage == null) {
            return;
        }
        Object scene = ReflectionLoader.call(stage, "getScene");
        if (DIALOGS_CSS_URL == null) {
            return;
        }
        String dialogsCssUrl = DIALOGS_CSS_URL.toExternalForm();
        if (scene == null && owner != null) {
            scene = ReflectionLoader.call(owner, "getScene");
        }
        if (scene != null) {
            /* install CSS */
            Object styleSheet = ReflectionLoader.call(scene, "getStylesheets");
            if (styleSheet instanceof List<?>) {
                List<String> list = (List<String>) styleSheet;
                if (!list.contains(dialogsCssUrl)) {
                    list.add(dialogsCssUrl);
                }
            }
        }
    }

    /**
     * Creates the content.
     *
     * @return the dialog box
     */
    public DialogBox createContent() {
        if (Os.isReflectionTest()) {
            return this;
        }
        root = ReflectionLoader.newInstance(ReflectionLoader.BORDERPANE);
        JavaBridgeFX.setStyle(root, false, "dialog", "decorated-root");

        Object property = ReflectionLoader.call(stage, "focusedProperty");
        JavaBridgeFX.addListener(property, "addListener", ReflectionLoader.CHANGELISTENER, this);

        /* --- titlebar (only used for cross-platform look) */
        dialogTitleBar = ReflectionLoader.newInstance(ReflectionLoader.TOOLBAR);
        JavaBridgeFX.setStyle(dialogTitleBar, false, "window-header");
        ReflectionLoader.call(dialogTitleBar, "setPrefHeight", double.class, HEADER_HEIGHT);
        ReflectionLoader.call(dialogTitleBar, "setMinHeight", double.class, HEADER_HEIGHT);
        ReflectionLoader.call(dialogTitleBar, "setMaxHeight", double.class, HEADER_HEIGHT);
        for (Control element : titleElements) {
            Object guiElement = JavaBridgeFX.convert(element, true);
            JavaBridgeFX.addChildren(dialogTitleBar, -1, guiElement);
        }

        JavaBridgeFX.addListener(dialogTitleBar, "setOnMousePressed", ReflectionLoader.EVENTHANDLER, this);

        JavaBridgeFX.addListener(dialogTitleBar, "setOnMouseDragged", ReflectionLoader.EVENTHANDLER, this);

        ReflectionLoader.call(root, "setTop", ReflectionLoader.NODE, dialogTitleBar);
        ReflectionLoader.call(root, "setCenter", ReflectionLoader.NODE, center);
        if (this.actionElements.size() > 0) {
            Object actionToolbar = ReflectionLoader.newInstance(ReflectionLoader.HBOX);
            JavaBridgeFX.setStyle(actionToolbar, false, "window-action");
            for (Control item : this.actionElements) {
                Object guiElement = JavaBridgeFX.convert(item, false);
                JavaBridgeFX.addChildren(actionToolbar, -1, guiElement);
            }
            Object pos = ReflectionLoader.getField(ReflectionLoader.POS, "TOP_RIGHT");
            ReflectionLoader.call(actionToolbar, "setAlignment", ReflectionLoader.POS, pos);
            ReflectionLoader.call(root, "setBottom", ReflectionLoader.NODE, actionToolbar);
        }
        return this;
    }

    /**
     * With center.
     *
     * @param node the node
     * @return the dialog box
     */
    public DialogBox withCenter(Object node) {
        this.center = node;
        return this;
    }

    /**
     * With title button.
     *
     * @param index the index
     * @param value the value
     * @return the dialog box
     */
    public DialogBox withTitleButton(int index, Control... value) {
        if (value == null) {
            return this;
        }
        ArrayList<Control> items = new ArrayList<Control>();
        for (Control item : value) {
            items.add(item);
        }
        this.titleElements.addAll(index, items);
        return this;

    }

    /**
     * With title button.
     *
     * @param value the value
     * @return the dialog box
     */
    public DialogBox withTitleButton(Control... value) {
        if (value == null) {
            return this;
        }
        for (Control item : value) {
            this.titleElements.add(item);
        }
        return this;
    }

    /**
     * With action button.
     *
     * @param index the index
     * @param value the value
     * @return the dialog box
     */
    public DialogBox withActionButton(int index, Control... value) {
        if (value == null) {
            return this;
        }
        ArrayList<Control> items = new ArrayList<Control>();
        for (Control item : value) {
            items.add(item);
        }
        this.actionElements.addAll(index, items);
        return this;

    }

    /**
     * With action button.
     *
     * @param value the value
     * @return the dialog box
     */
    public DialogBox withActionButton(Control... value) {
        if (value == null) {
            return this;
        }
        for (Control item : value) {
            this.actionElements.add(item);
        }
        return this;
    }

    /**
     * With center info.
     *
     * @param value the value
     * @return the dialog box
     */
    public DialogBox withCenterInfo(String value) {
        withCenterText("information.png", value);
        return this;
    }

    /**
     * With center question.
     *
     * @param value the value
     * @return the dialog box
     */
    public DialogBox withCenterQuestion(String value) {
        withCenterText("confirm.png", value);
        return this;
    }

    /**
     * With center text.
     *
     * @param image the image
     * @param value the value
     * @return the dialog box
     */
    public DialogBox withCenterText(String image, String value) {
        if (value == null) {
            return this;
        }
        if (Os.isReflectionTest()) {
            return this;
        }
        Object box = ReflectionLoader.newInstance(ReflectionLoader.HBOX);
        URL resource = DialogBox.class.getResource(image);
        if (resource == null) {
            return this;
        }
        Object imageView = ReflectionLoader.newInstance(ReflectionLoader.IMAGEVIEW, String.class, resource.toString());

        Object text = ReflectionLoader.newInstance(ReflectionLoader.LABEL, String.class, value);
        JavaBridgeFX.setStyle(text, false, "labelText");

        Object vBox = ReflectionLoader.newInstance(ReflectionLoader.VBOX);
        Object pos = ReflectionLoader.getField(ReflectionLoader.POS, "CENTER");
        ReflectionLoader.call(vBox, "setAlignment", ReflectionLoader.POS, pos);
        JavaBridgeFX.addChildren(vBox, -1, text);

        JavaBridgeFX.addChildren(box, -1, imageView, vBox);
        JavaBridgeFX.setStyle(box, false, "centerbox");
        this.center = box;
        return this;
    }

    /**
     * Gets the action.
     *
     * @return the action
     */
    public Button getAction() {
        return action;
    }

    /**
     * Sets the action.
     *
     * @param value the new action
     */
    public void setAction(Button value) {
        this.action = value;
        if (isInline) {
            /* hide the dialog */
            ReflectionLoader.call(root, "setVisible", boolean.class, false);
            /* reset the scene root */
            Object scene = ReflectionLoader.call(stage, "getScene");
            Object oldParent = ReflectionLoader.call(scene, "getRoot");

            JavaBridgeFX.removeChildren(oldParent, originalParent);
            JavaBridgeFX.removeStyle(originalParent, "root");
            ReflectionLoader.call(scene, "setRoot", ReflectionLoader.PARENT, originalParent);
            return;
        }
        if (stage != null) {
            ReflectionLoader.call(stage, "hide");
        }
    }

    /**
     * Update.
     *
     * @param event the event
     * @return true, if successful
     */
    @Override
    public boolean update(Object event) {
        if (event == null) {
            return false;
        }
        String name = event.getClass().getName();
        if (name.startsWith("javafx")) {
            return updateJavaFX(event);
        }
        if (name.startsWith("java.awt") || name.equalsIgnoreCase(TimedWindowEvent)) {
            return updateAWT(event);
        }
        if (event instanceof Boolean) {
            boolean active = (Boolean) event;
            ReflectionLoader.call(root, "pseudoClassStateChanged", ReflectionLoader.PSEUDOCLASS, ACTIVE_PSEUDO_CLASS,
                    boolean.class, active);
            return true;
        }

        if (!(event instanceof Button)) {
            return false;
        }
        Button btn = (Button) event;
        if (Button.CLOSE.equalsIgnoreCase(btn.getActionType())) {
            this.hide(btn);
        }
        if (Button.MINIMIZE.equalsIgnoreCase(btn.getActionType())) {
            this.minimize();
        }
        if (Button.MAXIMIZE.equalsIgnoreCase(btn.getActionType())) {
            this.maximize();
        }
        return true;
    }

    private boolean updateJavaFX(Object event) {
        double x = (Double) ReflectionLoader.call(event, "getSceneX");
        double y = (Double) ReflectionLoader.call(event, "getSceneY");
        String name = (String) ReflectionLoader.call(event, "getEventType", "getName");
        if (name == null || !name.startsWith("MOUSE-DRAG")) {
            mouseDragDeltaX = x;
            mouseDragDeltaY = y;
        } else {
            double eventX = x - mouseDragDeltaX;
            double eventY = y - mouseDragDeltaY;
            if (isInline) {
                double xNew = (Double) ReflectionLoader.call(root, "getLayoutX");
                double yNew = (Double) ReflectionLoader.call(root, "getLayoutY");
                ReflectionLoader.call(root, "setLayoutX", double.class, xNew + eventX);
                ReflectionLoader.call(root, "setLayoutY", double.class, yNew + eventY);
            } else {
                ReflectionLoader.call(stage, "setX", double.class, eventX);
                ReflectionLoader.call(stage, "setY", double.class, eventY);
            }

            return true;
        }
        return true;
    }

    private boolean updateAWT(Object event) {
        if (event.getClass().getName().equalsIgnoreCase(TimedWindowEvent)) {
            if (this.root != null) {
                ReflectionLoader.call(root, "dispose");
            }
            return true;
        }
        if (event.getClass().equals(ReflectionLoader.ACTIONEVENT)) {
            String value = (String) ReflectionLoader.call(event, "getActionCommand");
            for (Control control : this.actionElements) {
                if (control instanceof Button && value.equals(control.getValue())) {
                    this.action = (Button) control;
                    if (this.root != null) {
                        ReflectionLoader.call(root, "dispose");
                    }
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Pref width.
     *
     * @param value the value
     * @return the double
     */
    public double prefWidth(double value) {
        if (root != null) {
            return (Double) ReflectionLoader.call(root, "prefWidth", double.class, -1);
        }
        return -1;
    }

    /**
     * Pref height.
     *
     * @param value the value
     * @return the double
     */
    public double prefHeight(double value) {
        if (root != null) {
            return (Double) ReflectionLoader.call(root, "prefHeight", double.class, -1);
        }
        return -1;
    }

    /**
     * Sets the stage.
     *
     * @param newStage the new stage
     */
    public void setStage(Object newStage) {
        this.stage = newStage;
    }

    /**
     * Gets the root.
     *
     * @return the root
     */
    public Object getRoot() {
        return root;
    }

    /**
     * Checks if is model.
     *
     * @return true, if is model
     */
    public boolean isModel() {
        return this.modal;
    }

    /**
     * Gets the scene.
     *
     * @return the scene
     */
    public Object getScene() {
        return ReflectionLoader.call(stage, "getScene");
    }
}
