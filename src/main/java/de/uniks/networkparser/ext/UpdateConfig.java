package de.uniks.networkparser.ext;

import de.uniks.networkparser.SendableItem;
import de.uniks.networkparser.StringUtil;
import de.uniks.networkparser.interfaces.SendableEntityCreator;
import de.uniks.networkparser.logic.VersionCondition;

/**
 * Configuration for Checking Program for Update
 *
 * @author Stefan
 */
public class UpdateConfig extends SendableItem implements SendableEntityCreator {
    /**
     * The Constant PROPERTY_CMD.
     */
    public static final String PROPERTY_CMD = "cmd";
    /**
     * The Constant PROPERTY_REPLACE.
     */
    public static final String PROPERTY_FILENAME = "filename";
    /**
     * CONSTANT FOR CURRENT-VERISON
     */
    public static final String PROPERTY_CURRENTVERSION = "version";
    /**
     * CONSTANT FOR INTERVAL FOR CHECKING
     */
    public static final String PROPERTY_INTERVAL = "interval";
    /**
     * CONSTANT FOR CURRENT DATE OF VERSION
     */
    public static final String PROPERTY_DATETIME = "datetime";
    /**
     * Constant for UPDATE_COMMAND
     */
    public static final String PROPERTY_UPDATECOMMAND = "updatecmd";
    /**
     * The Constant properties.
     */
    public static final String[] properties = new String[]{PROPERTY_CMD, PROPERTY_UPDATECOMMAND,
            PROPERTY_FILENAME, PROPERTY_CURRENTVERSION, PROPERTY_INTERVAL, PROPERTY_DATETIME};
    private String executeCommand;
    private String updateCommand;
    private VersionCondition currentVersion;
    private String fileName;
    private String dateTime;
    private int intervalInS;


    /**
     * Create a new Instance
     *
     * @return Create an new Instance of UpdateConfig
     */
    public static UpdateConfig createDefault() {
        return new UpdateConfig().withIntervalInS(1).withCurrentVersion("1.0");
    }

    /**
     * Gets the Command.
     *
     * @return the Command
     */
    public String getCmd() {
        return executeCommand;
    }

    /**
     * With Command.
     *
     * @param value the Command
     * @return the Command
     */
    public UpdateConfig withCommand(String value) {
        setCommand(value);
        return this;
    }

    /**
     * Sets the Command.
     *
     * @param value the value
     * @return true, if successful
     */
    public boolean setCommand(String value) {
        if (!StringUtil.stringEquals(executeCommand, value)) {
            String oldValue = executeCommand;
            executeCommand = value;
            firePropertyChange(PROPERTY_CMD, oldValue, value);
            return true;
        }
        return false;
    }

    /**
     * Gets the UpdateCommand.
     *
     * @return the UpdateCommand
     */
    public String getUpdateCmd() {
        return updateCommand;
    }

    /**
     * Return The ExecuteCommand
     *
     * @return The Execute Command
     */
    public String getExecuteCommand() {
        if (updateCommand == null) {
            return executeCommand;
        }
        return updateCommand;
    }

    /**
     * With UpdateCommand.
     *
     * @param value the UpdateCommand
     * @return ThisComponent
     */
    public UpdateConfig withUpdateCommand(String value) {
        setUpdateCommand(value);
        return this;
    }

    /**
     * Sets the Command.
     *
     * @param value the value
     * @return true, if successful
     */
    public boolean setUpdateCommand(String value) {
        if (!StringUtil.stringEquals(updateCommand, value)) {
            String oldValue = updateCommand;
            updateCommand = value;
            firePropertyChange(PROPERTY_UPDATECOMMAND, oldValue, value);
            return true;
        }
        return false;
    }

    /**
     * OriginalFile.
     *
     * @return the FileName of Application
     */
    public String getFileName() {
        return fileName;
    }

    /**
     * With FileName.
     *
     * @param value the FileName
     * @return the ThisComponent
     */
    public UpdateConfig withFilename(String value) {
        setFilename(value);
        return this;
    }

    /**
     * Sets the Filename.
     *
     * @param value the Filename
     * @return true, if successful
     */
    public boolean setFilename(String value) {
        if (!StringUtil.stringEquals(fileName, value)) {
            String oldValue = fileName;
            fileName = value;
            firePropertyChange(PROPERTY_FILENAME, oldValue, value);
            return true;
        }
        return false;
    }

    /**
     * OriginalFile.
     *
     * @return the DateTime of Application
     */
    public String getDateTime() {
        return dateTime;
    }

    /**
     * With DateTime.
     *
     * @param value the DateTime
     * @return the ThisComponent
     */
    public UpdateConfig withDateTime(String value) {
        setDateTime(value);
        return this;
    }

    /**
     * Sets the DateTIme.
     *
     * @param value the DateTIme
     * @return true, if successful
     */
    public boolean setDateTime(String value) {
        if (!StringUtil.stringEquals(dateTime, value)) {
            String oldValue = dateTime;
            dateTime = value;
            firePropertyChange(PROPERTY_DATETIME, oldValue, value);
            return true;
        }
        return false;
    }


    /**
     * Gets the CurrentVersion.
     *
     * @return the CurrentVersion
     */
    public String getCurrentVersion() {
        if (currentVersion == null) {
            return null;
        }
        return currentVersion.toString();
    }

    /**
     * With currentVersion.
     *
     * @param value the CurrentVersion
     * @return the CurrentVersion
     */
    public UpdateConfig withCurrentVersion(String value) {
        setCurrentVersion(value);
        return this;
    }

    /**
     * Sets the name.
     *
     * @param value the value
     * @return true, if successful
     */
    public boolean setCurrentVersion(String value) {
        if (currentVersion == null || !StringUtil.stringEquals(currentVersion.toString(), value)) {
            String oldValue = currentVersion == null ? null : currentVersion.toString();
            currentVersion = new VersionCondition().withVersion(value);
            firePropertyChange(PROPERTY_CURRENTVERSION, oldValue, value);
            return true;
        }
        return false;
    }

    /**
     * Set new Version
     *
     * @param value new Version
     * @return Succes for setting
     */
    public boolean setVersion(VersionCondition value) {
        if (currentVersion == null || !currentVersion.equals(value)) {
            VersionCondition oldValue = currentVersion;
            currentVersion = value;
            firePropertyChange(PROPERTY_CURRENTVERSION, oldValue, value);
            return true;
        }
        return false;
    }

    /**
     * Gets the Interval.
     *
     * @return the Interval
     */
    public int getIntervalInS() {
        return intervalInS;
    }

    /**
     * @return The Version
     */
    public VersionCondition getVersion() {
        return currentVersion;
    }

    /**
     * add Interval in Seconds
     *
     * @param value the Interval
     * @return the ThisComponent
     */
    public UpdateConfig withIntervalInS(int value) {
        setIntervalInS(value);
        return this;
    }

    /**
     * Sets the Interval in Seconds.
     *
     * @param value the Interval
     * @return true, if successful
     */
    public boolean setIntervalInS(int value) {
        if (intervalInS != value) {
            int oldValue = intervalInS;
            intervalInS = value;
            firePropertyChange(PROPERTY_INTERVAL, oldValue, value);
            return true;
        }
        return false;
    }


    /**
     * Gets the properties.
     *
     * @return the properties
     */
    @Override
    public String[] getProperties() {
        return properties;
    }

    /**
     * Gets the sendable instance.
     *
     * @param prototyp the prototyp
     * @return the sendable instance
     */
    @Override
    public Object getSendableInstance(boolean prototyp) {
        return new UpdateConfig();
    }

    /**
     * Gets the value.
     *
     * @param entity    the entity
     * @param attribute the attribute
     * @return the value
     */
    @Override
    public Object getValue(Object entity, String attribute) {
        if (attribute == null || !(entity instanceof UpdateConfig)) {
            return null;
        }
        UpdateConfig item = (UpdateConfig) entity;
        if (PROPERTY_CMD.equalsIgnoreCase(attribute)) {
            return item.getCmd();
        }
        if (PROPERTY_UPDATECOMMAND.equalsIgnoreCase(attribute)) {
            return item.getUpdateCmd();
        }
        if (PROPERTY_FILENAME.equalsIgnoreCase(attribute)) {
            return item.getFileName();
        }
        if (PROPERTY_CURRENTVERSION.equalsIgnoreCase(attribute)) {
            return item.getCurrentVersion();
        }
        if (PROPERTY_INTERVAL.equalsIgnoreCase(attribute)) {
            return item.getIntervalInS();
        }
        if (PROPERTY_DATETIME.equalsIgnoreCase(attribute)) {
            return item.getDateTime();
        }
        return null;
    }

    /**
     * Sets the value.
     *
     * @param entity    the entity
     * @param attribute the attribute
     * @param value     the value
     * @param type      the type
     * @return true, if successful
     */
    @Override
    public boolean setValue(Object entity, String attribute, Object value, String type) {
        if (attribute == null || !(entity instanceof UpdateConfig)) {
            return false;
        }
        UpdateConfig item = (UpdateConfig) entity;
        if (PROPERTY_CMD.equalsIgnoreCase(attribute)) {
            return item.setCommand("" + value);
        }
        if (PROPERTY_UPDATECOMMAND.equalsIgnoreCase(attribute)) {
            return item.setUpdateCommand("" + value);
        }
        if (PROPERTY_FILENAME.equalsIgnoreCase(attribute)) {
            return item.setFilename("" + value);
        }
        if (PROPERTY_CURRENTVERSION.equalsIgnoreCase(attribute)) {
      return item.setCurrentVersion("" + value);
    }
    if (PROPERTY_INTERVAL.equalsIgnoreCase(attribute)) {
      return item.setIntervalInS((int) value);
    }
    if (PROPERTY_DATETIME.equalsIgnoreCase(attribute)) {
      return item.setDateTime("" + value);
    }
    return false;
  }
}
