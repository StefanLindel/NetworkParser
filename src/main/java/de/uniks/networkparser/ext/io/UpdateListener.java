package de.uniks.networkparser.ext.io;

/**
 * The listener interface for receiving update events.
 * The class that is interested in processing a update
 * event implements this interface, and the object created
 * with that class is registered with a component using the
 * component's <code>addUpdateListener</code> method. When
 * the update event occurs, that object's appropriate
 * method is invoked.
 * @author Stefan
 */
public interface UpdateListener {
    
    /**
     * Version.
     *
     * @return the long
     */
    long version();
    
    /**
     * Start.
     *
     * @return true, if successful
     */
    boolean start();
    
    /**
     * End.
     *
     * @return true, if successful
     */
    boolean end();
}
