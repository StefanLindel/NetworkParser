package de.uniks.networkparser.ext.io;

import java.io.InputStream;

import de.uniks.networkparser.buffer.BufferedBuffer;
import de.uniks.networkparser.buffer.ByteBuffer;
import de.uniks.networkparser.buffer.CharacterBuffer;

/**
 * The Class PDFStream.
 * 
 * @author Stefan
 */
public class PDFStream {
    private final ByteBuffer buffer;

    /**
     * Instantiates a new PDF stream.
     * 
     * @param inputStream the input stream
     */
    public PDFStream(InputStream inputStream) {
	buffer = new ByteBuffer();
	buffer.addStream(inputStream);
    }

    /**
     * Set a new Value.
     * 
     * @param element the element
     * @param value   the value
     * @return true, if successful
     */
    public boolean setValue(String element, Object value) {
	if (value instanceof Boolean) {
	    return setValueToBoolean(element, (boolean) value);
	}
	return setValueToText(element, "" + value);
    }

    /**
     * Sets the value as text.
     *
     * @param element the element
     * @param value   the value
     * @return true, if successful
     */
    private boolean setValueToText(String element, String value) {
	BufferedBuffer line = buffer.findLine("T(" + element + ")");
	if (line.length() < 1) {
	    line = buffer.findLine("T (" + element + ")");
	    if (line.length() < 1) {
		return false;
	    }
	}
	String[] split = line.toString().split("/");
	CharacterBuffer sb = new CharacterBuffer();
	int i = 0;
	for (i = 0; i < split.length; i++) {
	    split[i] = cleanUpPart(split[i]);
	    boolean found = "Annot".equalsIgnoreCase(split[i]);
	    sb.withAppendItem(split[i], '/');
	    if (found && i + 1 < split.length) {
		sb.with("/V(" + value + ")");
		if (split[i + 1].startsWith("V(")) {
		    // REPLACE
		    i++;
		}
		i++;
		break;
	    }
	}
	String searchConfig = "N ";
	String search = null;
	// REPLACE CACHE TEXT

	while (i < split.length) {
	    split[i] = cleanUpPart(split[i]);
	    sb.withAppendItem(split[i], '/');
	    if (split[i].startsWith(searchConfig) && split[i - 1].endsWith("<<")) {
		search = split[i].substring(searchConfig.length(), split[i].indexOf('R')) + "obj";
	    }
	    i++;
	}

	buffer.replace(line.getStartPosition(), line.getStartPosition() + line.length(), sb.toString());
	if (search == null) {
	    search = "% " + element;
	}
	replaceCache(search, value);
	if (split.length > 0) {
	    i = split[split.length - 1].indexOf("%");
	    if (i > 0) {
		search = split[split.length - 1].substring(i + 2, split[split.length - 1].indexOf('R')) + "obj";
		replaceCache(search, value);
	    }
	}
	return true;
    }

    private void replaceCache(String search, String value) {
	BufferedBuffer line = buffer.findLine(search);
	if (line.length() < 1) {
	    return;
	}
	ByteBuffer cacheText = buffer.nextStop(line.position() + search.length() + 1, "endstream");

	int z = cacheText.indexOf("stream");
	if (z > 0) {
	    cacheText.withStart(cacheText.getStartPosition() + z + 7);
	    cacheText.withLength(cacheText.length() - z - 7);
	}
	String cache = cacheText.substring(0, cacheText.indexOf("(") + 1) + value
		+ cacheText.substring(cacheText.indexOf(")"));
	buffer.replace(cacheText.getStartPosition(), cacheText.getStartPosition() + cacheText.length(), cache);
    }

    private String cleanUpPart(String part) {
	part = part.trim();
	int z = part.indexOf("<<");
	if (z > 0 && part.charAt(z - 1) == ' ') {
	    part = part.substring(0, z - 1) + part.substring(z);
	}
	z = part.indexOf(">>");
	if (z > 0 && part.charAt(z - 1) == ' ') {
	    part = part.substring(0, z - 1) + part.substring(z);
	}
	return part;
    }

    private boolean setValueToBoolean(String element, boolean value) {
	BufferedBuffer line = buffer.findLine("T(" + element + ")");
	if (line.length() < 1) {
	    line = buffer.findLine("T (" + element + ")");
	    if (line.length() < 1) {
		return false;
	    }
	}
	String[] split = line.toString().split("/");
	String searchItem = "Off";
	String replacementItem = "checked";
	if (!value) {
	    String old = searchItem;
	    searchItem = replacementItem;
	    replacementItem = old;
	}
	CharacterBuffer sb = new CharacterBuffer();
	for (int i = 0; i < split.length; i++) {
	    split[i] = split[i].trim();
	    if (searchItem.equalsIgnoreCase(split[i])) {
		split[i] = replacementItem;
	    }
	    if (i > 0 && !split[i].isEmpty()) {
		sb.with('/');
	    }
	    sb.with(split[i]);
	}
	buffer.replace(line.getStartPosition(), line.getStartPosition() + line.length(), sb.toString());
	return true;
    }

    /**
     * Save a PDF.
     * 
     * @param fullFileName the full file name
     */
    public void save(String fullFileName) {
    	new FileBufferImpl().writeFile(fullFileName, FileBuffer.OVERRIDE, buffer.toBytes());
    }
    
    @Override
    public String toString() {
        if(this.buffer == null) {
            return super.toString();
        }
        return this.buffer.string();
    }
}
