package de.uniks.networkparser.ext.io;

/*
The MIT License

Copyright (c) 2010-2016 Stefan Lindel https://www.github.com/fujaba/NetworkParser/

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/
import java.io.OutputStream;
import java.io.PrintStream;

import de.uniks.networkparser.DateTimeEntity;
import de.uniks.networkparser.NetworkParserLog;
import de.uniks.networkparser.SimpleEvent;
import de.uniks.networkparser.ext.ErrorHandler;
import de.uniks.networkparser.interfaces.BaseItem;
import de.uniks.networkparser.interfaces.ObjectCondition;

/**
 * StringPrintStream for Output.
 *
 * @author Stefan Lindel
 */
public class StringPrintStream extends PrintStream implements ObjectCondition {
    
    /** The Constant DEFAULT PRETTY-FORMAT. */
    public static final String FORMAT="%D %T %L: %M";
    private ErrorHandler handler;
    private String format;
    private boolean error;

    /** Instantiates a new string print stream. */
    public StringPrintStream() {
        super(new StringOutputStream());
    }

    /**
     * Instantiates a new string print stream.
     * @param handler the handler
     * @param error   the error
     */
    public StringPrintStream(ErrorHandler handler, boolean error) {
        super(new StringOutputStream());
        this.handler = handler;
        this.error = error;
    }

    /**
     * Instantiates a new string print stream.
     * @param out the out
     */
    public StringPrintStream(OutputStream out) {
        super(out);
    }

    /**
     * With listener.
     * @param value the value
     * @return the string print stream
     */
    public StringPrintStream withListener(ErrorHandler value) {
        this.handler = value;
        return this;
    }

    /**
     * Prints the Value.
     * @param value the value
     */
    public void print(Object value) {
        if (handler != null && value != null) {
            handler.writeOutput(value.toString(), error);
        }
    }

    /**
     * Prints the Value.
     * @param value the value
     */
    public void print(String value) {
        if (handler != null) {
            handler.writeOutput(value, error);
        }
    }

    /** Print new Line. */
    public void println() {
        if (handler != null) {
            handler.writeOutput(BaseItem.CRLF, error);
        }
    }

    /** Println.
     * @param value the value
     */
    public void println(String value) {
        if (handler != null) {
            handler.writeOutput(value, error);
            handler.writeOutput(BaseItem.CRLF, error);
        }
    }

    /**
     * Println.
     * @param value the value
     */
    public void println(Object value) {
        if (handler != null && value != null) {
            handler.writeOutput(value.toString(), error);
            handler.writeOutput(BaseItem.CRLF, error);
        }
    }

    /**
     * Update.
     * @param value the value
     * @return true, if successful
     */
    @Override
    public boolean update(Object value) {
        if (!(value instanceof SimpleEvent)) {
            return false;
        }
        SimpleEvent event = (SimpleEvent) value;
        Object newValue = event.getNewValue();
        if (newValue != null) {
            String msg = "" + newValue;
            if (msg.length() > 0) {
                if (NetworkParserLog.ERROR.equals(event.getType())) {
                    System.err.println(formatEvent(event));
                } else {
                    System.out.println(formatEvent(event));
                }
            }
        }
        return false;
    }
    
    private String formatEvent(SimpleEvent event) {
       if(this.format == null) {
          return event.getType() + ": " + event.getNewValue();
       }
       DateTimeEntity now = DateTimeEntity.Now();
       return format.replace("%T", now.toString("HH:MM:SS"))
             .replace("%L", event.getType())
             .replace("%D", DateTimeEntity.Now().toString())
             .replace("%M", ""+event.getNewValue());
    }
    
    /**
     * With format for Pretty printing.
     *
     * @param format the format
     * @return ThisComponent
     */
    public StringPrintStream withFormat(String format) {
       this.format = format;
       return this;
    }

    /**
     * Gets the string stream.
     * @return the string stream
     */
    public String getStringStream() {
        return this.out.toString();
    }
    /*
     * java.io.PrintStream.printf(String, Object...)
     * java.io.PrintStream.printf(Locale, String, Object...)
     * java.io.PrintStream.println(boolean) java.io.PrintStream.println(char)
     * java.io.PrintStream.println(char[]) java.io.PrintStream.println(double)
     * java.io.PrintStream.println(float) java.io.PrintStream.println(int)
     * java.io.PrintStream.println(long) public void print(boolean value) { public
     * void print(char value) { public void print(char[] value) { public void
     * print(double value) { public void print(float value) { public void print(int
     * value) { public void print(long value) {
     */
}
