package de.uniks.networkparser.ext.io;

import java.io.InputStream;
import java.io.OutputStream;
import de.uniks.networkparser.buffer.ByteBuffer;
import de.uniks.networkparser.buffer.CharacterBuffer;
import de.uniks.networkparser.interfaces.BaseItem;

/**
 * Public Interface for Interaction with Files
 * @author Stefan
 */
public interface FileBuffer {
  /** The none-Flag. */
  public static byte NONE = 0;
  /** The append-Flag. */
  public static byte APPEND = 1;
  /** The override. */
  public static byte OVERRIDE = 2;
  
    /** Copy file.
     *
     * @param sourceFile     the source file
     * @param targetfileName the targetfile name
     * @return the int
     */
	int copyFile(String sourceFile, String targetfileName);
    /** Copy.
     * @param input  the input
     * @param output the output
     * @return the long
     */
	long copy(final InputStream input, final OutputStream output);
    /** Read file.
     *
     * @param file the file
     * @return the character buffer
     */
	CharacterBuffer readFile(String file);
    /** Read base file.
     *
     * @param configFile the config file
     * @return the base item
     */
	BaseItem readBaseFile(String configFile);
    /**
     * Read base file.
     *
     * @param configFile the config file
     * @param container  the container
     * @return the base item
     */
	BaseItem readBaseFile(String configFile, BaseItem container);
	/**Read resource.
	 *
	 * @param file the file
	 * @return the character buffer
	 */
	CharacterBuffer readResource(String file);
    /**
     * Read resource.
     *
     * @param file      the file
     * @param reference the reference
     * @return the character buffer
     */
	CharacterBuffer readResource(String file, Class<?> reference);
    /**
     * Read resource.
     *
     * @param is the is
     * @return the character buffer
     */
	CharacterBuffer readResource(InputStream is);
    /**
     * Read base file resource.
     *
     * @param file           the file
     * @param referenceClass the reference class
     * @return the base item
     */
	BaseItem readBaseFileResource(String file, Class<?> referenceClass);
	
  /**
   * Read all.
   *
   * @return the character buffer
   */
	ByteBuffer readBinary();
  
	/**
     * Write String.
     *
     * @param flag the flag
     * @param data the data
     * @return the int
     */
	int write(byte flag, CharSequence data);
    /**
     * Write file.
     *
     * @param fileName the file name
     * @param data     the data
     * @return the int
     */
	int writeFile(String fileName, CharSequence data);
    /**
     * Write file.
     *
     * @param fileName the file name
     * @param data     the data
     * @param flag     the flag
     * @return the int
     */
	int writeFile(String fileName, CharSequence data, byte flag);
    /**
     * Write file.
     *
     * @param fileName the file name
     * @param data     the data
     * @param flag     the flag
     * @return the int
     */
	int writeFile(String fileName, byte flag, byte... data);
    /**
     * Write file.
     *
     * @param fileName the file name
     * @param data     the data
     * @return the int
     */
	int writeFileUTF8(String fileName, String data);
    /**
     * Write reource file.
     *
     * @param fileName the file name
     * @param path     the path
     * @return the int
     */
	int writeReourceFile(String fileName, String path);

  /**
   * Exists File.
   *
   * @return true, if successful
   */
	boolean exists();
	
  /**
   * Write binary.
   *
   * @param data the data
   * @return the size for writing
   */
	int writeBinary(byte... data);
}
