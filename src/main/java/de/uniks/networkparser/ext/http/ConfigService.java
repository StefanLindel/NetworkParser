package de.uniks.networkparser.ext.http;

import java.util.Iterator;

import de.uniks.networkparser.SimpleEvent;
import de.uniks.networkparser.StringUtil;
import de.uniks.networkparser.ext.RESTServiceTask;
import de.uniks.networkparser.ext.io.FileBuffer;
import de.uniks.networkparser.ext.io.FileBufferImpl;
import de.uniks.networkparser.graph.GraphUtil;
import de.uniks.networkparser.interfaces.BaseItem;
import de.uniks.networkparser.interfaces.Condition;
import de.uniks.networkparser.interfaces.SendableEntityCreator;
import de.uniks.networkparser.interfaces.SendableEntityCreatorTag;
import de.uniks.networkparser.interfaces.SimpleUpdateListener;
import de.uniks.networkparser.json.JsonObject;
import de.uniks.networkparser.xml.HTMLEntity;
import de.uniks.networkparser.xml.XMLEntity;

/**
 * The Class ConfigService.
 *
 * @author Stefan
 */
public class ConfigService implements Condition<HTTPRequest> {
	/** The Constant ACTION_CLOSE. */
	public static final String ACTION_CLOSE = "close";

	/** The Constant ACTION_OPEN. */
	public static final String ACTION_OPEN = "open";

	/** The Constant ACTION_SAVE. */
	public static final String ACTION_SAVE = "Speichern";

	/** The Constant ACTION_ABORT. */
	public static final String ACTION_ABORT = "Abbrechen";

	private final HTTPRequest routing;
	private final Configuration configuration;
	private RESTServiceTask task;
	private SimpleUpdateListener listener;
	private String tag = "Konfiguration";

	/**
	 * Instantiates a new config service.
	 *
	 * @param configuration the configuration
	 */
	public ConfigService(Configuration configuration) {
		routing = HTTPRequest.createRouting("/" + configuration.getTag());
		routing.withTag(tag);
		routing.withUpdateCondition(this);
		this.configuration = configuration;
	}

	/**
	 * With tag.
	 *
	 * @param value the value
	 * @return the config service
	 */
	public ConfigService withTag(String value) {
		tag = value;
		return this;
	}

	/**
	 * Gets the routing.
	 *
	 * @return the routing
	 */
	public HTTPRequest getRouting() {
		return routing;
	}

	/**
	 * With task.
	 *
	 * @param task the task
	 * @return the config service
	 */
	public ConfigService withTask(RESTServiceTask task) {
		this.task = task;
		return this;
	}

	/**
	 * Update.
	 *
	 * @param value the value
	 * @return true, if successful
	 */
	@Override
	public boolean update(HTTPRequest value) {
		if (task == null) {
			return false;
		}
		if (HTTPRequest.HTTP_TYPE_POST.equalsIgnoreCase(value.getHttp_Type())) {
			if (executeSettings(value)) {
				return true;
			}
		}
		if (HTTPRequest.HTTP_TYPE_GET.equalsIgnoreCase(value.getHttp_Type())) {
			return showDefault(value);
        } else if (HTTPRequest.HTTP_TYPE_POST.equalsIgnoreCase(value.getHttp_Type())) {
			String key = value.parse().getContentValue(HTMLEntity.ACTION, true);
			if (ACTION_CLOSE.equalsIgnoreCase(key)) {
				task.close();
				listener.update(new SimpleEvent(this, ACTION_CLOSE, value));
			}
		}
		return false;
	}

	private boolean showDefault(HTTPRequest value) {
		HTMLEntity entity = new HTMLEntity().withEncoding(BaseItem.ENCODING);

		XMLEntity headerTag = entity.createChild("div", HTMLEntity.CLASS, "header");
		XMLEntity backBtn = headerTag.createChild("a", "href", "/");
		backBtn.createChild("div", getFileBuffer().readResource("back.svg", GraphUtil.class).toString());
		backBtn.createChild("div", "Zur&uuml;ck");

		entity.createChild("h1", tag);

		entity.withActionButton("Allgemein", ACTION_OPEN, routing.getAbsolutePath("general"));

		if (configuration != null && configuration.getSettings() != null) {
            for (Iterator<SendableEntityCreatorTag> iterator = configuration.getSettings().iterator(); iterator
                    .hasNext();) {
				SendableEntityCreatorTag entry = iterator.next();
				entity.withActionButton(entry.getTag(), ACTION_OPEN, routing.getAbsolutePath(entry.getTag()));
			}
		}

		entity.withActionButton("Beenden", ACTION_CLOSE);
		if (task.getImpressumService() != null) {
			XMLEntity footer = entity.createChild("div").with(HTMLEntity.CLASS, "footer");
			XMLEntity lnk = footer.createChild("a", "href",
				task.getImpressumService().getRouting().getAbsolutePath());
			lnk.withValue("Impressum");
		}
		value.withContentBody(entity);
		if (listener != null) {
			listener.update(new SimpleEvent(this, HTTPRequest.HTTP_TYPE_GET, value));
		}
		value.write(entity);
		return true;
	}
	
    private static FileBuffer getFileBuffer() {
		return new FileBufferImpl();
	}

	private void createDefaultButton(XMLEntity forms) {
		XMLEntity parent = new XMLEntity().withType("div").withKeyValue(HTMLEntity.CLASS, "actions");
		parent.createChild("input", "type", "submit", "value", ACTION_SAVE, "name", HTMLEntity.ACTION, HTMLEntity.TITLE, "Speichert die Einstellungen.");
		parent.createChild("input", "type", "submit", "value", ACTION_ABORT, "name", HTMLEntity.ACTION, HTMLEntity.TITLE, "Abbrechten");
		forms.withChild(parent);
	}

	private boolean executeSettings(HTTPRequest value) {
		String path = routing.getUrl() + "/general";
		if (value.getUrl().equalsIgnoreCase(path)) {
			String key = value.parse().getContentValue(HTMLEntity.ACTION, true);
			if (key != null) {
				if (key.equalsIgnoreCase(ACTION_ABORT)) {
					return value.redirect(routing.getAbsolutePath());
				}
				if (key.equalsIgnoreCase(ACTION_SAVE)) {
					configuration.setValue(configuration, Configuration.PORT, Integer.valueOf(value.getContentValue("port", true)),
						SendableEntityCreator.NEW);
					listener.update(new SimpleEvent(this, ACTION_SAVE, value));
					return value.redirect(routing.getAbsolutePath());
				}
			}
			HTMLEntity entity = new HTMLEntity();
			entity.createChild("h1", "Allgemein");
			XMLEntity formTag = entity.createChild("form").withKeyValue("method", HTTPRequest.HTTP_TYPE_POST).withKeyValue("enctype", HTTPRequest.CONTENT_JSON);

			formTag.withChild(entity.createInput("Port:", "port", configuration.getPort()));
			createDefaultButton(formTag);

			value.withContentBody(entity);
			if (listener != null) {
				listener.update(new SimpleEvent(this, HTTPRequest.HTTP_TYPE_GET, value));
			}
			value.write(entity);
			return true;
		}
		if (configuration != null && configuration.getSettings() != null) {
			for (SendableEntityCreatorTag creator : configuration.getSettings()) {
				path = routing.getUrl() + "/" + creator.getTag();
				if (value.getUrl().equalsIgnoreCase(path)) {
					String key = value.parse().getContentValue(HTMLEntity.ACTION, true);
					SimpleEvent event = new SimpleEvent(this, ACTION_SAVE, value).withBeforeElement(creator);
					event.with(new JsonObject());
					if (key != null) {
						if (key.equalsIgnoreCase(ACTION_ABORT)) {
							return value.redirect(routing.getAbsolutePath());
						}
						if (key.equalsIgnoreCase(ACTION_SAVE)) {
							for (String prop : creator.getProperties()) {
								Object newValue = StringUtil.convert(creator, creator, prop, value.getContentValue(prop, true));
	              
								event.withKeyValue(prop, newValue);
								creator.setValue(creator, prop, newValue, SendableEntityCreator.NEW);
							}
							if(listener.update(event)) {
								return value.redirect(routing.getAbsolutePath());
							}
						}
					} else {
						for (String prop : creator.getProperties()) {
							event.withKeyValue(prop, creator.getValue(creator, prop));
						}
					}
					HTMLEntity entity = new HTMLEntity().withEncoding(BaseItem.ENCODING);
					entity.createChild("h1", creator.getTag());
					XMLEntity formTag = entity.createChild("form").withKeyValue("method", HTTPRequest.HTTP_TYPE_POST).withKeyValue("enctype", HTTPRequest.CONTENT_JSON);
					
					for (String prop : creator.getProperties()) {
						formTag.withChild(entity.createInput(prop + ":", prop, creator.getValue(creator, prop)));
					}
					createDefaultButton(formTag);
					value.withContentBody(entity);
					if (listener != null) {
						return listener.update(new SimpleEvent(this, HTTPRequest.HTTP_TYPE_GET, value).withBeforeElement(creator));
					}
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * With listener.
	 *
	 * @param updateListener the update listener
	 * @return the config service
	 */
	public ConfigService withListener(SimpleUpdateListener updateListener) {
		listener = updateListener;
		return this;
	}
}
