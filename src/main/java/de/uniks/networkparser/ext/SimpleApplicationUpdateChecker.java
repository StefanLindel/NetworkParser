package de.uniks.networkparser.ext;

import java.io.File;
import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;

import de.uniks.networkparser.DateTimeEntity;
import de.uniks.networkparser.Filter;
import de.uniks.networkparser.IdMap;
import de.uniks.networkparser.SimpleEvent;
import de.uniks.networkparser.ext.io.FileBuffer;
import de.uniks.networkparser.ext.io.FileBufferImpl;
import de.uniks.networkparser.interfaces.ObjectCondition;
import de.uniks.networkparser.xml.ArtifactFile;

/**
 * The Class SimpleApplicationUpdateChecker. Usage: <code>
 * public static void main(String[] args) {		
		SimpleApplicationUpdateChecker.create(args).withListener(applicationUpdateListener);
 }</code> The applicationUpdateListener is Callback for Error, Shutdown and Updated. All Config
 * is in updates.json
 *
 * @author Stefan
 */
public class SimpleApplicationUpdateChecker extends TimerTask {
    /**
     * CONSTANT FOR ERROR-Event
     */
    public static final String ERROR = "error";
    /**
     * CONSTANT FOR SHUTDOWN-Event
     */
    public static final String SHUTDOWN = "shutdown";
    /**
     * CONSTANT FOR UPDATED-Event
     */
    public static final String UPDATED = "update_complete";
    /**
     * Constant for CONFIG-Json
     */
    private static final String UPDATES_JSON = "updates.json";
    private ObjectCondition listener;
    private UpdateConfig config;
    private IdMap map;

    /**
     * Create a new Instance
     *
     * @param args Start-Parameter
     * @return Create a new Instance
     */
    public static SimpleApplicationUpdateChecker create(String[] args) {
        return new SimpleApplicationUpdateChecker().initialize(args);
    }

    /**
     * Add Configuration
     *
     * @param config a new Configuration
     * @return ThisComponent
     */
    public SimpleApplicationUpdateChecker withConfig(UpdateConfig config) {
        this.config = config;
        return this;
    }

    /**
     * @param listener The Listener for notify Event
     * @return ThisComponent
     */
    public SimpleApplicationUpdateChecker withListener(ObjectCondition listener) {
        this.listener = listener;
        return this;
    }

    /**
     * Initialize Checker
     *
     * @param args Start Parameter of Application
     * @return ThisComponent
     */
    private SimpleApplicationUpdateChecker initialize(String[] args) {
        map = new IdMap();
        map.withCreator(new UpdateConfig());

        config = (UpdateConfig) map.decode(getFileBuffer().readFile(UPDATES_JSON));
        if (config == null) {
            config = UpdateConfig.createDefault();
            writeConfig();
        }
        for (String item : args) {
            if (UPDATED.equalsIgnoreCase(item)) {
                updateCondition(new SimpleEvent(this, UPDATED));
                break;
            }
            if (config.getFileName() != null && item.startsWith(config.getFileName())) {
                File updateFile = new File(config.getFileName() + ".jar");
                if (getFileBuffer().copyFile(item, updateFile.getName()) > 0) {
                    ArtifactFile artefactID = ArtifactFile.createContext(item);
                    config.setVersion(artefactID.getVersion());
                    config.setDateTime(new DateTimeEntity().toString());
                    writeConfig();
                    try {
                        Runtime.getRuntime().exec(config.getCmd() + " " + updateFile.getName() + " " + UPDATED);
                        System.exit(0);
                    } catch (IOException e) {
                        updateCondition(new SimpleEvent(this, ERROR, e));
                    }
                }
            }
        }
        if (config.getFileName() != null && config.getIntervalInS() > 0 && config.getCmd() != null) {
            new Timer().schedule(this, config.getIntervalInS() * 1000, config.getIntervalInS() * 1000);
        }
        return this;
    }


    private FileBuffer getFileBuffer() {
        return new FileBufferImpl();
    }

    /**
     * Write the Config
     *
     * @return success
     */
    public boolean writeConfig() {
        return getFileBuffer().writeFile(UPDATES_JSON, FileBuffer.OVERRIDE,
                map.toJsonObject(config, Filter.createFull()).toString(2).getBytes()) > 0;
    }

    /**
     * Execute a UpdateEvent
     *
     * @param event the new Event
     * @return success
     */
    public boolean updateCondition(SimpleEvent event) {
        if (this.listener == null) {
            return true;
        }
        return this.listener.update(event);
    }

    /**
     * Execute UpdateChecker
     */
    public void run() {
        String userDirectory = System.getProperty("user.dir");
        for (File file : new File(userDirectory).listFiles()) {
            if (file.getName().startsWith(config.getFileName())) {
                ArtifactFile artefactID = ArtifactFile.createContext(file.getName());
                boolean isVersion = artefactID.getVersion().getMaxPos() > 1;
                if (isVersion && artefactID.getVersion().isNewer(config.getVersion())) {
                    File originalFile = new File(config.getFileName() + "-" + config.getVersion() + ".jar");
                    if (!originalFile.exists()) {
                        if (getFileBuffer().copyFile(config.getFileName() + ".jar",
                                originalFile.getName()) < 1) {
                            updateCondition(new SimpleEvent(this, ERROR,
                                    "FILE NOT COPY: " + config.getFileName() + ".jar -> " + originalFile.getName()));
                            break;
                        }
                    }
                    updateCondition(new SimpleEvent(this, SHUTDOWN));
                    try {
                        Runtime.getRuntime().exec(
                                config.getExecuteCommand() + " " + originalFile.getName() + " " + file.getName());
                        System.exit(0);
                    } catch (IOException e) {
                        updateCondition(new SimpleEvent(this, ERROR, e));
          }
        }
      }
    }
  }

}
