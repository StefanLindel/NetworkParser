package de.uniks.networkparser.ext;

/*
 * The MIT License
 *
 * Copyright (c) 2010-2016 Stefan Lindel https://www.github.com/fujaba/NetworkParser/
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
import java.util.Collection;

import de.uniks.networkparser.ext.io.FileBufferImpl;
import de.uniks.networkparser.ext.story.Story;
import de.uniks.networkparser.graph.Annotation;
import de.uniks.networkparser.graph.Association;
import de.uniks.networkparser.graph.AssociationSet;
import de.uniks.networkparser.graph.Attribute;
import de.uniks.networkparser.graph.Clazz;
import de.uniks.networkparser.graph.ClazzSet;
import de.uniks.networkparser.graph.DataType;
import de.uniks.networkparser.graph.Feature;
import de.uniks.networkparser.graph.FeatureSet;
import de.uniks.networkparser.graph.GraphList;
import de.uniks.networkparser.graph.GraphMember;
import de.uniks.networkparser.graph.GraphModel;
import de.uniks.networkparser.graph.GraphTokener;
import de.uniks.networkparser.graph.GraphUtil;
import de.uniks.networkparser.graph.Match;
import de.uniks.networkparser.graph.ModifyEntry;
import de.uniks.networkparser.interfaces.BaseItem;
import de.uniks.networkparser.list.SimpleKeyValueList;
import de.uniks.networkparser.xml.HTMLEntity;

/**
 * The Class ClassModel.
 *
 * @author Stefan
 */
public class ClassModel extends GraphModel {
    /**
     * The Constant ONE.
     */
    public static final int ONE = 1;
    /**
     * The Constant MANY.
     */
    public static final int MANY = 42;

    private ModelGenerator generator = new ModelGenerator().withDefaultModel(this);

    /**
     * Constructor.
     *
     * @param packageName PackageName of ClassModel
     */
    public ClassModel(String packageName) {
        this();
        with(packageName);
    }

    /**
     * Instantiates a new class model.
     */
    public ClassModel() {
        name = getDefaultPackage();
        setAuthorName(System.getProperty("user.name"));
    }

    /**
     * Reset generator.
     */
    public void resetGenerator() {
        generator = new ModelGenerator().withDefaultModel(this);
    }

    /**
     * With feature.
     *
     * @param feature the feature
     * @return the class model
     */
    public ClassModel withFeature(Feature... feature) {
        generator.withFeature(feature);
        return this;
    }

    /**
     * With feature.
     *
     * @param feature the feature
     * @return the class model
     */
    public ClassModel withFeature(FeatureSet feature) {
        generator.withFeature(feature);
        return this;
    }

    /**
     * Without feature.
     *
     * @param feature the feature
     * @return the class model
     */
    public ClassModel withoutFeature(Feature feature) {
        generator.withoutFeature(feature);
        return this;
    }

    /**
     * Removes the.
     *
     * @param member the member
     * @return true, if successful
     */
    @Override
    public boolean remove(GraphMember member) {
        return super.remove(member);
    }

    /**
     * Gets the generator.
     *
     * @param params the params
     * @return the generator
     */
    public ModelGenerator getGenerator(String... params) {
        if (params != null) {
            if (params.length > 0 && params[0] != null) {
                generator.withRootDir(params[0]);
            }
            if (params.length > 1 && params[1] != null) {
                generator.withFileType(params[1]);
            }
        }
        return generator;
    }

    /**
     * Gets the feature.
     *
     * @param feature the feature
     * @param clazzes the clazzes
     * @return the feature
     */
    public Feature getFeature(Feature feature, Clazz... clazzes) {
        return generator.getFeature(feature, clazzes);
    }

    /**
     * Dump HTML.
     *
     * @param diagramName the diagram name
     * @param write       the write
     * @return the HTML entity
     */
    @Override
    public HTMLEntity dumpHTML(String diagramName, boolean... write) {
        if (diagramName == null || diagramName.length() < 1) {
            diagramName = getName();
        }
        if (diagramName == null) {
            diagramName = "Model";
        }
        if (diagramName.length() < 1) {
            return null;
        }

        HTMLEntity entity = super.dumpHTML(diagramName, write);

        if (diagramName.indexOf('/') < 0) {
            diagramName = "doc/" + diagramName;
        }

        diagramName = getStory().addResource(entity, diagramName, false);

        if (write == null || write.length < 1 || !write[0]) {
            return entity;
        }
        String htmlText = entity.toString();
        if (getStory().getBuffer().writeFile(diagramName, htmlText) >= 0) {
            return entity;
        }
        return null;
    }

    /**
     * Generate.
     *
     * @param rootDir the root dir
     * @return the class model
     */
    @Override
    public ClassModel generate(String... rootDir) {
        String path = null;
        if (rootDir != null && rootDir.length > 0) {
            path = rootDir[0];
        }
        getGenerator().generate(path, this);
        return this;
    }

    /**
     * Adds the.
     *
     * @param values the values
     * @return true, if successful
     */
    @Override
    public boolean add(Object... values) {
        if (values == null) {
            return true;
        }
        boolean add = true;
        for (Object item : values) {
            if (item instanceof Collection<?>) {
                Collection<?> items = (Collection<?>) item;
                for (Object i : items) {
                    add = add(i);
                }
                continue;
            }
            if (item instanceof Match) {
                /* Change */
                Match match = (Match) item;
                GraphMember member = match.getMatch();
                Clazz clazz = createClazz(member.getClazz().getName());
                ModifyEntry modifier = ModifyEntry.createModifier(member);
                GraphUtil.setChildren(clazz, modifier);

                Object newValue = match.getNewValue();
                if (newValue instanceof Attribute) {
                    GraphUtil.setChildren(clazz, newValue);
                } else if (newValue instanceof DataType) {
                    if (member instanceof Attribute) {
                        clazz.createAttribute(member.getName(), (DataType) newValue);
                    }
                }
            }
            if (item instanceof Annotation) {
                super.withAnnotation((Annotation) item);
            } else if (item instanceof Association) {
                Association assoc = (Association) item;
                with(assoc);
            } else if (item instanceof Clazz) {
                Clazz clazz = (Clazz) item;
                clazz.setClassModel(this);
            } else if (item instanceof GraphModel) {
                GraphModel model = (GraphModel) item;
                ClazzSet clazzes = model.getClazzes();
                AssociationSet associations = model.getAssociations();
                // TEST CLAZZES
                if (model instanceof GraphList) {
                    String type = ((GraphList) model).getType();
                    if (GraphTokener.OBJECTDIAGRAM.equalsIgnoreCase(type)) {
                        ClazzSet newList = new ClazzSet();
                        SimpleKeyValueList<Clazz, Clazz> mapping = new SimpleKeyValueList<Clazz, Clazz>();
                        for (Clazz clazz : clazzes) {
                            if (clazz.getName() == null) {
                                continue;
                            }
                            Clazz newClazz = newList.getClazz(clazz.getName());
                            if (newClazz == null) {
                                newClazz = new Clazz(clazz.getName());
                                newList.add(newClazz);
                            }
                            for (Attribute attr : clazz.getAttributes()) {
                                newClazz.createAttribute(attr.getName(), attr.getType());
                            }
                            mapping.add(clazz, newClazz);
                        }
                        add(newList);
                        // Mapping Association
                        for (Association assoc : associations) {
                            Association other = assoc.getOther();
                            Clazz sourceNew = mapping.get(other.getClazz());
                            Clazz targetNew = mapping.get(assoc.getClazz());
                            if (sourceNew != null && targetNew != null) {
                                sourceNew.createBidirectional(targetNew, assoc.getName(), assoc.getCardinality(),
                                        other.getName(), other.getCardinality());
                            }
                        }

                        return true;
                    }
                }
                add(clazzes);
                add(associations);
            } else {
                add = false;
            }
        }
        return add;
    }

    /**
     * Gets the new list.
     *
     * @param keyValue the key value
     * @return the new list
     */
    @Override
    public BaseItem getNewList(boolean keyValue) {
        return new ClassModel();
    }

    /**
     * With.
     *
     * @param name the name
     * @return the class model
     */
    /* Override some Method because change ReturnValue */
    @Override
    public ClassModel with(String name) {
        super.with(name);
        return this;
    }

    /**
     * With.
     *
     * @param value the value
   * @return the class model
   */
  @Override
  public ClassModel with(Annotation value) {
    super.with(value);
    return this;
  }
}
