package de.uniks.networkparser.logic;

import de.uniks.networkparser.SimpleEvent;
import de.uniks.networkparser.buffer.CharacterBuffer;
import de.uniks.networkparser.interfaces.ObjectCondition;

/**
 * The Class VersionCondition.
 *
 * @author Stefan
 */
public class VersionCondition implements ObjectCondition, Comparable<VersionCondition> {
    /**
     * The Constant SNAPSHOT.
     */
    public static final String SNAPSHOT = "SNAPSHOT";
    /**
     * Constant for Major-Part
     */
    public static final String MAJOR = "Major";
    /**
     * Constant for Minor-Part
     */
    public static final String MINOR = "Minor";
    /**
     * Constant for Revision-Part
     */
    public static final String REVISION = "Revision";
    /**
     * Constant for Add Symbol
     */
    public static final String ADD = "+";
    private final int[] parts = new int[3];
    private ObjectCondition children;
    private int maxPos;
    private boolean isSnapshot;

    /**
     * add version-String
     *
     * @param version the version
     * @return the version condition
     */
    public VersionCondition withVersion(String version) {
        if (version != null) {
            String[] split = version.split("\\.");
            maxPos = split.length;
            for (int i = 0; i < maxPos - 1; i++) {
                try {
                    if (ADD.equals(split[i])) {
                        children = new CompareTo().withCompare(CompareTo.EQUALS);
                        parts[i] = -1;
                    } else if (split[i].startsWith("^")) {
                        /* COMPAREGRATER */
                        children = new CompareTo().withCompare(CompareTo.GREATER);
                        parts[i] = Integer.parseInt(split[i].substring(1));
                    } else {
                        parts[i] = Integer.parseInt(split[i]);
                    }
                } catch (Exception e) { // Empty
                }
            }
            try {
                if ("+".equals(split[maxPos - 1])) {
                    children = new CompareTo().withCompare(CompareTo.EQUALS);
                    parts[maxPos - 1] = -1;
                } else if (split[maxPos - 1].indexOf("-") > 0) {
                    isSnapshot = true;
                    parts[maxPos - 1] =
                            Integer.parseInt(split[maxPos - 1].substring(0, split[2].indexOf("-")));
                } else {
                    parts[maxPos - 1] = Integer.parseInt(split[maxPos - 1]);
                }
            } catch (Exception e) {
            }
        }
        return this;
    }

    /**
     * To string.
     *
     * @return the string
     */
    @Override
    public String toString() {
        CharacterBuffer buffer = new CharacterBuffer();
        buffer.withInt(parts[0]);
        for (int i = 1; i < parts.length; i++) {
            if (parts[i] == -1) {
                buffer.with('.').with(ADD);
                break;
            }
            buffer.with('.').withInt(parts[i]);
        }
        if (isSnapshot) {
            buffer.with('-').with(SNAPSHOT);
        }
        return buffer.toString();
    }

    /**
     * Update-Event
     *
     * @param value the value
     * @return true, if successful
     */
    @Override
    public boolean update(Object value) {
        if (!(value instanceof SimpleEvent)) {
            return false;
        }
        SimpleEvent evt = (SimpleEvent) value;
        Object newValue = evt.getNewValue();
        if (newValue instanceof VersionCondition && children != null) {
            return children.update(value);
        }
        return true;
    }

    /**
     * Compare to other Version
     *
     * @param o the o
     * @return the int
     */
    @Override
    public int compareTo(VersionCondition o) {
        if (o == null) {
            return -1;
        }
        for (int i = 0; i < parts.length; i++) {
            if (parts[i] < o.parts[i]) {
                return -1;
            } else if (parts[i] > o.parts[i]) {
                return 1;
            }
        }
        return 0;
    }

    /**
     * Check if Version is newer
     *
     * @param o is Version is Newer
     * @return is Parameter is newer
     */
    public boolean isNewer(VersionCondition o) {
        return compareTo(o) > 0;
    }

    /**
     * Set new Value to Postion
     *
     * @param pos   Positon for Set
     * @param value The real Value
     * @return ThisComponent
     */
    public VersionCondition withPosition(String pos, int value) {
        if (MAJOR.equals(pos)) {
            parts[0] = value;
        }
        if (MINOR.equals(pos)) {
            parts[1] = value;
        }
        if (REVISION.equals(pos)) {
            parts[2] = value;
        }
        return this;
    }

    /**
     * Return Value from Instance
     *
     * @param pos The Position
     * @return The Value
     */
    public int getValue(String pos) {
        if (MAJOR.equals(pos)) {
            return parts[0];
        }
        if (MINOR.equals(pos)) {
            return parts[1];
        }
        if (REVISION.equals(pos)) {
            return parts[2];
        }
        return 0;
    }

    /**
     * @return The maximal Position
     */
    public int getMaxPos() {
        return maxPos;
    }

    /** @return is Version is Snapshot */
  public boolean isSnapshot() {
    return isSnapshot;
  }
}
