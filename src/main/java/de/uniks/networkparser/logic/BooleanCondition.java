package de.uniks.networkparser.logic;

import de.uniks.networkparser.interfaces.ObjectCondition;
import de.uniks.networkparser.interfaces.SendableEntityCreator;

/**
 * The Class BooleanCondition.
 *
 * @author Stefan
 */
public class BooleanCondition implements ObjectCondition, SendableEntityCreator {

    /**
     * The Constant VALUE.
     */
    public static final String VALUE = "value";
    private boolean value;

    /**
     * Static Method for instance a new Instance of BooleanCondition Object.
     *
     * @param value Value
     * @return The new Instance
     */
    public static BooleanCondition create(boolean value) {
        return new BooleanCondition().withValue(value);
    }

    /**
     * Update.
     *
     * @param value the value
     * @return true, if successful
     */
    @Override
    public boolean update(Object value) {
        return this.value;
    }

    /**
     * With value.
     *
     * @param value the value
     * @return the boolean condition
     */
    public BooleanCondition withValue(boolean value) {
        this.value = value;
        return this;
    }

    /**
     * Gets the value.
     *
     * @return the value
     */
    public boolean getValue() {
        return value;
    }

    /**
     * Gets the properties.
     *
     * @return the properties
     */
    @Override
    public String[] getProperties() {
        return new String[]{VALUE};
    }

    /**
     * Gets the sendable instance.
     *
     * @param prototyp the prototyp
     * @return the sendable instance
     */
    @Override
    public Object getSendableInstance(boolean prototyp) {
        return new BooleanCondition();
    }

    /**
     * Gets the value.
     *
     * @param entity    the entity
     * @param attribute the attribute
     * @return the value
     */
    @Override
    public Object getValue(Object entity, String attribute) {
        if (VALUE.equalsIgnoreCase(attribute)) {
            return ((BooleanCondition) entity).getValue();
        }
        return null;
    }

    /**
     * Sets the value.
     *
     * @param entity    the entity
     * @param attribute the attribute
     * @param value     the value
     * @param type      the type
     * @return true, if successful
     */
    @Override
    public boolean setValue(Object entity, String attribute, Object value, String type) {
        if (VALUE.equalsIgnoreCase(attribute)) {
            ((BooleanCondition) entity).withValue((Boolean) value);
      return true;
    }
    return false;
  }
}
