package de.uniks.networkparser;

import de.uniks.networkparser.interfaces.BaseItem;
import de.uniks.networkparser.interfaces.Entity;
import de.uniks.networkparser.interfaces.SendableEntityCreator;
import de.uniks.networkparser.list.SimpleKeyValueList;
import java.beans.PropertyChangeEvent;

/**
 * Event for Changes in IdMap
 * <p>
 * typ the typ of Message: NEW UPDATE, REMOVE or SENDUPDATE.
 *
 * @author Stefan Lindel
 */
public class SimpleEvent extends PropertyChangeEvent {
    private static final long serialVersionUID = 1L;
    /**
     * Variable for Deep from Root.
     */
    private int depth;
    /**
     * The entity.
     */
    private Entity entity;
    /**
     * The value.
     */
    private Object value;
    /**
     * The type.
     */
    private String type;
    /**
     * The before element.
     */
    private Object beforeElement;

    /**
     * Instantiates a new simple event.
     */
    public SimpleEvent() {
        super("", "", null, null);
    }

    /**
     * Constructor for example Filter Regard or Convertable.
     *
     * @param source   List Container
     * @param property Property of Event
     * @param oldValue Old Element
     * @param newValue new Element
     */
    public SimpleEvent(Object source, String property, Object oldValue, Object newValue) {
        super(source, property, oldValue, newValue);
        type = SendableEntityCreator.NEW;
    }

    /**
     * Constructor for example Filter Regard or Convertable.
     *
     * @param source   List Container
     * @param property Property of Event
     */
    public SimpleEvent(Object source, String property) {
        super(source, property, null, null);
        type = SendableEntityCreator.NEW;
    }

    /**
     * Constructor for example Filter Regard or Convertable.
     *
     * @param source   List Container
     * @param property Property of Event
     * @param newValue new Element
     */
    public SimpleEvent(Object source, String property, Object newValue) {
        super(source, property, null, newValue);
        type = SendableEntityCreator.NEW;
    }

    /**
     * Constructor for example Filter and UpdateJson.
     *
     * @param type     typ of Event
     * @param entity   source Entity
     * @param source   List Container
     * @param property Property of Event
     * @param oldValue Old Element
     * @param newValue new Element
     */
    public SimpleEvent(String type, Entity entity, BaseItem source, String property, Object oldValue,
                       Object newValue) {
        super(source, property, oldValue, newValue);
        this.entity = entity;
        this.type = type;
    }

    /**
     * Constructor for example UpdateJson.
     *
     * @param type   typ of Event
     * @param entity source Entity
     * @param source source PropertyChange
     * @param map    IdMap
     */
    public SimpleEvent(String type, Entity entity, PropertyChangeEvent source, SimpleMap map) {
        super(map, source.getPropertyName(), source.getOldValue(), source.getNewValue());
        value = source.getSource();
        this.type = type;
        this.entity = entity;
    }

    /**
     * Constructor for example Event of List.
     *
     * @param type     typ of Event
     * @param source   List Container
     * @param property Property of Event
     * @param index    is the Index of Evententity(List) or depth of Element in Model structure
     * @param oldValue Old Element
     * @param newValue New Element
     * @param value    Value of KeyValue List or the original modelItem
     * @param before   Value of BeforeElement of List
     */
    public SimpleEvent(String type, BaseItem source, String property, int index, Object oldValue,
                       Object newValue, Object value, Object before) {
        super(source, property, oldValue, newValue);
        this.type = type;
        depth = index;
        beforeElement = before;
        this.value = value;
    }

    /**
     * Constructor for example Event of List.
     *
     * @param source        List Container
     * @param index         is the Index of EventEntity(List)
     * @param newCollection the new Collection
     * @param model         the Model
     * @param newValue      New Element
     * @param filter        The Filter of Getter
     * @return new SimpleEvent
     */
    public static SimpleEvent create(Object source, int index, Object newCollection, Object model,
                                     Object newValue, Object filter) {
        SimpleEvent evt = new SimpleEvent(source, "createpattern", model, newValue);
        evt.depth = index;
        evt.beforeElement = newCollection;
        evt.value = filter;
        return evt;
    }

    /**
     * Gets the index.
     *
     * @return the index
     */
    public int getIndex() {
        return depth;
    }

    /**
     * Gets the depth.
     *
     * @return the depth
     */
    public int getDepth() {
        return depth;
    }

    /**
     * With value.
     *
     * @param value the value
     * @return the simple event
     */
    public SimpleEvent withValue(int value) {
        depth = value;
        return this;
    }

    /**
     * Gets the entity.
     *
     * @return the entity
     */
    public Entity getEntity() {
        return entity;
    }

    /**
     * Gets the model value.
     *
     * @return the model value
     */
    public Object getModelValue() {
        return value;
    }

    /**
     * With model value.
     *
     * @param value the value
     * @return the simple event
     */
    public SimpleEvent withModelValue(Object value) {
        this.value = value;
        return this;
    }

    /**
     * Gets the type.
     *
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * With type.
     *
     * @param value the value
     * @return the simple event
     */
    public SimpleEvent withType(String value) {
        type = value;
        return this;
    }

    /**
     * Checks if is new event.
     *
     * @return true, if is new event
     */
    public boolean isNewEvent() {
        return SendableEntityCreator.NEW.equals(type);
    }

    /**
     * Checks if is id event.
     *
     * @return true, if is id event
     */
    public boolean isIdEvent() {
        return "id".equals(type);
    }

    /**
     * Checks if is update event.
     *
     * @return true, if is update event
     */
    public boolean isUpdateEvent() {
        return SendableEntityCreator.UPDATE.equals(type);
    }

    /**
     * add a new Entity.
     *
     * @param entity the entity
     * @return the simple event
     */
    public SimpleEvent with(Entity entity) {
        this.entity = entity;
        return this;
    }

    /**
     * Gets the before element.
     *
     * @return the beforeElement
     */
    public Object getBeforeElement() {
        return beforeElement;
    }

    /**
     * Set the BeforeElement for Event.
     *
     * @param element The Element Before
     * @return ThisComponent
     */
    public SimpleEvent withBeforeElement(Object element) {
        beforeElement = element;
        return this;
    }

    /**
     * With key value.
     *
     * @param key      the key
     * @param newValue the value
     * @return the simple event
     */
    public SimpleEvent withKeyValue(Object key, Object newValue) {
        if (value instanceof SimpleKeyValueList<?, ?>) {
            ((SimpleKeyValueList<?, ?>) value).with(key, newValue);
            return this;
        }
        value = new SimpleKeyValueList<Object, Object>().with(key, newValue);
        return this;
    }

    /**
     * Checks if key is exists.
     *
     * @param key the key
     * @return true, if is value exists.
     */
    public boolean isValue(Object key) {
        return isValue(key, false);
    }

    /**
     * Checks if key is exists.
     *
     * @param key          the key
     * @param defaultValue the defaultValue
     * @return true, if is value exists.
     */
    public boolean isValue(Object key, boolean defaultValue) {
        if (value instanceof SimpleKeyValueList<?, ?>) {
            Object returnValue = ((SimpleKeyValueList<?, ?>) value).get(key);
            if (returnValue instanceof Boolean) {
                return (Boolean) returnValue;
            }
        }
        return defaultValue;
  }

  /**
   * Return Map of Values
   * 
   * @return List of MapValues
   */
  @SuppressWarnings("unchecked")
  public SimpleKeyValueList<Object, Object> getMapValues() {
    if (value instanceof SimpleKeyValueList<?, ?>) {
      return (SimpleKeyValueList<Object, Object>) value;
    }
    return null;
  }
}
