package de.uniks.networkparser.xml;

import de.uniks.networkparser.EntityStringConverter;
import de.uniks.networkparser.StringUtil;
import de.uniks.networkparser.buffer.CharacterBuffer;
import de.uniks.networkparser.interfaces.BaseItem;
import de.uniks.networkparser.interfaces.Converter;
import de.uniks.networkparser.interfaces.Entity;
import de.uniks.networkparser.interfaces.SendableEntityCreatorTag;
import de.uniks.networkparser.list.SimpleList;
import de.uniks.networkparser.logic.VersionCondition;

/**
 * The Class ArtifactFile.
 *
 * @author Stefan
 */
public class ArtifactFile implements SendableEntityCreatorTag, BaseItem, Comparable<ArtifactFile> {
    /**
     * CONSTANT FOR DEFAULT EXTENSION
     */
    public static final String DEFAULT = "jar";

    /**
     * The Constant PROPERTY_MODELVERSION.
     */
    public static final String PROPERTY_MODELVERSION = "modelVersion?";
    /**
     * The Constant PROPERTY_GROUPID.
     */
    public static final String PROPERTY_GROUPID = "groupId?";
    /**
     * The Constant PROPERTY_ARTIFACTID.
     */
    public static final String PROPERTY_ARTIFACTID = "artifactId?";
    /**
     * The Constant PROPERTY_VERSION.
     */
    public static final String PROPERTY_VERSION = "version?";
    /**
     * The Constant PROPERTY_SCOPE.
     */
    public static final String PROPERTY_SCOPE = "scope?";
    /**
     * The Constant PROPERTY_DEPENDENCIES.
     */
    public static final String PROPERTY_DEPENDENCIES = "dependencies";
    /**
     * The Constant PROPERTY_DEPENDENCY.
     */
    public static final String PROPERTY_DEPENDENCY = "dependency";
    /**
     * The Constant PROPERTY_CLASSIFIER.
     */
    public static final String PROPERTY_CLASSIFIER = "classifier";
    /**
     * The Constant PROPERTY_LICENCES.
     */
    public static final String PROPERTY_LICENCES = "licenses";

    private static final String TAG = "project";
    private final SimpleList<Licence> licences = new SimpleList<>();

    /**
     * The prefix.
     */
    public String prefix = "";
    /**
     * The latest.
     */
    public boolean latest;
    private String modelVersion;
    private String groupId;
    private String artifactId;
    private String scope;
    private String tag = TAG;
    private String time;
    private SimpleList<ArtifactFile> dependencies;
    private SimpleList<String> classifier;
    private String index;
    private String fileName;
    private boolean simpleFormat;
    private VersionCondition version;


    /**
     * Create a new ArtifactFile
     *
     * @param fileName new Filename
     * @return a new ArtifactFile
     */
    public static final ArtifactFile createContext(String fileName) {
        return createContext(fileName, null, null);
    }

    /**
     * Creates the context.
     *
     * @param fileName the file name
     * @param groupId  the group id
     * @param time     the time
     * @return the artifact file
     */
    public static final ArtifactFile createContext(String fileName, String groupId, String time) {
        ArtifactFile artifactFile = new ArtifactFile();
        if (fileName == null) {
            return artifactFile;
        }
        int len;
        artifactFile.fileName = fileName;
        String defaultClassifier = DEFAULT;
        fileName = fileName.replace('\\', '/');
        if (fileName.indexOf("/") > 0) {
            /* YEAH FILENAME */
            int last = fileName.lastIndexOf('.');
            int path = fileName.lastIndexOf('/');
            if (path < 0) {
                path = 0;
            }
            if (last < 0) {
                last = fileName.length() - 1;
            }
            defaultClassifier = fileName.substring(last + 1);
            if (defaultClassifier.equalsIgnoreCase("asc")) {
                return null;
            }
            /* REMOVE EXTENSION */
            fileName = fileName.substring(path + 1, last);

        }
        /* Find ArtifactId */
        len = fileName.indexOf("-");
        if (len > 0) {
            artifactFile.artifactId = fileName.substring(0, len);
            fileName = fileName.substring(len + 1);
        }
        len = fileName.indexOf("-");
        if (len > 0) {
            artifactFile.withVersion(fileName.substring(0, len));
            fileName = fileName.substring(len + 1);
        } else {
            /* MUST BE RELASE AND NO CLASSIFIER */
            if (fileName.toLowerCase().endsWith("." + DEFAULT)) {
                fileName = fileName.substring(0, fileName.length() - 4);
            }
            artifactFile.withVersion(fileName);

            fileName = defaultClassifier;
        }
        /* REST IS CLASSIFIER */
        artifactFile.addClassifier(fileName);
        artifactFile.time = time;

        artifactFile.groupId = groupId;
        artifactFile.index = artifactFile.version + artifactFile.artifactId;

        return artifactFile;
    }

    /**
     * To GIT string.
     *
     * @return the string
     */
    public String toGITString() {
        if (System.getenv().get("BUILD_NUMBER") != null) {
            try {
                version.withPosition(VersionCondition.REVISION,
                        Integer.parseInt(System.getenv().get("BUILD_NUMBER")));
            } catch (Exception e) { // Empty
            }
        }
        if (latest) {
            return "";
        } else if (!isRelease() && !isMaster()) {
            return version.toString() + "-SNAPSHOT";
        }
        return version.toString();
    }

    /**
     * Checks if is master.
     *
     * @return true, if is master
     */
    public boolean isMaster() {
        if (scope == null) {
            return false;
        }
        return scope.contains("master");
    }

    /**
     * Checks if is release.
     *
     * @return true, if is release
     */
    public boolean isRelease() {
        if (scope == null) {
            return false;
        }
        return scope.contains("release");
    }

    /**
     * With model version.
     *
     * @param value the value
     * @return the artifact file
     */
    public ArtifactFile withModelVersion(String value) {
        modelVersion = value;
        return this;
    }

    /**
     * Gets the model version.
     *
     * @return the model version
     */
    public String getModelVersion() {
        return modelVersion;
    }

    /**
     * With group id.
     *
     * @param value the value
     * @return the artifact file
     */
    public ArtifactFile withGroupId(String value) {
        groupId = value;
        return this;
    }

    /**
     * Gets the group id.
     *
     * @return the group id
     */
    public String getGroupId() {
        return groupId;
    }

    /**
     * With artifact id.
     *
     * @param value the value
     * @return the artifact file
     */
    public ArtifactFile withArtifactId(String value) {
        artifactId = value;
        return this;
    }

    /**
     * Gets the artifact id.
     *
     * @return the artifact id
     */
    public String getArtifactId() {
        return artifactId;
    }

    /**
     * With version.
     *
     * @param value the value
     * @return the artifact file
     */
    public ArtifactFile withVersion(String value) {
        version = new VersionCondition().withVersion(value);
        return this;
    }

    /**
     * Gets the version.
     *
     * @return the version
     */
    public VersionCondition getVersion() {
        return version;
    }

    /**
     * With scope.
     *
     * @param value the value
     * @return the artifact file
     */
    public ArtifactFile withScope(String value) {
        scope = value;
        return this;
    }

    /**
     * Gets the scope.
     *
     * @return the scope
     */
    public String getScope() {
        return scope;
    }

    /**
     * With tag.
     *
     * @param value the value
     * @return the artifact file
     */
    public ArtifactFile withTag(String value) {
        tag = value;
        return this;
    }

    /**
     * Gets the tag.
     *
     * @return the tag
     */
    @Override
    public String getTag() {
        return tag;
    }

    /**
     * With dependency.
     *
     * @param value the value
     * @return the artifact file
     */
    public ArtifactFile withDependency(ArtifactFile value) {
        if (value == null) {
            return this;
        }
        value.withTag("dependency");
        if (dependencies == null) {
            dependencies = new SimpleList<ArtifactFile>();
        }
        dependencies.add(value);
        return this;
    }

    /**
     * Adds the.
     *
     * @param values the values
     * @return true, if successful
     */
    @Override
    public boolean add(Object... values) {
        if (values == null || values.length % 2 == 1) {
            return false;
        }
        for (int i = 0; i < values.length; i += 2) {
            if (values[i] instanceof String) {
                setValue(this, (String) values[i], values[i + 1], NEW);
            }
        }
        return true;
    }

    /**
     * To string.
     *
     * @return the string
     */
    @Override
    public String toString() {
        return toString(0, 0);
    }

    /**
     * To string.
     *
     * @param indentFactor the indent factor
     * @return the string
     */
    public String toString(int indentFactor) {
        return toString(indentFactor, 0);
    }

    private void addChildren(StringBuilder sb, String spaces) {
        if (sb == null) {
            return;
        }
        for (String property : getProperties()) {
            Object value = getValue(this, property);
            if (value != null) {
                sb.append(spaces);
                sb.append("<" + property.substring(0, property.length() - 1) + ">");
                sb.append(value);
                sb.append("</" + property.substring(0, property.length() - 1) + ">");
            }
        }
    }

    /**
     * With artifact.
     *
     * @param groupId the group id
     * @param artifactId the artifact id
     * @param version the version
     * @return the artifact file
     */
    public ArtifactFile withArtifact(String groupId, String artifactId, String version) {
        withGroupId(groupId);
        withArtifactId(artifactId);
        withVersion(version);
        return this;
    }

    protected String toString(int indentFactor, int indent) {
        String spacesChild = "";
        String spaces = "";
        if (indentFactor > 0) {
            spacesChild = "\r\n" + StringUtil.repeat(' ', indent + indentFactor);
        }
        spaces = StringUtil.repeat(' ', indent);
        StringBuilder sb = new StringBuilder(spaces);
        if (tag == TAG && !simpleFormat) {
            sb.append("<" + tag
                    + " xsi:schemaLocation=\"http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd\" xmlns=\"http://maven.apache.org/POM/4.0.0\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">");
        } else {
            sb.append("<" + tag + ">");
        }
        addChildren(sb, spacesChild);

        if (!simpleFormat && dependencies != null && dependencies.size() > 0) {
            sb.append(spacesChild + "<dependencies>");
            for (ArtifactFile item : dependencies) {
                sb.append("\r\n" + item.toString(indentFactor, indent + indentFactor + indentFactor));
            }
            sb.append(spacesChild + "</dependencies>");
        }
        if (indentFactor > 0) {
            sb.append("\r\n");
        }
        sb.append(spaces + "</" + tag + ">");

        return sb.toString();
    }

    /**
     * Gets the value.
     *
     * @param key the key
     * @return the value
     */
    public Object getValue(Object key) {
        return getValue(this, "" + key);
    }

    /**
     * Gets the new list.
     *
     * @param keyValue the key value
     * @return the new list
     */
    @Override
    public BaseItem getNewList(boolean keyValue) {
        return new ArtifactFile();
    }

    /**
     * Gets the sendable instance.
     *
     * @param prototyp the prototyp
     * @return the sendable instance
     */
    @Override
    public Object getSendableInstance(boolean prototyp) {
        return new ArtifactFile();
    }

    /**
     * Gets the properties.
     *
     * @return the properties
     */
    @Override
    public String[] getProperties() {
        return new String[]{PROPERTY_MODELVERSION, PROPERTY_GROUPID, PROPERTY_ARTIFACTID,
                PROPERTY_VERSION, PROPERTY_SCOPE, PROPERTY_LICENCES, PROPERTY_DEPENDENCIES};
    }

    /**
     * Gets the value.
     *
     * @param entity    the entity
     * @param attribute the attribute
     * @return the value
     */
    @Override
    public Object getValue(Object entity, String attribute) {
        if (PROPERTY_MODELVERSION.equals(attribute)) {
            return ((ArtifactFile) entity).getModelVersion();
        }
        if (PROPERTY_GROUPID.equals(attribute)) {
            return ((ArtifactFile) entity).getGroupId();
        }
        if (PROPERTY_ARTIFACTID.equals(attribute)) {
            return ((ArtifactFile) entity).getArtifactId();
        }
        if (PROPERTY_VERSION.equals(attribute)) {
            return ((ArtifactFile) entity).getVersion();
        }
        if (PROPERTY_SCOPE.equals(attribute)) {
            return ((ArtifactFile) entity).getScope();
        }
        return null;
    }

    /**
     * Sets the value.
     *
     * @param entity    the entity
     * @param attribute the attribute
     * @param value     the value
     * @param type      the type
     * @return true, if successful
     */
    @Override
    public boolean setValue(Object entity, String attribute, Object value, String type) {
        if (PROPERTY_MODELVERSION.equals(attribute)) {
            ((ArtifactFile) entity).withModelVersion("" + value);
            return true;
        }
        if (PROPERTY_GROUPID.equals(attribute)) {
            ((ArtifactFile) entity).withGroupId("" + value);
            return true;
        }
        if (PROPERTY_ARTIFACTID.equals(attribute)) {
            ((ArtifactFile) entity).withArtifactId("" + value);
            return true;
        }
        if (PROPERTY_VERSION.equals(attribute)) {
            ((ArtifactFile) entity).withVersion("" + value);
            return true;
        }
        if (PROPERTY_SCOPE.equals(attribute)) {
            ((ArtifactFile) entity).withScope("" + value);
            return true;
        }
        return false;
    }

    /**
     * To string.
     *
     * @param converter the converter
     * @return the string
     */
    @Override
    public String toString(Converter converter) {
        if (converter instanceof EntityStringConverter) {
            EntityStringConverter item = (EntityStringConverter) converter;
            return toString(item.getIndentFactor(), item.getIndent());
        }
        if (converter == null) {
            return null;
        }
        return converter.encode(this);
    }

    private Object getChild(XMLEntity xmlEntity, String value) {
        if (value == null || xmlEntity == null) {
            return null;
        }

        boolean isValue = false;
        String property;
        if (value.endsWith("?")) {
            property = value.substring(0, value.length() - 1);
            isValue = true;
        } else {
            property = value;
        }
        Entity child = xmlEntity.getElementBy(XMLEntity.PROPERTY_TAG, property);
        if (child != null) {
            if (isValue) {
                String newValue = ((XMLEntity) child).getValue();
                setValue(this, value, newValue, NEW);
                return newValue;
            }
            return child;
        }
        return null;
    }

    /**
     * With value.
     *
     * @param value the value
     * @return the artifact file
     */
    public ArtifactFile withValue(String value) {
        XMLEntity xmlEntity = new XMLEntity().withValue(value);
        return withValue(xmlEntity);
    }

    /**
     * Gets the licences.
     *
     * @return the licences
     */
    public SimpleList<Licence> getLicences() {
        return licences;
    }

    /**
     * With value.
     *
     * @param xmlEntity the xml entity
     * @return the artifact file
     */
    public ArtifactFile withValue(XMLEntity xmlEntity) {
        for (String property : getProperties()) {
            Object child = getChild(xmlEntity, property);
            if (PROPERTY_LICENCES.equals(property) && child != null) {
                XMLEntity children = (XMLEntity) child;
                for (int i = 0; i < children.sizeChildren(); i++) {
                    BaseItem dependency = children.getChild(i);
                    Licence licence = new Licence().withValue((XMLEntity) dependency);
                    licences.add(licence);
                }
            }
            if (PROPERTY_DEPENDENCIES.equals(property) && child != null) {
                /* Parse Dependency */
                XMLEntity children = (XMLEntity) child;
                for (int i = 0; i < children.sizeChildren(); i++) {
                    BaseItem dependency = children.getChild(i);
                    ArtifactFile pomDependency = new ArtifactFile().withValue((XMLEntity) dependency);
                    pomDependency.withTag("dependency");
                    withDependency(pomDependency);
                }
            }
        }
        return this;
    }

    /**
     * Size.
     *
     * @return the int
     */
    public int size() {
        if (dependencies == null) {
            return 0;
        }
        return dependencies.size();
    }

    /**
     * Gets the dependencies.
     *
     * @return the dependencies
     */
    public SimpleList<ArtifactFile> getDependencies() {
        return dependencies;
    }

    /**
     * Equals.
     *
     * @param obj the obj
     * @return true, if successful
     */
    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof ArtifactFile)) {
            return false;
        }
        String refA = getFullString();
        String refB = ((ArtifactFile) obj).getFullString();
        return refA.equals(refB);
    }

    /**
     * Gets the gradle string.
     *
     * @return the gradle string
     */
    public String getGradleString() {
        return "" + groupId + ":" + artifactId + ":" + version;
    }

    /**
     * Gets the full string.
     *
     * @return the full string
     */
    public String getFullString() {
        return "" + groupId + ":" + artifactId + ":" + version + (scope != null ? (":" + scope) : "");
    }

    /**
     * Compare to.
     *
     * @param o the o
     * @return the int
     */
    @Override
    public int compareTo(ArtifactFile o) {
        if (o == null || o.getVersion() == null) {
            return 1;
        }
        if (getVersion() == null) {
            return -1;
        }
        return getVersion().compareTo(o.getVersion());
    }

    /**
     * Gets the classifier.
     *
     * @return the classifier
     */
    public String getClassifier() {
        if (classifier == null || classifier.size() < 0) {
            return "";
        }
        return classifier.first();
    }

    /**
     * Gets the index.
     *
     * @return the index
     */
    public String getIndex() {
        return index;
    }

    /**
     * With file.
     *
     * @param name the name
     * @return the artifact file
     */
    public ArtifactFile withFile(String name) {
        fileName = name;
        return this;
    }

    /**
     * Gets the file name.
     *
     * @return the file name
     */
    public String getFileName() {
        return fileName;
    }

    /**
     * To path.
     *
     * @return the string
     */
    public String toPath() {
        CharacterBuffer path = new CharacterBuffer();
        if (groupId != null) {
            path.with(groupId.replace(".", "/"));
            path.with('/');
        }
        path.with(artifactId);
        path.with('/');
        path.with(version.toString());
        path.with('/');
        return path.toString();
    }

    /**
     * Gets the classifiers.
     *
     * @return the classifiers
     */
    public SimpleList<String> getClassifiers() {
        return classifier;
    }

    /**
     * Adds the classifier.
     *
     * @param value the value
     * @return true, if successful
     */
    public boolean addClassifier(String value) {
        if (classifier == null) {
            classifier = new SimpleList<String>();
        }
        return classifier.add(value);
    }

    /**
     * To file.
     *
     * @param groupPath  the group path
     * @param classifier the classifier
     * @return the string
     */
    public String toFile(boolean groupPath, String... classifier) {
        CharacterBuffer file = new CharacterBuffer();
        file.with(artifactId);
        if (groupPath) {
            file.with('-');
            file.with(version.toString());
        }
        String myClassifier;
        if (classifier != null && classifier.length == 1 && classifier[0] != null) {
            myClassifier = classifier[0];
        } else {
            myClassifier = getClassifier();
        }
        if (DEFAULT.equals(myClassifier)) {
            file.with("." + DEFAULT);
        } else if ("pom".equals(myClassifier)) {
            file.with(".pom");
        } else {
            file.with('-');
            file.with(myClassifier);
            file.with("." + DEFAULT);
        }
        return file.toString();
    }

    /**
     * Gets the builds the number.
     *
     * @return the builds the number
     */
    public String getBuildNumber() {
        if (version == null) {
            return "0";
        }
        return "" + version.getValue(VersionCondition.REVISION);
    }

    /**
     * Gets the extension.
     *
     * @return the extension
     */
    public String getExtension() {
        if (fileName != null) {
            int pos = fileName.lastIndexOf(".");
            if (pos > 0) {
                return fileName.substring(pos + 1);
            }
        }
        return DEFAULT;
    }

    /**
     * Gets the time.
     *
     * @param defaultTime the default time
     * @return the time
     */
    public String getTime(String defaultTime) {
        if (time != null) {
            return time;
        }
        return defaultTime;
    }

    /**
     * Gets the path.
     *
     * @return the path
     */
    public String getPath() {
    if (fileName != null) {
      fileName = fileName.replace('\\', '/');
      int pos = fileName.lastIndexOf('/');
      if (pos > 0) {
        return fileName.substring(0, pos + 1);
      }
    }
    return "";
  }

  /**
   * With simple format.
   *
   * @param simpleFormat the simple format
   * @return the artifact file
   */
  public ArtifactFile withSimpleFormat(boolean simpleFormat) {
    this.simpleFormat = simpleFormat;
    return this;
  }
}
