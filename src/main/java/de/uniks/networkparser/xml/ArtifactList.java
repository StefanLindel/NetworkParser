package de.uniks.networkparser.xml;

import de.uniks.networkparser.json.JsonArray;
import de.uniks.networkparser.json.JsonObject;
import de.uniks.networkparser.list.SimpleList;
import de.uniks.networkparser.list.SortedSet;

/**
 * May be a List of Pom of one or more Libaries.
 *
 * @author Stefan Lindel
 */
public class ArtifactList extends SortedSet<ArtifactFile> {
    /**
     * The is show meta data.
     */
    public boolean isShowMetaData;
    private String groupId;
    private String artifactId;
    private SortedSet<ArtifactList> children;

    /**
     * Instantiates a new artifact list.
     */
    public ArtifactList() {
        super(true);
    }

    /**
     * Adds the.
     *
     * @param value the value
     * @return true, if successful
     */
    @Override
    public boolean add(ArtifactFile value) {
        if (value == null) {
            return false;
        }
        if (children == null) {
            if (groupId == null) {
                addItem(value);
            } else if (groupId.equals(value.getGroupId())) {
                /* Check For Dupplicate */
                boolean found = false;
                for (ArtifactFile child : this) {
                    if (value.getVersion().isSnapshot() != child.getVersion().isSnapshot()) {
                        continue;
                    }
                    if (value.getArtifactId() == null) {
                        continue;
                    }
                    if (!value.getArtifactId().equals(child.getArtifactId())) {
                        continue;
                    }
                    if (!value.getVersion().equals(child.getVersion())) {
                        continue;
                    }
                    child.addClassifier(value.getClassifier());
                    found = true;
                    break;
                }
                if (!found) {
                    addItem(value);
                }
            } else {
                /* REFACTORING !!! */
                ArtifactList subList;
                if (children == null) {
                    children = new SortedSet<ArtifactList>();
                }
                if (size() > 0) {
                    subList = new ArtifactList();
                    subList.addAll(this);
                    children.add(value);
                    clear();
                }
                subList = new ArtifactList();
                subList.add(value);
                children.add(subList);
            }
        } else {
            /* PARENT LIST CHECK FOR CHILD */
            String id = value.getGroupId();
            for (ArtifactList childList : children) {
                if (childList.getGroup().equals(id)) {
                    childList.add(value);
                    id = null;
                    break;
                }
            }
            if (id != null) {
                /* So Element not added */
                ArtifactList subList = new ArtifactList();
                subList.add(value);
                children.add(subList);
            }
        }
        return true;
    }

    private boolean addItem(ArtifactFile pom) {
        if (pom == null) {
            return false;
        }
        super.add(pom);
        if (groupId == null) {
            groupId = pom.getGroupId();
        }
        if (artifactId == null) {
            artifactId = pom.getArtifactId();
        }
        return true;
    }

    /**
     * Clear.
     */
    @Override
    public void clear() {
        super.clear();
        artifactId = null;
        groupId = null;
    }

    /**
     * Gets the group.
     *
     * @return the group
     */
    public String getGroup() {
        return groupId;
    }

    /**
     * Gets the artifact.
     *
     * @return the artifact
     */
    public String getArtifact() {
        return artifactId;
    }

    /**
     * To json.
     *
     * @return the json object
     */
    public JsonObject toJson() {
        JsonObject json = new JsonObject();
        json.add("groupid", groupId);
        json.add("name", artifactId);
        JsonArray versions = new JsonArray();
        json.add("versions", versions);
        ArtifactFile biggest = this.getLast();
        if (biggest != null) {
            json.add("update", biggest.toPath() + biggest.toFile(true));
        }
        for (ArtifactFile file : this) {
            JsonObject jsonVersion = new JsonObject();
            SimpleList<String> classifiers = file.getClassifiers();
            jsonVersion.add("classifier", new JsonArray().withList(classifiers));
            jsonVersion.add("version", file.getVersion());
            if (file.getVersion().isSnapshot()) {
                jsonVersion.add("snapshot", true);
            }
            versions.add(jsonVersion);
        }
        return json;
    }

    /**
     * Return Latest Version of Artefact
     *
     * @param snapshot ius Snapshot
     * @return Version
     */
    public ArtifactFile getLast(boolean snapshot) {
        if (this.size() < 1) {
            return null;
        }
        for (int i = this.size() - 1; i >= 0; i--) {
            ArtifactFile file = get(i);
            if ((snapshot && file.getVersion().isSnapshot()) || (!snapshot && file.isRelease())) {
                return file;
            }
        }
        return null;
    }

    /**
     * To string.
     *
     * @return the string
     */
    public String toString() {
        return toJson().toString();
    }

    /**
     * Gets the version.
     *
     * @return the version
     */
    public String getVersion() {
        ArtifactFile biggest = this.getLast();
        if (biggest != null) {
      return biggest.getVersion().toString();
    }
    return "";
  }
}
