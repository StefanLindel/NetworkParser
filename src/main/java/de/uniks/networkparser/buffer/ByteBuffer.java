package de.uniks.networkparser.buffer;

import de.uniks.networkparser.bytes.ByteConverter;
import de.uniks.networkparser.interfaces.Converter;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;

/**
 * ByteBuffer .
 *
 * @author Stefan Lindel
 */
public class ByteBuffer extends BufferedBuffer {
    /**
     * The buffer.
     */
    protected byte[] buffer;
    private int bits = 0;
    private int nextBitMask = 0x100; /* triggers readOctet first time */

    /**
     * Allocate new Memory.
     *
     * @param len the len
     * @return the byte buffer
     */
    public static ByteBuffer allocate(int len) {
        if (len < 1) {
            return null;
        }
        ByteBuffer bytesBuffer = new ByteBuffer().withBufferLength(len);
        return bytesBuffer;
    }

    /**
     * Char at postion
     *
     * @param index the index
     * @return the char
     */
    @Override
    public char charAt(int index) {
        if (index < 0 || buffer == null || index >= buffer.length) {
            return 0;
        }
        return (char) buffer[index];
    }

    /**
     * Byte at position
     *
     * @param index the index
     * @return the byte
     */
    @Override
    public byte byteAt(int index) {
        if (index < 0 || buffer == null || index >= buffer.length) {
            return 0;
        }
        return buffer[index];
    }

    /**
     * Sub sequence.
     *
     * @param start the start
     * @param end   the end
     * @return the character buffer
     */
    @Override
    public CharacterBuffer subSequence(int start, int end) {
        start += this.start;
        end += this.start;
        int len = end - start;
        if (len < 0 || buffer == null) {
            return new CharacterBuffer();
        }
        byte[] sub = new byte[len];
        if (start < buffer.length && len <= buffer.length) {
            System.arraycopy(buffer, start + 0, sub, 0, len);
        }
        return new CharacterBuffer().with(sub, 0, len);
    }

    /**
     * With buffer length.
     *
     * @param length the length
     * @return the byte buffer
     */
    public ByteBuffer withBufferLength(int length) {
        if (length >= 0) {
            buffer = new byte[length];
        }
        return this;
    }

    /**
     * Gets the char.
     *
     * @return the char
     */
    @Override
    public char getChar() {
        int no = getByte();
        if (no >= 0) {
            return (char) no;
        }
        return (char) (no << 8 + (char) getByte());
    }

    /**
     * Gets the byte.
     *
     * @return the byte
     */
    @Override
    public byte getByte() {
        if (buffer == null || position >= buffer.length - 1) {
            return 0;
        }
        return buffer[++position];
    }

    /**
     * Get a Byte Field.
     *
     * @param parameter Null for full, Number Length, or new Byte Array
     * @return a ByteArray
     */
    public byte[] getBytes(Object... parameter) {
        if (buffer == null || position >= buffer.length - 1) {
            return new byte[0];
        }

        int len = length;
        byte[] result = null;
        if (parameter != null && parameter.length > 0) {
            if (parameter[0] instanceof Integer) {
                len = (Integer) parameter[0];
            } else if (parameter[0] instanceof Byte[] || parameter[0] instanceof byte[]) {
                result = (byte[]) parameter[0];
                len = result.length;
            }
        }
        if (result == null) {
            result = new byte[len];
        }
        for (int i = 0; i < len; i++) {
            result[i] = buffer[++position];
        }
        return result;
    }

    /**
     * Init the new ByteArray.
     *
     * @param values the reference ByteArray
     * @param start  the Startposition for the new ByteBuffer
     * @param end    the Endposition for the new ByteBuffer
     * @return the new ByteBuffer
     */
    public ByteBuffer withReference(byte[] values, int start, int end) {
        buffer = values;
        this.start = start;
        if (end > values.length) {
            end = buffer.length;
        }
        length = end - start;
        return this;
    }

    /**
     * Gets the value.
     *
     * @param start the start
     * @param len   the len
     * @return the value
     */
    public byte[] getValue(int start, int len) {
        withPosition(start);
        if (len < 0) {
            len = 0;
        }
        byte[] array = new byte[len];
        for (int i = 0; i < len; i++) {
            array[i] = getByte();
        }
        return array;
    }

    /**
     * Array.
     *
     * @return the byte[]
     */
    public byte[] array() {
        return buffer;
    }

    /**
     * Adds the.
     *
     * @param values the values
     * @return true, if successful
     */
    public boolean add(Object... values) {
        if (values == null) {
            return true;
        }
        for (Object item : values) {
            insert(item, true);
        }
        return true;
    }

    /**
     * Insert.
     *
     * @param item        the item
     * @param bufferAdEnd the buffer ad end
     * @return true, if successful
     */
    public boolean insert(Object item, boolean bufferAdEnd) {
        if (item instanceof Byte) {
            return addBytes(item, 1, bufferAdEnd);
        }
        if (item instanceof Character) {
            return addBytes(item, 1, bufferAdEnd);
        }
        if (item instanceof String) {
            String str = (String) item;
            byte[] array = str.getBytes();
            return addBytes(array, array.length, bufferAdEnd);
        }
        if (item instanceof CharSequence) {
            CharSequence str = (CharSequence) item;
            resize(str.length(), bufferAdEnd);
            for (int i = 0; i < str.length(); i++) {
                addBytes(str.charAt(i), 1, bufferAdEnd);
            }
            return true;
        }
        if (item instanceof byte[]) {
            byte[] array = (byte[]) item;
            return addBytes(array, array.length, bufferAdEnd);
        }
        if (item instanceof Byte[]) {
            Byte[] array = (Byte[]) item;
            return addBytes(array, array.length, bufferAdEnd);
        }
        if (item instanceof Integer) {
            return addBytes(item, 4, bufferAdEnd);
        }
        if (item instanceof Short) {
            return addBytes(item, 2, bufferAdEnd);
        }
        if (item instanceof Long) {
            return addBytes(item, 8, bufferAdEnd);
        }
        if (item instanceof Boolean) {
            return addBytes(item, 1, bufferAdEnd);
        }
        return false;
    }

    private boolean resize(int len, boolean bufferAtEnd) {
        int bufferLen = 0;
        if (buffer != null) {
            bufferLen = buffer.length;
        }
        /* Add ad end of Array */
        if (position < 0) {
            /* New Size with Buffer */
            int newSize;
            if (bufferAtEnd) {
                newSize = (length + len) + (length + len) / 2 + 5;
            } else {
                newSize = length + len;
            }
            if (newSize < 0) {
                newSize = 0;
            }
            byte[] oldBuffer = buffer;
            buffer = new byte[newSize];
            int oldSize = 0;
            if (oldBuffer != null) {
                oldSize = oldBuffer.length;
                if (length > 0) {
                    System.arraycopy(oldBuffer, oldBuffer.length - length, buffer, newSize - length, length);
                }
            }
            position += newSize - oldSize;
            return true;
        } else if (position + len > bufferLen) {
            /* New Size with Buffer */
            if (bufferLen > 0) {
                int newSize;
                if (bufferAtEnd) {
                    newSize = (length + len) + (length + len) / 2 + 5;
                } else {
                    newSize = length + len;
                }
                byte[] oldBuffer = buffer;
                buffer = new byte[newSize];
                System.arraycopy(oldBuffer, 0, buffer, 0, position);
            } else {
                buffer = new byte[len];
            }
            return true;
        }
        return false;
    }

    /**
     * Adds the bytes.
     *
     * @param value       the value
     * @param len         the len
     * @param bufferAtEnd the buffer at end
     * @return true, if successful
     */
    public boolean addBytes(Object value, int len, boolean bufferAtEnd) {
        boolean addEnd = position == length;
        resize(len, bufferAtEnd);
        /* one Byte */
        if (value instanceof Byte) {
            buffer[position] = (Byte) value;
        } else {
            if (value instanceof byte[]) {
                byte[] source = (byte[]) value;
                if (buffer != null && buffer.length >= position + len) {
                    if (len >= 0) System.arraycopy(source, 0, buffer, position + 0, len);
                }
            } else if (value instanceof Byte[]) {
                Byte[] source = (Byte[]) value;
                if (buffer != null && buffer.length >= position + len) {
                    for (int i = 0; i < len; i++) {
                        buffer[position + i] = source[i];
                    }
                }
            } else if (value instanceof Character) {
                char charItem = (Character) value;
                if (len == 1) {
                    buffer[position] = (byte) (charItem);
                } else {
                    buffer[position] = (byte) (charItem >>> 8);
                    buffer[position + 1] = (byte) charItem;
                }
                /* two Bytes */
            } else if (value instanceof Short) {
                short item = (Short) value;
                buffer[position] = (byte) (item >>> 8);
                buffer[position + 1] = (byte) item;
                /* four Bytes */
            } else if (value instanceof Integer) { /* value instanceof Float */
                Integer item = (Integer) value;
                buffer[position] = (byte) (item >>> 24);
                buffer[position + 1] = (byte) (item >>> 16);
                buffer[position + 2] = (byte) (item >>> 8);
                buffer[position + 3] = (byte) (item & 0xff);
            } else if (value instanceof Long) { /* value instanceof Double */
                Long item = (Long) value;
                buffer[position] = (byte) (item >>> 56);
                buffer[position + 1] = (byte) (item >>> 48);
                buffer[position + 2] = (byte) (item >>> 40);
                buffer[position + 3] = (byte) (item >>> 32);
                buffer[position + 4] = (byte) (item >>> 24);
                buffer[position + 5] = (byte) (item >>> 16);
                buffer[position + 6] = (byte) (item >>> 8);
                buffer[position + 7] = (byte) (item & 0xff);
            } else if (value instanceof Boolean) { /* value instanceof Float */
                Boolean item = (Boolean) value;
                if (item) {
                    buffer[position] = 1;
                } else {
                    buffer[position] = 0;
                }
            }
        }
        length += len;
        if (addEnd) {
            position = length;
        }
        return true;
    }

    /**
     * Put.
     *
     * @param value the value
     * @return true, if successful
     */
    public boolean put(byte value) {
        if (buffer != null && position + 1 <= buffer.length) {
            buffer[position++] = value;
            length++;
            return true;
        }
        return false;
    }

    /**
     * Put.
     *
     * @param value the value
     * @return true, if successful
     */
    public boolean put(short value) {
        if (buffer != null && position + 2 <= buffer.length) {
            buffer[position++] = (byte) (value >>> 8);
            buffer[position++] = (byte) value;
            length += 2;
            return true;
        }
        return false;
    }

    /**
     * Put.
     *
     * @param value the value
     * @return true, if successful
     */
    public boolean put(int value) {
        if (buffer != null && position + 4 <= buffer.length) {
            buffer[position++] = (byte) (value >>> 24);
            buffer[position++] = (byte) (value >>> 16);
            buffer[position++] = (byte) (value >>> 8);
            buffer[position++] = (byte) value;
            length += 4;
            return true;
        }
        return false;
    }

    /**
     * Put.
     *
     * @param value the value
     * @return true, if successful
     */
    public boolean put(long value) {
        if (buffer != null && position + 8 <= buffer.length) {
            buffer[position++] = (byte) (value >>> 56);
            buffer[position++] = (byte) (value >>> 48);
            buffer[position++] = (byte) (value >>> 40);
            buffer[position++] = (byte) (value >>> 32);
            buffer[position++] = (byte) (value >>> 24);
            buffer[position++] = (byte) (value >>> 16);
            buffer[position++] = (byte) (value >>> 8);
            buffer[position++] = (byte) value;
            length += 8;
            return true;
        }
        return false;
    }

    /**
     * Put.
     *
     * @param value the value
     * @return true, if successful
     */
    public boolean put(char value) {
        if (buffer != null && position + 2 <= buffer.length) {
            buffer[position++] = (byte) (value >>> 8);
            buffer[position++] = (byte) value;
            length += 2;
            return true;
        }
        return false;
    }

    /**
     * Put.
     *
     * @param value the value
     * @return true, if successful
     */
    public boolean put(float value) {
        int bits = Float.floatToIntBits(value);
        if (buffer != null && position + 4 <= buffer.length) {
            buffer[position++] = (byte) (bits & 0xff);
            buffer[position++] = (byte) ((bits >> 8) & 0xff);
            buffer[position++] = (byte) ((bits >> 16) & 0xff);
            buffer[position++] = (byte) ((bits >> 24) & 0xff);
            length += 4;
            return true;
        }
        return false;
    }

    /**
     * Put.
     *
     * @param value the value
     * @return true, if successful
     */
    public boolean put(double value) {
        long bits = Double.doubleToLongBits(value);
        if (buffer != null && position + 8 <= buffer.length) {
            buffer[position++] = (byte) ((bits >> 56) & 0xff);
            buffer[position++] = (byte) ((bits >> 48) & 0xff);
            buffer[position++] = (byte) ((bits >> 40) & 0xff);
            buffer[position++] = (byte) ((bits >> 32) & 0xff);
            buffer[position++] = (byte) ((bits >> 24) & 0xff);
            buffer[position++] = (byte) ((bits >> 16) & 0xff);
            buffer[position++] = (byte) ((bits >> 8) & 0xff);
            buffer[position++] = (byte) ((bits >> 0) & 0xff);
            length += 8;
            return true;
        }
        return false;
    }

    /**
     * Put.
     *
     * @param value the value
     * @return true, if successful
     */
    public boolean put(byte[] value) {
        if (value != null) {
            for (int i = 0; i < value.length; i++) {
                put(value[i]);
            }
            return true;
        }
        return false;
    }

    /**
     * Put.
     *
     * @param value the value
     * @param offset the offset
     * @param length the length
     */
    public void put(byte[] value, int offset, int length) {
        if (value == null || offset < 0 || offset > value.length) {
            return;
        }
        for (int i = 0; i < length; i++) {
            put(value[offset + i]);
        }
    }

    /**
     * Flip.
     *
     * @param preFirst the pre first
     * @return the byte buffer
     */
    public ByteBuffer flip(boolean preFirst) {
        if (preFirst) {
            position = -1;
            return this;
        }
        position = 0;
        return this;
    }

    /**
     * Gets the new buffer.
     *
     * @param capacity the capacity
     * @return the new buffer
     */
    public ByteBuffer getNewBuffer(int capacity) {
        return new ByteBuffer().withBufferLength(capacity);
    }

    /**
     * Gets the new buffer.
     *
     * @param array the array
     * @return the new buffer
     */
    public ByteBuffer getNewBuffer(byte[] array) {
        return new ByteBuffer().with(array);
    }

    /**
     * With.
     *
     * @param string the string
     * @return the byte buffer
     */
    public ByteBuffer with(CharSequence... string) {
        if (string != null && string.length > 0 && string[0] instanceof String) {
            buffer = ((String) string[0]).getBytes(Charset.forName(ENCODING));
            position = 0;
            length = buffer.length;
        }
        return this;
    }

    /**
     * With.
     *
     * @param array the array
     * @return the byte buffer
     */
    public ByteBuffer with(byte... array) {
        buffer = array;
        position = 0;
        if (array != null) {
            length = array.length;
        } else {
            length = 0;
        }
        return this;
    }

    /**
     * With.
     *
     * @param value the value
     * @return the byte buffer
     */
    public ByteBuffer with(byte value) {
        if (buffer == null) {
            buffer = new byte[]{value};
            length = 1;
            position = 0;
        } else if (length < buffer.length) {
            buffer[length++] = value;
        }
        return this;
    }

    /**
     * To string.
     *
     * @return the string
     */
    @Override
    public String toString() {
        return toString(new ByteConverter());
    }

    /**
     * Gets the new list.
     *
     * @param keyValue the key value
     * @return the new list
     */
    @Override
    public ByteBuffer getNewList(boolean keyValue) {
        return new ByteBuffer();
    }

    /**
     * Size.
     *
     * @return the int
     */
    @Override
    public int size() {
        return length();
    }

    /**
     * String.
     *
     * @return the string
     */
    public String string() {
        return new String(buffer);
    }

    /**
     * To string.
     *
     * @param converter the converter
     * @return the string
     */
    public String toString(Converter converter) {
        if (converter == null) {
            converter = new ByteConverter();
        }
        if (converter instanceof ByteConverter) {
            if (start > 0) {
                return new String(buffer, start, length);
            }
            return ((ByteConverter) converter).toString(toBytes());
        }
        return String.valueOf(buffer);
    }

    /**
     * With.
     *
     * @param array the array
     * @param len   the len
     * @return the byte buffer
     */
    public ByteBuffer with(byte[] array, int start, int len) {
        position = 0;
        if (array == null) {
            return this;
        }
        if (len < 0 || len > array.length) {
            len = array.length;
        }
        if (buffer == null) {
            buffer = array;
            this.start = start;
            length = len;
        } else {
            /* Resize */
            byte[] oldBuffer = buffer;
            buffer = new byte[length * 2 + len];
            System.arraycopy(oldBuffer, 0, buffer, 0, length);
            System.arraycopy(array, start, buffer, length, len);
            length += len;
        }
        return this;
    }

    /**
     * With.
     *
     * @param values the values
     * @param start  the start
     * @param len    the len
     * @return the byte buffer
     */
    public ByteBuffer with(char[] values, int start, int len) {
        if (values == null || start + length > values.length) {
            return this;
        }
        if (length < 0) {
            length = buffer.length;
        }
        int newLen = length + length;
        if (buffer == null || newLen + start > buffer.length) {
            byte[] oldValue = buffer;
            buffer = new byte[(newLen * 2 + 2)];
            if (oldValue != null) {
                System.arraycopy(oldValue, start, buffer, 0, length);
            }
            start = 0;
            position = 0;
        }
        System.arraycopy(values, start, buffer, length, length);
        length = newLen;
        return this;
    }

    /**
     * Public API - reads a bit/boolean argument.
     *
     * @return boolean
     */

    public boolean getBit() {
        if (nextBitMask > 0x80) {
            bits = getByte();
            nextBitMask = 0x01;
        }
        boolean result = (bits & nextBitMask) != 0;
        nextBitMask = nextBitMask << 1;
        return result;
    }

    /**
     * Convenience method - reads a short string from a DataInput Stream.
     *
     * @return a new String
     */
    public String getShortstr() {
        final int contentLength = getByte() & 0xff;
        byte[] b = getBytes(new byte[contentLength]);
        return new String(b);
    }

    /**
     * Sets the.
     *
     * @param pos   the pos
     * @param value the value
     * @return true, if successful
     */
    public boolean set(int pos, byte value) {
        if (pos >= 0 && pos <= length && buffer != null) {
            buffer[pos] = value;
            return true;
        }
        return false;
    }

    /**
     * Adds the stream.
     *
     * @param stream the stream
     * @return true, if successful
     */
    public boolean addStream(InputStream stream) {
        if (stream == null) {
            return true;
        }
        try {
            resize(stream.available(), true);
        } catch (IOException e) {
            return false;
        }
        return putStream(stream);
    }

    /**
     * Put stream.
     *
     * @param stream the stream
     * @return true, if successful
     */
    public boolean putStream(InputStream stream) {
        try {
            byte[] buf = new byte[1048];
            do {
                int read = stream.read(buf);
                if (read > 0) {
                    put(buf, 0, read);
                }
            } while (stream.available() > 0);
            return true;
        } catch (Exception e) {
        }
        return false;
    }

    @Override
    public BufferedBuffer findLine(String element) {
        ByteBuffer result = new ByteBuffer();
        int pos = indexOf(element);
        if (pos < 0) {
            return result;
        }
        result.withPosition(pos);
        int start = lastIndexOf('\n', pos + 1);
        int end = indexOf("\n", pos);
        if (end < 0) {
            end = length;
        }
        result.withReference(buffer, start, end);
        return result;
    }

    /**
     * Index of.
     *
     * @param str       the str
     * @param fromIndex the from index
     * @return the int
     */
    public int indexOf(CharSequence str, int fromIndex) {
        final int max = length();
        if (fromIndex < 0) {
            fromIndex = 0;
        } else if (fromIndex >= length) {
            /* Note: fromIndex might be near -1>>>1. */
            return -1;
        }
        if (str == null || str.length() == 0) {
            return -1;
        }
        final int len = str.length() - 1;
        int pos = 0;
        for (int i = fromIndex; i < max; i++) {
            if (buffer[i + start] == str.charAt(pos)) {
                if (pos == len) {
                    return i - pos;
                }
                pos++;
            } else {
                pos = 0;
            }
        }
        return -1;
    }

    /**
     * Last index of.
     *
     * @param character the character for search
     * @param offset    from index
     * @return the position or -1 if not found
     */
    public int lastIndexOf(char character, int offset) {
        for (int i = offset - 1; i >= start; i--) {
            if (buffer[i] == character) {
                return i - start;
            }
        }
        return -1;
    }

    /**
     * Replace.
     *
     * @param start   the start
     * @param end     the end
     * @param replace the replace
     * @return true, if successful
     */
    public boolean replace(int start, int end, String replace) {
        int pos = 0;
        if (replace == null || buffer == null || start > end || start > buffer.length) {
            return false;
        }
        int diff = replace.length() - (end - start);
        if (diff < 1 || (this.start + length) + diff <= buffer.length) {
            // Simple Case
            // MOVE THE LAST TO GET En

            start += this.start;
            end += this.start;
            if (diff != 0) {
                System.arraycopy(buffer, end, buffer, end + diff, (this.start + length));
            }
            while (pos < replace.length()) {
                buffer[start++] = (byte) replace.charAt(pos++);
            }
            length = length + diff;
            position = 0;
            return true;
        }
        /* Argh array is to Small */
        int newCapacity = (length + diff) * 2 + 2;
        byte[] oldChar = buffer;
        byte[] copy = new byte[newCapacity];
        // RESIZE BUFFER AND COPY OLD DATA TO LEFT BUFFER
        System.arraycopy(buffer, this.start, copy, 0, this.start + start);
        buffer = copy;
        int oldStart = this.start + end;
        int endLen = length - oldStart;
        while (pos < replace.length()) {
            buffer[start++] = (byte) replace.charAt(pos++);
        }
        System.arraycopy(oldChar, oldStart, buffer, start, endLen);

        length = length + diff;
        position = 0;
        return true;
    }

    public ByteBuffer nextLine(int pos) {
        return nextStop(pos, "\n");
    }

    /**
     * @param pos  Postition
     * @param stop Stopstring
     * @return nextString
     */
    public ByteBuffer nextStop(int pos, String stop) {
    return new ByteBuffer().withReference(buffer, pos, indexOf(stop, pos));
  }

  /**
   * Return a Number of Byte
   * 
   * @return a Number
   */
  public int getNumber() {
    byte[] bytes = array();
    if (bytes.length < 1) {
      return 0;
    }
    int value = bytes[0];
    int i = 1;
    while (i < bytes.length) {
      value = (value << 8) + (bytes[i++] & 0xff);
    }
    return value;
  }
}
