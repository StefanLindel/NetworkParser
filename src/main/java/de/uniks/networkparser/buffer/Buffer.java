package de.uniks.networkparser.buffer;

import de.uniks.networkparser.StringUtil;
import de.uniks.networkparser.interfaces.BufferItem;
import de.uniks.networkparser.list.SimpleList;

/**
 * Interface for Buffer For Tokener to parse some Values.
 *
 * @author Stefan
 */
public abstract class Buffer implements BufferItem {
    /**
     * The Constant ENDLINE.
     */
    public static final char ENDLINE = '\n';
    /**
     * The Constant EMPTY Buffer.
     */
    public static final String EMPTY = "";
    /**
     * The Constant QUOTE CHAR.
     */
    protected static final char[] QUOTE = new char[]{'"'};

    /** The index of Buffer. */
    protected int position;

    /**
     * Convert a Byte from -128 - 127 to 0 - 255
     *
     * @param value the byte
     * @return a formated byte
     */
    public static long convertByte(byte value) {
        return (value < 0) ? value + 256 : value;
    }

    /**
     * Gets the short.
     *
     * @return the short
     */
    public short getShort() {
        byte[] bytes = array(Short.SIZE / Byte.SIZE, false);
        return (short) ((bytes[0] << 8) + (bytes[1] & 0xFF));
    }

    /**
     * Gets the int.
     *
     * @return the int
     */
    public int getInt() {
        byte[] bytes = array(Integer.SIZE / Byte.SIZE, false);
        return ((bytes[0] << 24) + (bytes[1] << 16) + (bytes[2] << 8) + bytes[3]);
    }

    /**
     * Gets the unsigned int.
     *
     * @return the unsigned int
     */
    public int getUnsignedInt() {
        byte[] bytes = array(Integer.SIZE / Byte.SIZE, false);
        return (((bytes[0] & 0xff) << 24) + ((bytes[1] & 0xff) << 16) + ((bytes[2] & 0xff) << 8)
                + (bytes[3] & 0xff));
    }

    /**
     * Read resource.
     *
     * @param file the file
     * @return the character buffer
     */
    public CharacterBuffer readResource(String file) {
        return new CharacterBuffer();
    }

    /**
     * Gets the long.
     *
     * @return the long
     */
    public long getLong() {
        byte[] bytes = array(Long.SIZE / Byte.SIZE, false);
        long result = bytes[0];
        result = result << 8 + bytes[1];
        result = result << 8 + bytes[2];
        result = result << 8 + bytes[3];
        result = result << 8 + bytes[4];
        result = result << 8 + bytes[5];
        result = result << 8 + bytes[6];
        result = result << 8 + bytes[7];
        return result;
    }

    /**
     * Gets the float.
     *
     * @return the float
     */
    public float getFloat() {
        int asInt = getInt();
        return Float.intBitsToFloat(asInt);
    }

    /**
     * Substring.
     *
     * @param values the values
     * @return the string
     */
    public String substring(int... values) {
        return EMPTY;
    }

    /**
     * Next.
     *
     * @param positions the positions
     * @return the string
     */
    public String next(int... positions) {
        return EMPTY;
    }

    /**
     * Gets the double.
     *
     * @return the double
     */
    public double getDouble() {
        long asLong = getLong();
        return Double.longBitsToDouble(asLong);
    }

    /**
     * Array.
     *
     * @param len     the len
     * @param current the current
     * @return the byte[]
     */
    public byte[] array(int len, boolean current) {
        if (len == -1) {
            len = remaining();
        } else if (len == -2) {
            len = length();
        }
        if (len < 0) {
            return null;
        }
        byte[] result = new byte[len];
        int start = 0;
        if (current) {
            if (len > 0) {
                if (position < 0) {
                    position = 0;
                }
                result[0] = (byte) getCurrentChar();
            }
            start = 1;
        }
        for (int i = start; i < len; i++) {
            result[i] = getByte();
        }
        return result;
    }

    /**
     * Convert to new ByteArray
     *
     * @return a new ByteArry
     */
    public Byte[] arrayObject() {
        Byte[] result = new Byte[remaining()];
        for (int i = 0; i < result.length; i++) {
            result[i] = getByte();
        }
        return result;
    }

    /**
     * Gets the byte.
     *
     * @return the byte
     */
    @Override
    public byte getByte() {
        return (byte) getChar();
    }

    /**
     * Gets the boolean.
     *
     * @return the boolean
     */
    public boolean getBoolean() {
        int ch = getChar();
        return ch != 1 && ch != 0;
    }

    /**
     * Position.
     *
     * @return the int
     */
    @Override
    public int position() {
        return position;
    }

    /**
     * Remaining.
     *
     * @return the int
     */
    @Override
    public int remaining() {
        int remaining = length() - position();
        if (remaining > 0) {
            return remaining - 1;
        }
        return 0;
    }

    /**
     * Checks if is empty.
     *
     * @return true, if is empty
     */
    @Override
    public boolean isEmpty() {
        return length() == 0;
    }

    /**
     * Checks if is end.
     *
     * @return true, if is end
     */
    @Override
    public boolean isEnd() {
        return position() >= length();
    }

    /**
     * Gets the string.
     *
     * @param len the len
     * @return the string
     */
    public CharacterBuffer getString(int len) {
        CharacterBuffer result = new CharacterBuffer();
        if (len < 1) {
            return result;
        }
        result.withBufferLength(len);
        result.with(getCurrentChar());
        for (int i = 1; i < len; i++) {
            result.with(getChar());
            if (isEnd()) {
                break;
            }
        }
        return result;
    }

    /**
     * Read line.
     *
     * @return the character buffer
     */
    public CharacterBuffer readLine() {
        CharacterBuffer line = new CharacterBuffer();
        char character = getCurrentChar();
        while (character != '\r' && character != '\n' && character != 0) {
            line.with(character);
            character = getChar();
        }
        if (character == '\r') {
            character = getChar();
        }
        if (character == '\n') {
            skip();
        }
        return line;
    }

    /**
     * Next clean.
     *
     * @return the char
     */
    @Override
    public char nextClean() {
        char c = getCurrentChar();
        if (c > ' ') {
            return c;
        }
        do {
            c = getChar();
        } while (c != 0 && c <= ' ');
        return c;
    }

    /**
     * Next clean.
     *
     * @return the char
     */
    @Override
    public char nextCleanSkip() {
        char c;
        do {
            c = getChar();
        } while (c != 0 && c <= ' ');
        return c;
    }

    /**
     * Next string.
     *
     * @param quotes the quotes
     * @return the character buffer
     */
    @Override
    public CharacterBuffer nextString(char... quotes) {
        if (quotes == null) {
            quotes = QUOTE;
        }
        CharacterBuffer result = nextString(false, quotes);
        skip();
        return result;
    }

    /**
     * Next string.
     *
     * @return the character buffer
     */
    public CharacterBuffer nextString() {
        nextClean();
        boolean isQuote = getCurrentChar() == QUOTES;
        if (isQuote) {
            skipChar(QUOTES);
            return nextString(QUOTES);
        }
        nextClean();
        return nextString(SPACE, ENDLINE);
    }

    /**
     * Next string.
     *
     * @param allowQuote the allow quote
     * @param quotes     the quotes
     * @return the character buffer
     */
    public CharacterBuffer nextString(boolean allowQuote, char... quotes) {
        if (getCurrentChar() == 0 || quotes == null) {
            return new CharacterBuffer();
        }
        char c = getCurrentChar();
        int i = 0;
        for (i = 0; i < quotes.length; i++) {
            if (c == quotes[i]) {
                break;
            }
        }
        if (i < quotes.length) {
            return new CharacterBuffer();
        }
        return parseString(allowQuote, quotes);
    }

    /**
     * Parsing a String
     *
     * @param allowQuote the allow quote
     * @param quotes     the quotes
     * @return the character buffer
     */
    public CharacterBuffer parseString(boolean allowQuote, char... quotes) {
        CharacterBuffer sc = new CharacterBuffer().with(getCurrentChar());
        if (quotes == null) {
            return sc;
        }
        char b = 0;
        char c;
        int i;
        boolean isQuote = false;

        int quoteLen = quotes.length;
        do {
            c = getChar();
            i = quoteLen;
            switch (c) {
                case 0:
                    return sc;
                case '\n':
                case '\r':
                default:
                    if (b == '\\') {
                        if (allowQuote) {
                            sc.with(c);
                            if (c == '\\') {
                                c = 1;
                            }
                            b = c;
                            c = 1;
                            continue;
                        } else if (c == '\\') {
                            c = 1;
                        }
                        isQuote = true;
                    }
                    for (i = 0; i < quoteLen; i++) {
                        if (c == quotes[i]) {
                            break;
                        }
                    }
                    if (i == quoteLen) {
                        sc.with(c);
                    }
                    b = c;
            }
            if (i < quoteLen) {
                c = 0;
            }
        } while (c != 0);
        if (isQuote) {
            sc.remove(sc.length() - 1);
        }
        return sc;
    }

    /**
     * Next value.
     *
     * @param stopChars Chars for stopping
     * @return nextValue
     */
    @Override
    public CharacterBuffer nextValue(char[] stopChars) {
        CharacterBuffer sb = new CharacterBuffer();
        char c = getCurrentChar();
        do {
            sb.with(c);
            c = getChar();
        } while (c >= ' ' && !StringUtil.containsAny(c, stopChars));
        return sb;
    }

    /**
     * Convert a Buffer to another Value.
     *
     * @param sb the sb
     * @return the object
     */
    public Object validateReturn(CharacterBuffer sb) {
        if (sb == null) {
            return null;
        }
        sb.trim();
        if (sb.length() < 1) {
            return null;
        }
        if (sb.equals(EMPTY)) {
            return sb;
        }
        if (sb.equalsIgnoreCase("true")) {
            return Boolean.TRUE;
        }
        if (sb.equalsIgnoreCase("false")) {
            return Boolean.FALSE;
        }
        if (sb.equalsIgnoreCase("null")) {
            return null;
        }
        /*
         * If it might be a number, try converting it. If a number cannot be produced, then the value
         * will just be a string. Note that the plus and implied string conventions are non-standard. A
         * JSON parser may accept non-JSON forms as long as it accepts all correct JSON forms.
         */

        char b = sb.charAt(0);
        if ((b >= '0' && b <= '9') || b == '.' || b == '-' || b == '+') {
            try {
                if (sb.indexOf('.') > -1 || sb.indexOf('e') > -1 || sb.indexOf('E') > -1) {
                    Double d = Double.valueOf(sb.toString());
                    if (!d.isInfinite() && !d.isNaN()) {
                        return d;
                    }
                } else {
                    Long myLong = Long.valueOf(sb.toString());
                    if (myLong.longValue() == myLong.intValue()) {
                        return Integer.valueOf(myLong.intValue());
                    }
                    return myLong;
                }
            } catch (Exception ignore) {
                /* Do nothing */
            }
        }
        return sb.toString();
    }

    /**
     * Skip to.
     *
     * @param search    the search
     * @param notEscape the not escape
     * @return true, if successful
     */
    @Override
    public boolean skipTo(char search, boolean notEscape) {
        int len = length();
        char lastChar = 0;
        if (position() > 0 && position() < len) {
            lastChar = getCurrentChar();
        }
        while (position() < len) {
            char currentChar = getCurrentChar();
            if (currentChar == search && (!notEscape || lastChar != '\\')) {
                return true;
            }
            lastChar = currentChar;
            if (!skip()) {
                break;
            }
        }
        return false;
    }

    /**
     * Skip to.
     *
     * @param search    the search
     * @param order     the order
     * @param notEscape the not escape
     * @return true, if successful
     */
    @Override
    public boolean skipTo(String search, boolean order, boolean notEscape) {
        if (position() < 0) {
            return false;
        }
        char[] character = search.toCharArray();
        int z = 0;
        int strLen = character.length;
        int len = length();
        char lastChar = 0;
        if (position() > 0 && position() < len) {
            lastChar = getCurrentChar();
        }
        while (position() < len) {
            char currentChar = getCurrentChar();
            if (order) {
                if (currentChar == character[z]) {
                    z++;
                    if (z >= strLen) {
                        return true;
                    }
                } else {
                    z = 0;
                }
            } else {
                for (char zeichen : character) {
                    if (currentChar == zeichen && (!notEscape || lastChar != '\\')) {
                        return true;
                    }
                }
            }
            lastChar = currentChar;
            skip();
        }
        return false;
    }

    /**
     * Skip.
     *
     * @param pos the pos
     * @return true, if successful
     */
    @Override
    public boolean skip(int pos) {
        while (pos > 0) {
            if (getChar() == 0) {
                return false;
            }
            pos--;
        }
        return true;
    }

    /**
     * Skip.
     *
     * @return true, if successful
     */
    @Override
    public boolean skip() {
        return getChar() != 0;
    }

    /**
     * Next token.
     *
     * @param stopWords the stop words
     * @return the character buffer
     */
    @Override
    public CharacterBuffer nextToken(char... stopWords) {
        char c = nextClean();
        for (int i = 0; i < stopWords.length; i++) {
            if (stopWords[i] == c) {
                return new CharacterBuffer();
            }
        }
        return parseString(true, stopWords);
    }

    /**
     * Check values.
     *
     * @param items the items
     * @return true, if successful
     */
    @Override
    public boolean checkValues(char... items) {
        char current = getCurrentChar();
        for (char item : items) {
            if (current == item) {
                return true;
            }
        }
        return false;
    }

    /**
     * Gets the string list.
     *
     * @return the string list
     */
    @Override
    public SimpleList<String> getStringList() {
        SimpleList<String> list = new SimpleList<String>();
        CharacterBuffer sc;
        do {
            sc = nextString(true, '"');
            skip();
            if (sc.length() > 0) {
                if (sc.indexOf('\"') >= 0) {
                    list.add("\"" + sc + "\"");
                } else {
                    list.add(sc.toString());
                }
            }
        } while (sc.length() > 0);
        return list;
    }

    /**
     * Skip char.
     *
     * @param quotes the quotes
     * @return the char
     */
    @Override
    public char skipChar(char... quotes) {
        char c = getCurrentChar();
        if (quotes == null) {
            return c;
        }
        boolean found;
        do {
            found = false;
            for (int i = 0; i < quotes.length; i++) {
                if (quotes[i] == c) {
                    found = true;
                    break;
                }
            }
            c = getChar();
            if (found) {
                break;
            }
        } while (c != 0);
        return c;
    }

    /**
     * Skip if.
     *
     * @param allowSpace the allow space
     * @param quotes     the quotes
     * @return true, if successful
     */
    public boolean skipIf(boolean allowSpace, char... quotes) {
        char c = getCurrentChar();
        if (quotes == null) {
            return true;
        }
        for (int i = 0; i < quotes.length; i++) {
            if (quotes[i] != c) {
                if (allowSpace && c == ' ') {
                    c = getChar();
                    i--;
                    continue;
                }
                return false;
            }
            c = getChar();
        }
        return true;
    }

    /**
     * Skip for.
     *
     * @param quotes the quotes
     * @return true, if successful
     */
    public boolean skipFor(char... quotes) {
        char c = getChar();
        if (quotes == null) {
            return true;
        }
        boolean found;
        do {
            found = false;
            for (int i = 0; i < quotes.length; i++) {
                if (quotes[i] == c) {
                    found = true;
                    break;
        }
      }
      if (found) {
        c = getChar();
      }
    } while (found);
    return true;
  }

  /**
   * Prints the error.
   *
   * @param msg the msg
   */
  public void printError(String msg) {
    if (msg != null && msg.length() > 0) {
      System.err.println(msg);
    }
  }
}
