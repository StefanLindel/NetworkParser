package de.uniks.networkparser.buffer;

import de.uniks.networkparser.StringUtil;
import de.uniks.networkparser.interfaces.BaseItem;

/*
 * NetworkParser The MIT License Copyright (c) 2010-2016 Stefan Lindel
 * https://www.github.com/fujaba/NetworkParser/
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

/**
 * Buffered Buffer.
 *
 * @author Stefan Lindel
 */
public abstract class BufferedBuffer extends Buffer implements BaseItem {
    /**
     * The count is the number of characters used.
     */
    protected int length;

    /**
     * The start is the number of characters started.
     */
    int start;

    /**
     * Back.
     *
     * @return true, if successful
     */
    public boolean back() {
        if (position > 0) {
            position--;
            return true;
        }
        return false;
    }

    /**
     * With position.
     *
     * @param value the value
     * @return the buffered buffer
     */
    public BufferedBuffer withPosition(int value) {
        position = value;
        return this;
    }

    /**
     * The Length.
     *
     * @return the int
     */
    @Override
    public int length() {
        return length;
    }

    /**
     * Checks if is end.
     *
     * @return true, if is end
     */
    @Override
    public boolean isEnd() {
        if (position() - start + 1 < 0) {
            return true;
        }
        return position() - start + 1 >= length();
    }

    /**
     * Checks if is end character.
     *
     * @return true, if is end character
     */
    public boolean isEndCharacter() {
        if (position() - start + 1 >= length()) {
            return true;
        }
        return nextClean() == 0;
    }

    /**
     * With length.
     *
     * @param value the value
     * @return the buffered buffer
     */
    public BufferedBuffer withLength(int value) {
        length = value;
        return this;
    }

    /**
     * With Start Position.
     *
     * @param value the value
     * @return the buffered buffer
     */
    public BufferedBuffer withStart(int value) {
        start = value;
        return this;
    }

    /**
     * Byte at.
     *
     * @param index the index
     * @return the byte
     */
    public abstract byte byteAt(int index);

    /**
     * Char at.
     *
     * @param index the index
     * @return the char
     */
    public abstract char charAt(int index);

    /**
     * Get the Current Character.
     *
     * @return The currentChar
     */
    public char getCurrentChar() {
        return charAt(position());
    }

    /**
     * Substring of Buffer.
     *
     * @param start startindex for parsing
     * @param endPosition the endPosition of Substring
     * @return the Substring
     */
    public abstract CharacterBuffer subSequence(int start, int endPosition);

    /**
     * With look A head.
     *
     * @param lookahead the lookahead
     * @return the buffered buffer
     */
    @Override
    public BufferedBuffer withLookAHead(CharSequence lookahead) {
        if (lookahead == null) {
            return this;
        }
        withPosition(position() - lookahead.length() + 1);
        return this;
    }

    /**
     * With look A head.
     *
     * @param lookahead the lookahead
     * @return the buffered buffer
     */
    @Override
    public BufferedBuffer withLookAHead(char lookahead) {
        if (lookahead < 0) {
            return this;
        }
        do {
            withPosition(position() - 1);
        } while (getCurrentChar() != lookahead && getCurrentChar() <= ' ' && position() > 1);
        return this;
    }

    private void next(boolean isValueAllow, char... quotes) {
        char c = getCurrentChar();
        char b = 0;
        int i;
        do {
            switch (c) {
                case 0:
                    c = 0;
                    break;
                case '\n':
                case '\r':
                    break;
                case '\\':
                    if (b == '\\') {
                        b = 0;
                        c = getChar();
                        continue;
                    }
                    break;
                case '"':
                    if (b == '\\' && isValueAllow) {
                        b = c;
                        c = getChar();
                        continue;
                    }
                    break;
                default:
            }
            for (i = 0; i < quotes.length; i++) {
                if (c == quotes[i]) {
                    return;
                }
            }
            b = c;
            c = getChar();

        } while (c != 0);
    }

    @Override
    public CharacterBuffer parseString(boolean value, char... quotes) {
        int startpos = position();
        next(value, quotes);
        int endPos = position();
        return new CharacterBuffer().with(subSequence(startpos, endPos));
    }

    /**
     * Get the next n characters.
     *
     * @param n The number of characters to take.
     * @return A string of n characters. Substring bounds error if there are not n characters
     * remaining in the source string.
     */
    public String getNextString(int n) {
        int pos = 0;
        if (n < -1) {
            n = n * -1;
            char[] chars = new char[n];
            while (pos < n) {
                chars[pos] = charAt(position() - (n - pos++));
            }
            return new String(chars);
        } else if (n == -1) {
            n = length() - position();
        } else if (n == 0) {
            return EMPTY;
        } else if (position() + n > length()) {
            n = length() - position();
        }
        char[] chars = new char[n];

        while (pos < n) {
            chars[pos] = charAt(position() + pos++);
        }
        return new String(chars);
    }

    @Override
    public CharacterBuffer nextValue(char[] stopChars) {
        if (StringUtil.containsAny(getCurrentChar(), stopChars)) {
            return null;
        }
        int s = position();
        char c;
        do {
            c = getChar();
        } while (c >= ' ' && !StringUtil.containsAny(c, stopChars));
        return subSequence(s, position()).trim();
    }

    /**
     * Get the next n characters.
     *
     * @param count The number of characters to take.
     * @return A string of n characters. Substring bounds error if there are not n characters
     * remaining in the source string.
     */
    public String nextString(int count) {
        int pos = 0;
        if (count < -1) {
            count = count * -1;
            char[] chars = new char[count];
            while (pos < count) {
                chars[pos] = charAt(position() - (count - pos++));
            }
            return new String(chars);
        } else if (count == -1) {
            count = length() - position();
        } else if (count == 0) {
            return EMPTY;
        } else if (position() + count > length()) {
            count = length() - position();
        }
        char[] chars = new char[count];

        while (pos < count) {
            chars[pos] = charAt(position() + pos++);
        }
        return new String(chars);
    }

    /**
     * Substring.
     *
     * @param positions first is start Position, second is Endposition
     *                  <p>
     *                  Absolut fix Start and End start&gt;0 StartPosition end&gt;Start EndPosition
     *                  <p>
     *                  Absolut from fix Position Start&gt;0 Position end NULL To End end -1 To index
     *                  <p>
     *                  Relativ from indexPosition Start Position from index + (-Start) End = 0 current Position
     * @return substring from buffer
     */
    public String substring(int... positions) {
        if (positions == null || positions.length < 1) {
            positions = new int[]{-1};
        }
        int start = positions[0], end = -1;
        if (positions.length < 2) {
            /* END IS END OF BUFFER (Exclude) */
            end = length();
            if (start == -1) {
                start = position();
            }
        } else {
            end = positions[1];
        }
        if (end == -1) {
            end = position();
        } else if (end == 0) {
            if (start < 0) {
                end = position();
                start = position() + start;
            } else {
                end = position() + start;
                if (position() + end > length()) {
                    end = length();
                }
                start = position();
            }
        }
        if (start < 0 || end <= 0 || start > end) {
            return EMPTY;
        }
        return subSequence(start, end).toString();
    }

    /**
     * Next.
     *
     * @param positions the positions
     * @return the string
     */
    public String next(int... positions) {
        if (positions == null || positions.length != 1) {
            return substring(positions);
        }
        return subSequence(position(), position() + positions[0]).toString();
    }

    /**
     * To bytes.
     *
     * @param all the all
     * @return the byte[]
     */
    public byte[] toBytes(boolean... all) {
        byte[] result;
        int i = start;
        if (all != null && all.length > 0 && all[0]) {
            result = new byte[length];
        } else {
            if (length - position < 0) {
                return null;
            }
            result = new byte[length - position];
            i = position;
        }

        for (; i < result.length; i++) {
            result[i] = byteAt(i);
        }
        return result;
    }

    /**
     * Format Buffer to String
     *
     * @return The Buffer as String
     */
    public String asString() {
        return new String(toBytes(true));
    }

    /**
     * To array string.
     *
     * @param addString the add string
     * @return the string
     */
    public String toArrayString(boolean... addString) {
        CharacterBuffer sb = new CharacterBuffer();
        sb.with('[');

        byte[] byteArray = toBytes();
        if (byteArray != null && byteArray.length > 0) {
            sb.with("" + byteArray[0]);
            for (int i = 1; i < length; i++) {
                sb.with("," + byteArray[i]);
            }
        }
        sb.with(']');
        if (addString != null && addString.length > 0 && addString[0]) {
            sb.with(' ', '(').with(new String(byteArray)).with(')');
        }
        return sb.toString();
    }

    /**
     * Clear.
     */
    public final void clear() {
        length = 0;
        start = 0;
        position = 0;
    }

    /**
     * add char[] to Buffer.
     *
     * @param buffer the buffer
     * @param start the startposition
     * @param length the length
     * @return the buffered buffer
     */
    public abstract BufferedBuffer with(char[] buffer, int start, int length);

    /**
     * add byte[] to Buffer.
     *
     * @param buffer the buffer
     * @param start the startposition
     * @param length the length
     * @return the buffered buffer
     */
    public abstract BufferedBuffer with(byte[] buffer, int start, int length);

    /**
     * With.
     *
     * @param items the items
     * @return the buffered buffer
     */
    public abstract BufferedBuffer with(CharSequence... items);

    /**
     * Gets the new list.
     *
     * @param list the list
     * @return the new list
     */
    public abstract BufferedBuffer getNewList(boolean list);

    /**
     * Find a line.
     *
     * @param element the element
     * @return the buffered buffer
     */
    public abstract BufferedBuffer findLine(String element);

    /**
     * Index of String.
     *
     * @param value the value
     * @return the int
     */
    public int indexOf(CharSequence value) {
        return indexOf(value, 0);
    }

    /**
     * Last index of.
     *
     * @param ch the ch
     * @return the int
     */
    public int lastIndexOf(char ch) {
        return lastIndexOf(ch, length);
    }

    /**
     * Read the next line.
     *
     * @param pos the pos
     * @return the buffered buffer
     */
    public abstract BufferedBuffer nextLine(int pos);

    /**
     * Last index of Element.
     *
     * @param character the character
     * @param offset    the offset
     * @return the Position
     */
    public abstract int lastIndexOf(char character, int offset);

    /**
     * Index of Element.
     *
     * @param str       the str
     * @param fromIndex the from index
     * @return the Position
     */
    public abstract int indexOf(CharSequence str, int fromIndex);

    /**
     * Replace a String.
     *
     * @param start the start
     * @param end the end
   * @param replace the replace
   * @return true, if successful
   */
  public abstract boolean replace(int start, int end, String replace);

  /**
   * Gets the start position.
   * 
   * @return the start position
   */
  public int getStartPosition() {
    return start;
  }
}
