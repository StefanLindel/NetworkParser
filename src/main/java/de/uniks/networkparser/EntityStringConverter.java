package de.uniks.networkparser;

import de.uniks.networkparser.buffer.CharacterBuffer;
import de.uniks.networkparser.interfaces.BaseItem;
import de.uniks.networkparser.interfaces.Converter;
import de.uniks.networkparser.interfaces.Entity;
import de.uniks.networkparser.list.SimpleKeyValueList;

/**
 * The Class EntityStringConverter.
 *
 * @author Stefan
 */
public class EntityStringConverter implements Converter {
    /**
     * The Constant EMPTY.
     */
    public static final String EMPTY = "";
    private int indentFactor;
    private int indent;
    private String value;

    /**
     * Instantiates a new entity string converter.
     */
    public EntityStringConverter() {
    }

    /**
     * Instantiates a new entity string converter.
     *
     * @param indentFactor the indent factor
     * @param indent       the indent
     */
    public EntityStringConverter(int indentFactor, int indent) {
        this.indentFactor = indentFactor;
        this.indent = indent;
    }

    /**
     * Instantiates a new entity string converter.
     *
     * @param indentFactor the indent factor
     */
    public EntityStringConverter(int indentFactor) {
        this.indentFactor = indentFactor;
    }

    /**
     * With Delimiter Structure.
     *
     * @param value the Delimiter
     * @return the entity string converter
     */
    public EntityStringConverter withDelimeter(String value) {
        this.value = value;
        return this;
    }

    /**
     * Gets the Delimiter.
     *
     * @return the Delimiter
     */
    public String getDelimiter() {
        return value;
    }

    /**
     * Encode.
     *
     * @param entity the entity
     * @return the string
     */
    @Override
    public String encode(BaseItem entity) {
        if (entity instanceof Entity) {
            return ((Entity) entity).toString(getIndentFactor());
        }
        if (entity instanceof SimpleKeyValueList<?, ?>) {
            SimpleKeyValueList<?, ?> collection = (SimpleKeyValueList<?, ?>) entity;
            CharacterBuffer result = new CharacterBuffer();
            for (int i = 0; i < collection.size(); i++) {
                Object key = collection.getKeyByIndex(i);
                Object value = collection.getValueByIndex(i);
                result.withLine("" + key + value + value);
            }
            return result.toString();
        }
        if (entity != null) {
            return entity.toString(this);
        }
        return null;
    }

    /**
     * Gets the indent factor.
     *
     * @return the indent factor
     */
    public int getIndentFactor() {
        return indentFactor;
    }

    /**
     * Gets the indent.
     *
     * @return the indent
     */
    public int getIndent() {
        return indent;
    }

    /**
     * Gets the step.
     *
     * @return the step
     */
    public String getStep() {
        char[] buf = new char[indentFactor];
        for (int i = 0; i < indentFactor; i++) {
            buf[i] = SimpleMap.SPACE;
        }
        return new String(buf);
    }

    /**
     * Gets the prefix first.
     *
     * @return the prefix first
     */
    public String getPrefixFirst() {
        if (indent < 1) {
            return EMPTY;
        }
        char[] buf = new char[indent + 2];
        buf[0] = '\r';
        buf[1] = '\n';
        for (int i = 0; i < indent; i++) {
            buf[i + 2] = SimpleMap.SPACE;
        }
        return new String(buf);
    }

    /**
     * Gets the prefix.
     *
     * @return the prefix
     */
    public String getPrefix() {
        if (indent + indentFactor == 0) {
            return EMPTY;
        }
        char[] buf = new char[indent + 2];
        buf[0] = '\r';
        buf[1] = '\n';
        for (int i = 0; i < indent; i++) {
            buf[i + 2] = SimpleMap.SPACE;
        }
        return new String(buf);
    }

    /**
     * Adds the.
     */
    public void add() {
        indent = indent + indentFactor;
    }

  /**
   * Minus.
   */
  public void minus() {
    indent = indent - indentFactor;
  }
}
