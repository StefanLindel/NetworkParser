package de.uniks.networkparser.json;

import java.util.Map;

import de.uniks.networkparser.EntityStringConverter;
import de.uniks.networkparser.StringUtil;
import de.uniks.networkparser.Tokener;
import de.uniks.networkparser.buffer.Buffer;
import de.uniks.networkparser.buffer.CharacterBuffer;
import de.uniks.networkparser.interfaces.BaseItem;
import de.uniks.networkparser.interfaces.BufferItem;
import de.uniks.networkparser.interfaces.Entity;
import de.uniks.networkparser.list.AbstractList;
import de.uniks.networkparser.list.SimpleKeyValueList;

/**
 * A JsonObject is an unordered collection of name/value pairs. Its external form is a string
 * wrapped in curly braces with colons between the names and values, and commas between the values
 * and names.
 * 
 * @author Stefan Lindel
 */

public class JsonObject extends SimpleKeyValueList<String, Object> implements Entity {
  /** The Constant START. */
  public static final char START = '{';
  /** The Constant END. */
  public static final char END = '}';

  /** Instantiates a new json object. */

  public JsonObject() {
    this.withAllowDuplicate(false);
  }

  /**
   * Create a new JsonObject
   *
   * @param value the value
   * @return the JsonObject
   */
  public static JsonObject create(String value) {
    return new JsonObject().withValue(value);
  }

  /**
   * Get the JsonArray value associated with a key.
   *
   * @param key A key string.
   * @return A JsonArray which is the value. if the key is not found or if the value is not a
   *         JsonArray.
   */
  public JsonArray getJsonArray(String key) {
    Object object = this.get(key);
    if (object instanceof JsonArray) {
      return (JsonArray) object;
    }
    JsonArray returnValue = new JsonArray();
    if (object != null) {
      if (object instanceof String) {
        String item = (String) object;
        if (item.length() > 0) {
          char startChar = item.charAt(0);
          if (startChar == JsonArray.START || startChar == JsonObject.START) {
            returnValue.withValue(item);
            return returnValue;
          }
        }
      }
      returnValue.add(object);
      return returnValue;
    }
    int pos = key.indexOf(".");
    if (pos > -1) {
      object = this.get(key.substring(0, pos));
      if (object instanceof JsonObject) {
        return ((JsonObject) object).getJsonArray(key.substring(pos + 1));
      }
    }
    return returnValue;
  }

  /**
   * Get the JsonObject value associated with a key.
   *
   * @param key A key string.
   * @return A JsonObject which is the value.
   */
  public JsonObject getJsonObject(String key) {
    Object object = this.get(key);
    if (object instanceof JsonObject) {
      return (JsonObject) object;
    }
    if (object instanceof String) {
      return new JsonObject().withValue("" + object);
    }
    int pos = key.indexOf(".");
    if (pos > -1) {
      object = this.get(key.substring(0, pos));
      if (object instanceof JsonObject) {
        return ((JsonObject) object).getJsonObject(key.substring(pos + 1));
      }
    }
    return null;
  }

  /**
   * Make a JSON text of this JsonObject. For compactness, no whitespace is added. If this would not
   * result in a syntactically correct JSON text, then null will be returned instead.
   * <p>
   * Warning: This method assumes that the data structure is acyclical.
   *
   * @return a printable, displayable, portable, transmittable representation of the object,
   *         beginning with <code>{</code>&nbsp;<small>(left brace)</small> and ending with
   *         <code>}</code>&nbsp;<small>(right brace)</small>.
   */
  @Override
  public String toString() {
    return parseItem(new EntityStringConverter());
  }

  /**
   * Make a prettyprinted JSON text of this JsonObject.
   * <p>
   * Warning: This method assumes that the data structure is acyclical.
   *
   * @param indentFactor The number of spaces to add to each level of indentation.
   * @return a printable, displayable, portable, transmittable representation of the object,
   *         beginning with <code>{</code>&nbsp;<small>(left brace)</small> and ending with
   *         <code>}</code>&nbsp;<small>(right brace)</small>.
   */
  @Override
  public String toString(int indentFactor) {
    return parseItem(new EntityStringConverter(indentFactor));
  }

  protected String parseItem(EntityStringConverter converter) {
    int length = this.size();
    if (length == 0 || converter == null) {
      return "{}";
    }

    if (!isVisible()) {
      return "{" + size() + " values}";
    }
    converter.add();
    StringBuilder sb = new StringBuilder();
    sb.append(START);
    if (length > 1) {
      sb.append(converter.getPrefix());
    }
    sb.append(StringUtil.quote(get(0)));
    sb.append(":");
    sb.append(StringUtil.valueToString(getValueByIndex(0), false, this, converter));
    for (int i = 1; i < length; i++) {
      sb.append(",");
      sb.append(converter.getPrefix());
      sb.append(StringUtil.quote(get(i)));
      sb.append(":");
      sb.append(StringUtil.valueToString(getValueByIndex(i), false, this, converter));
    }
    converter.minus();
    if (length > 1) {
      sb.append(converter.getPrefix());
    }
    sb.append(END);
    return sb.toString();
  }

  /**
   * Set the value to Tokener or pairs of values.
   *
   * @param values a simple String of Value or pairs of key-values
   * @return Itself
   */
  public JsonObject withValue(String... values) {
    if (values == null || values.length == 1 && values[0] == null) {
      return this;
    }
    if (values.length % 2 == 0) {
      for (int z = 0; z < values.length; z += 2) {
        if (values[z + 1] != null) {
          /* Only add value != null */
          put(values[z], values[z + 1]);
        }
      }
      return this;
    }
    CharacterBuffer buffer = new CharacterBuffer().with(values[0]);
    Tokener tokener = new JsonTokener();
    tokener.parseToEntity(this, buffer);
    return this;
  }

  /**
   * Set the value to Tokener or pairs of values.
   *
   * @param values a simple String of Value or pairs of key-values
   * @return Itself
   */
  public JsonObject withValue(BufferItem values) {
    new JsonTokener().parsingEntity(this, (Buffer) values);
    return this;
  }

  /**
   * Tokener to init the JsonObject.
   *
   * @param entity entity to add values with the tokener
   * @return Itself
   */
  public JsonObject withEntity(Map<?, ?> entity) {
    new JsonTokener().parseToEntity(this, entity);
    return this;
  }

  /**
   * Get a new Instance of JsonArray, JsonObject.
   *
   * @param keyValue the key value
   * @return the new list
   */
  @Override
  public BaseItem getNewList(boolean keyValue) {
    if (keyValue) {
      return new JsonObject();
    }
    return new JsonArray();
  }

  /**
   * Checks for key, if JsonObject has the Key
   *
   * @param key the key
   * @return true, if successful
   */
  public boolean has(String key) {
    return containsKey(key);
  }

  /**
   * With key value.
   *
   * @param key the key
   * @param value the value
   * @return the json object
   */
  @Override
  public JsonObject withKeyValue(Object key, Object value) {
    super.withKeyValue(key, value);
    return this;
  }

  /**
   * Accumulate values under a key. It is similar to the put method except that if there is already
   * an object stored under the key then a EntityList is stored under the key to hold all of the
   * accumulated values. If there is already a EntityList, then the new value is appended to it. In
   * contrast, the put method replaces the previous value.
   * <p>
   * If only one value is accumulated that is not a EntityList, then the result will be the same as
   * using put. But if multiple values are accumulated, then the result will be like append.
   *
   * @param key A key string.
   * @param value An object to be accumulated under the key.
   * @return this.
   */
  public JsonObject addToList(String key, Object value) {
    Object object = this.get(key);
    if (object == null) {
      if (value instanceof AbstractList) {
        BaseItem newList = getNewList(true);
        newList.add(value);
        this.put(key, newList);
      } else {
        this.put(key, value);
      }
    } else if (object instanceof AbstractList) {
      ((AbstractList<?>) object).with(value);
    } else {
      BaseItem newList = getNewList(false);
      newList.add(object, value);
      this.put(key, newList);
    }
    return this;
  }

  /**
   * With key value.
   *
   * @param key the key
   * @param value the value
   * @return the json object
   */
  public JsonObject withKeyValue(String key, Object value) {
    if (value != null) {
      /* Only add value != null */
      int index = indexOf(key);
      if (index >= 0) {
        setValueItem(key, value);
        return this;
      }
      super.withKeyValue(key, value);
    }
    return this;
  }

  /**
   * Without.
   *
   * @param key the key
   * @return the entity
   */
  public Entity without(String key) {
    remove(key);
    return this;
  }

  /**
   * Creates the child.
   *
   * @param key the key
   * @return the json object
   */
  public JsonObject createChild(String key) {
    JsonObject child = new JsonObject();
    put(key, child);
    return child;
  }

  /**
   * Gets the element by.
   *
   * @param key the key
   * @param value the value
   * @return the element by
   */
  @Override
  public Entity getElementBy(String key, String value) {
    if (value == null || size() < 1) {
      return null;
    }
    Object item = get(value);
    JsonObject child;
    if (item instanceof JsonObject) {
      child = (JsonObject) item;
    } else {
      child = new JsonObject();
      this.put(value, child);
    }
    return child;
  }

  /**
   * With type.
   *
   * @param type the type
   * @return the json object
   */
  @Override
  public JsonObject withType(String type) {
    this.add(Entity.CLASS, type);
    return this;
  }

  /**
   * Adds the comment.
   *
   * @param comment the comment
   * @return the json object
   */
  public JsonObject addComment(String comment) {
    if (comment == null) {
      return this;
    }
    this.withAllowDuplicate(true);
    this.withAllowEmptyValue(true);
    this.add(null, comment);
    return this;
  }

  /**
   * Equals.
   *
   * @param obj the obj
   * @return true, if successful
   */
  @Override
  public boolean equals(Object obj) {
    if (obj instanceof JsonObject) {
      JsonObject other = (JsonObject) obj;
      if (this.size() != other.size()) {
        return false;
      }
      for (int i = 0; i < this.size(); i++) {
        if (!this.getKeyByIndex(i).equals(other.getKeyByIndex(i))) {
          return false;
        }
        Object value = this.getValueByIndex(i);
        Object otherValue = other.getValueByIndex(i);
        if (value == null && otherValue != null) {
          return false;
        } else if (value != null && !value.equals(otherValue)) {
          return false;
        }
      }
      return true;
    }
    return super.equals(obj);
  }

}
