package de.uniks.networkparser.gui;

/*
 * NetworkParser The MIT License Copyright (c) 2010-2016 Stefan Lindel
 * https://www.github.com/fujaba/NetworkParser/
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

/**
 * The Enum FieldTyp.
 *
 * @author Stefan
 */
public enum FieldTyp {
  /**
   * The assoc.
   */
  ASSOC(1),
  /**
   * The date.
   */
  DATE(2),
  /**
   * The checkbox.
   */
  CHECKBOX(3),
  /**
   * The combobox.
   */
  COMBOBOX(4),
  /**
   * The double.
   */
  DOUBLE(5),
  /**
   * The integer.
   */
  INTEGER(6),
  /**
   * The text.
   */
  TEXT(7),
  /**
   * The password.
   */
  PASSWORD(8),
  /**
   * The valuefromdropdownlist.
   */
  VALUEFROMDROPDOWNLIST(9);

  private int value;

  FieldTyp(int value) {
    this.setValue(value);
  }

  /**
   * Gets the value.
   *
   * @return the value
   */
  public int getValue() {
    return value;
  }

  /**
   * Sets the value.
   *
   * @param value the new value
   */
  public void setValue(int value) {
    this.value = value;
  }
}
