package de.uniks.networkparser.parser;

import de.uniks.networkparser.*;
import de.uniks.networkparser.buffer.Buffer;
import de.uniks.networkparser.buffer.CharacterBuffer;
import de.uniks.networkparser.interfaces.BaseItem;
import de.uniks.networkparser.interfaces.Entity;
import de.uniks.networkparser.interfaces.EntityList;
import de.uniks.networkparser.interfaces.SendableEntityCreator;
import de.uniks.networkparser.list.SimpleKeyValueList;
import de.uniks.networkparser.list.SimpleList;
import de.uniks.networkparser.xml.XMLEntity;
import de.uniks.networkparser.xml.XMLTokener;

/**
 * The Class ExcelParser.
 *
 * @author Stefan
 */
public class ExcelParser {
    /**
     * The Constant ROW.
     */
    public static final String ROW = "row";
    /**
     * The Constant CELL.
     */
    public static final String CELL = "c";
    /**
     * The Constant CELL_TYPE.
     */
    public static final String CELL_TYPE = "t";
    /**
     * The Constant REF.
     */
    public static final String REF = "ref";
    /**
     * The Constant CELL_TYPE_REFERENCE.
     */
    public static final String CELL_TYPE_REFERENCE = "s";
    /**
     * The Constant SEMICOLON.
     */
    public static final char SEMICOLON = ';';
    private final static String HEADER =
            "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\r\n";
    private final static String APP =
            "<Properties xmlns=\"http://schemas.openxmlformats.org/officeDocument/2006/extended-properties\" xmlns:vt=\"http://schemas.openxmlformats.org/officeDocument/2006/docPropsVTypes\"><TotalTime>0</TotalTime><Application>NetworkParser</Application><DocSecurity>0</DocSecurity><ScaleCrop>false</ScaleCrop><AppVersion>1.42</AppVersion></Properties>";
    private final static String RELS =
            "<Relationships xmlns=\"http://schemas.openxmlformats.org/package/2006/relationships\"><Relationship Id=\"rId1\" Type=\"http://schemas.openxmlformats.org/officeDocument/2006/relationships/officeDocument\" Target=\"xl/workbook.xml\"/><Relationship Id=\"rId2\" Type=\"http://schemas.openxmlformats.org/package/2006/relationships/metadata/core-properties\" Target=\"docProps/core.xml\"/><Relationship Id=\"rId3\" Type=\"http://schemas.openxmlformats.org/officeDocument/2006/relationships/extended-properties\" Target=\"docProps/app.xml\"/></Relationships>";

    /**
     * Parses the sheets.
     *
     * @param stringFile the string file
     * @param sheetFile  the sheet file
     * @return the excel work book
     */
    public ExcelWorkBook parseSheets(CharSequence stringFile, CharSequence... sheetFile) {
        ExcelWorkBook excelWorkBook = new ExcelWorkBook();
        if (sheetFile == null) {
            return excelWorkBook;
        }
        for (CharSequence sheet : sheetFile) {
            excelWorkBook.add(parseSheet(stringFile, sheet));
        }
        return excelWorkBook;
    }

    /**
     * Parses the sheet.
     *
     * @param stringFile the string file
     * @param sheetFile  the sheet file
     * @return the excel sheet
     */
    public ExcelSheet parseSheet(CharSequence stringFile, CharSequence sheetFile) {
        ExcelSheet data = new ExcelSheet();
        SimpleKeyValueList<String, ExcelCell> cells = new SimpleKeyValueList<String, ExcelCell>();
        SimpleKeyValueList<String, String> mergeCellPos = new SimpleKeyValueList<String, String>();

        IdMap map = new IdMap();
        map.add(new ExcelCell());
        XMLTokener tokener = new XMLTokener().withMap(map);
        tokener.withDefaultFactory(EntityCreator.createXML());
        CharacterBuffer buffer = null;
        if (sheetFile instanceof CharacterBuffer) {
            buffer = (CharacterBuffer) sheetFile;
        } else {
            buffer = new CharacterBuffer().with(sheetFile.toString());
        }

        XMLEntity sheet = (XMLEntity) map.decode(tokener, buffer, map.getFilter());
        XMLEntity sharedStrings = null;
        if (stringFile != null) {
            sharedStrings = (XMLEntity) map.decode(stringFile.toString());
        }

        if (sheet != null) {
            EntityList mergeCells = sheet.getElementsBy(XMLEntity.PROPERTY_TAG, "mergeCells", false);
            /* <mergeCells count="1"><mergeCell ref="A2:A3"/></mergeCells> */
            if (mergeCells != null) {
                for (int i = 0; i < mergeCells.sizeChildren(); i++) {
                    BaseItem mergeCell = mergeCells.getChild(i);
                    if (mergeCell == null) {
                        continue;
                    }
                    SimpleList<Pos> excelRange =
                            EntityUtil.getExcelRange(((Entity) mergeCell).getString(REF));
                    for (Pos item : excelRange) {
                        if (item == null || item.x < 0 || item.y < 0) {
                            continue;
                        }
                        mergeCellPos.add(item.toString(), excelRange.first().toString());
                    }
                }
            }
            EntityList sheetDatas = sheet.getElementsBy(XMLEntity.PROPERTY_TAG, "sheetData", false);
            if (sheetDatas != null) {
                for (int s = 0; s < sheetDatas.sizeChildren(); s++) {
                    EntityList sheetData = (EntityList) sheetDatas.getChild(s);
                    for (int i = 0; i < sheetData.sizeChildren(); i++) {
                        BaseItem child = sheetData.getChild(i);
                        if (!(child instanceof XMLEntity)) {
                            continue;
                        }
                        XMLEntity row = (XMLEntity) child;
                        if (!ROW.equalsIgnoreCase(row.getTag())) {
                            continue;
                        }
                        ExcelRow dataRow = new ExcelRow();
                        /* <c r="A1" t="s"><v>2</v></c> */
                        for (int c = 0; c < row.size(); c++) {
                            BaseItem item = row.getChild(c);
                            if (!(item instanceof ExcelCell)) {
                                continue;
                            }
                            ExcelCell cell = (ExcelCell) item;
                            if (!CELL.equalsIgnoreCase(cell.getTag())) {
                                continue;
                            }
                            if (CELL_TYPE_REFERENCE.equalsIgnoreCase(cell.getType())) {
                                /* <v>2</v> */
                                EntityList element = cell.getChild(0);
                                if (element != null) {
                                    String ref = ((XMLEntity) element).getValue();
                                    if (sharedStrings != null) {
                                        XMLEntity refString = (XMLEntity) sharedStrings.getChild(Integer.parseInt(ref));
                                        String text = ((XMLEntity) refString.getChild(0)).getValue();
                                        cell.setContent(text);
                                    }
                                }
                            } else if (cell.sizeChildren() < 1) {
                                String pos = mergeCellPos.get(cell.getReferenz().toString());
                                if (pos != null && cells.contains(pos)) {
                                    ExcelCell firstCell = cells.get(pos);
                                    cell.setReferenceCell(firstCell);
                                }
                            }
                            cells.add(cell.getReferenz().toString(), cell);
                            dataRow.add(cell);
                        }
                        if (dataRow.size() > 0) {
                            data.add(dataRow);
                        }
                    }
                }
            }
        }
        return data;
    }

    /**
     * Write CSV.
     *
     * @param data the data
     * @return the character buffer
     */
    public CharacterBuffer writeCSV(SimpleList<SimpleList<ExcelCell>> data) {
        CharacterBuffer result = new CharacterBuffer();
        if (data == null) {
            return result;
        }
        for (SimpleList<ExcelCell> row : data) {
            boolean first = true;
            for (ExcelCell cell : row) {
                if (!first) {
                    result.with(SEMICOLON);
                }
                result.with(cell.getContentAsString());
                first = false;
            }
            result.with(BaseItem.CRLF);
        }
        return result;
    }

    /**
     * Read CSV.
     *
     * @param data    the data
     * @param creator the creator
     * @return the simple list
     */
    public SimpleList<Object> readCSV(Buffer data, SendableEntityCreator creator) {
        SimpleList<Object> result = new SimpleList<Object>();
        if (data == null || creator == null) {
            return result;
        }
        SimpleList<String> header = new SimpleList<String>();
        CharacterBuffer line = data.readLine();
        if (line == null || line.length() < 1) {
            return result;
        }
        int start = 0;
        for (int i = 0; i < line.length(); i++) {
            if (line.charAt(i) == SEMICOLON) {
                header.add(line.substring(start, i));
                start = i + 1;
            }
        }
        do {
            line = data.readLine();
            int column = 0;
            start = 0;
            /* Parsing data */
            Object item = creator.getSendableInstance(false);
            for (int i = 0; i < line.length(); i++) {
                if (line.charAt(i) == SEMICOLON) {
                    String value = line.substring(start, i);
                    creator.setValue(item, header.get(column), value, SendableEntityCreator.NEW);
                    column++;
                    if (column > header.size()) {
                        break;
                    }
                    start = i + 1;
                }
            }
            result.add(item);
            if (data.isEnd()) {
                break;
            }
        } while (line != null);
        return result;
    }

    /**
     * Creates the excel content.
     *
     * @param content the content
     * @return the simple key value list
     */
    public SimpleKeyValueList<String, String> createExcelContent(ExcelWorkBook content) {
        int id = 4;
        SimpleKeyValueList<String, String> fileContent = new SimpleKeyValueList<String, String>();
        fileContent.add("[Content_Types].xml", getContentTypes(content));
        fileContent.add("docProps/app.xml", HEADER + APP);
        fileContent.add("_rels/.rels", HEADER + RELS);

        fileContent.add("xl/_rels/workbook.xml.rels", getHeaderWorkbookRel(content, id));
        fileContent.add("xl/workbook.xml", getHeaderWorkbook(content, id));
        fileContent.add("docProps/core.xml", getHeader(content));
        for (int i = 0; i < content.size(); i++) {
            ExcelSheet sheet = content.get(i);
            if (sheet == null) {
                continue;
            }
            fileContent.add("xl/worksheets/sheet" + (i + 1) + ".xml", getSheet(sheet, id));
        }
        return fileContent;
    }

    private String getContentTypes(ExcelWorkBook content) {
        CharacterBuffer data = new CharacterBuffer().with(HEADER);
        data.with(
                "<Types xmlns=\"http://schemas.openxmlformats.org/package/2006/content-types\"><Default Extension=\"rels\" ContentType=\"application/vnd.openxmlformats-package.relationships+xml\"/><Default Extension=\"xml\" ContentType=\"application/xml\"/><Override PartName=\"/xl/workbook.xml\" ContentType=\"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet.main+xml\"/><Override PartName=\"/docProps/app.xml\" ContentType=\"application/vnd.openxmlformats-officedocument.extended-properties+xml\"/><Override PartName=\"/docProps/core.xml\" ContentType=\"application/vnd.openxmlformats-package.core-properties+xml\"/>");
        for (int i = 1; i <= content.size(); i++) {
            data.with("<Override PartName=\"/xl/worksheets/sheet" + i
                    + ".xml\" ContentType=\"application/vnd.openxmlformats-officedocument.spreadsheetml.worksheet+xml\"/>");
        }
        data.with("</Types>");
        return data.toString();
    }

    private String getHeaderWorkbook(ExcelWorkBook content, int id) {
        CharacterBuffer data = new CharacterBuffer().with(HEADER);
        data.with(
                "<workbook xmlns=\"http://schemas.openxmlformats.org/spreadsheetml/2006/main\" xmlns:r=\"http://schemas.openxmlformats.org/officeDocument/2006/relationships\" xmlns:mc=\"http://schemas.openxmlformats.org/markup-compatibility/2006\">\r\n    <sheets>\r\n");
        for (int i = 0; i < content.size(); i++) {
            ExcelSheet sheet = content.get(i);
            if (sheet == null) {
                continue;
            }
            data.with("        <sheet name=\"");
            if (sheet.getName() == null) {
                data.with("Table" + (i + 1));
            } else {
                data.with(sheet.getName());
            }
            data.with("\" sheetId=\"", "" + (i + 1), "\" r:id=\"rId", "" + (i + id), "\"/>\r\n");
        }
        data.with("    </sheets>\r\n</workbook>");

        return data.toString();
    }

    private String getSheet(ExcelSheet sheet, int id) {
        int rowPos;
        CharacterBuffer data = new CharacterBuffer().with(HEADER);
        data.with(
                "<worksheet xmlns=\"http://schemas.openxmlformats.org/spreadsheetml/2006/main\" xmlns:r=\"http://schemas.openxmlformats.org/officeDocument/2006/relationships\" xmlns:mc=\"http://schemas.openxmlformats.org/markup-compatibility/2006\">\r\n");
        data.with("<sheetData>\r\n");
        for (rowPos = 0; rowPos <= sheet.size(); rowPos++) {
            ExcelRow row = sheet.get(rowPos);
            if (row == null) {
                continue;
            }
            data.with("  <row r=\"" + row.getRowPos() + "\">");
            for (ExcelCell cell : row) {
                data.with(cell.toString());
            }
            data.with("</row>\r\n");
        }
        data.with("</sheetData>");
        data.with("</worksheet>");
        return data.toString();
    }

    private String getHeaderWorkbookRel(ExcelWorkBook content, int id) {
        CharacterBuffer data = new CharacterBuffer().with(HEADER);
        data.with(
                "<Relationships xmlns=\"http://schemas.openxmlformats.org/package/2006/relationships\">");
        for (int i = 0; i < content.size(); i++) {
            data.with("<Relationship Id=\"rId", "" + (id + i),
                    "\" Type=\"http://schemas.openxmlformats.org/officeDocument/2006/relationships/worksheet\" Target=\"worksheets/sheet",
                    "" + (i + 1), ".xml\"/>");
        }
        data.with("</Relationships>");
        return data.toString();
    }

    private String getHeader(ExcelWorkBook content) {
        if (content.getAuthor() == null) {
            content.withAuthor(System.getProperty("user.name"));
        }
        CharacterBuffer data = new CharacterBuffer().with(HEADER);
        DateTimeEntity date = new DateTimeEntity();
        String string = date.toString("yyyy-mm-dd'T'HZ:MM:SS'Z'");
        data.with(
                "<cp:coreProperties xmlns:cp=\"http://schemas.openxmlformats.org/package/2006/metadata/core-properties\" xmlns:dc=\"http://purl.org/dc/elements/1.1/\" xmlns:dcterms=\"http://purl.org/dc/terms/\" xmlns:dcmitype=\"http://purl.org/dc/dcmitype/\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">");
        data.with("<dc:creator>", content.getAuthor(), "</dc:creator>");
        data.with("<cp:lastModifiedBy>", content.getAuthor(), "</cp:lastModifiedBy>");
        data.with("<dcterms:created xsi:type=\"dcterms:W3CDTF\">", string, "</dcterms:created>");
        data.with("<dcterms:modified xsi:type=\"dcterms:W3CDTF\">", string, "</dcterms:modified>");
        data.with("</cp:coreProperties>");
        return data.toString();
    }

    /**
     * Parse WorkBook to Model.
     *
   * @return success
   */
  public boolean parseModel() {
    return false;
  }
}
