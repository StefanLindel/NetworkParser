package de.uniks.networkparser.parser;

import de.uniks.networkparser.buffer.CharacterBuffer;
import de.uniks.networkparser.graph.*;
import de.uniks.networkparser.list.EntityComparator;
import de.uniks.networkparser.list.SimpleKeyValueList;
import de.uniks.networkparser.list.SimpleList;
import de.uniks.networkparser.list.SimpleSet;
import de.uniks.networkparser.parser.differ.AddCondition;
import de.uniks.networkparser.parser.differ.AttributeChangeCondition;
import de.uniks.networkparser.parser.differ.MemberDiffer;
import de.uniks.networkparser.parser.differ.RemoveCondition;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.SortedSet;
import java.util.TreeSet;

/**
 * The Class GraphMatcher.
 *
 * @author Stefan
 */
public class GraphMatcher extends GraphEntity {
    private final SimpleList<Match> clazzMatches = new SimpleList<Match>();

    private final SimpleList<Match> attributeMatches = new SimpleList<Match>();

    private final SimpleList<Match> associationMatches = new SimpleList<Match>();

    private final SimpleList<Match> methodMatches = new SimpleList<Match>();
    private final SimpleKeyValueList<Match, Match> possibleLinks =
            new SimpleKeyValueList<Match, Match>();
    private final GraphSimpleSet diffs = new GraphSimpleSet();
    // private MemberDiffer clazzDiffer = new MemberDiffer(new ClazzAddCondition(),
    // new ClazzChangeCondition(), new ClazzRemoveCondition());
    private final MemberDiffer attributeDiffer =
            new MemberDiffer(new AddCondition(), new AttributeChangeCondition(), new RemoveCondition());
    private GraphModel oldModel;
    private GraphModel newModel;
    private GraphModel metaModel;

    /**
     * Instantiates a new graph matcher.
     */
    public GraphMatcher() {
    }

    /**
     * Instantiates a new graph matcher.
     *
     * @param oldModel the old model
     * @param newModel the new model
     */
    public GraphMatcher(GraphModel oldModel, GraphModel newModel) {
        this.oldModel = oldModel;
        this.newModel = newModel;
        createMatches();
    }

    /**
     * Instantiates a new graph matcher.
     *
     * @param oldModel  the old model
     * @param newModel  the new model
     * @param metaModel the meta model
     */
    public GraphMatcher(GraphModel oldModel, GraphModel newModel, GraphModel metaModel) {
        this.oldModel = oldModel;
        this.newModel = newModel;
        this.metaModel = metaModel;
        createMatches();
    }

    /**
     * Creates the matches.
     *
     * @return true, if successful
     */
    public boolean createMatches() {
        matchClazzes();

        ClazzSet newMatches = new ClazzSet();

        if (metaModel != null) {
            for (Clazz metaClazz : metaModel.getClazzes()) {
                for (Match clazzMatch : getClazzMatches()) {
                    Clazz clazz = (Clazz) clazzMatch.getMatch();
                    if (newMatches.contains(clazz)) {
                        continue;
                    }
                    if (matchClazzValues(metaClazz, clazz)) {
                        clazzMatch.withMetdaMatch(metaClazz);
                        newMatches.add(clazz);
                    }
                }
            }
        }
        return true;
    }
    // private MemberDiffer attributeDiffer = new MemberDiffer(new AddCondition());
    // private MemberDiffer associationDiffer = new MemberDiffer(new
    // AssociationAddCondition(), new AssociationChangeCondition(), new
    // AssociationRemoveCondition());
    // private MemberDiffer methodDiffer = new MemberDiffer(new AddCondition(), new
    // MethodChangeCondition(), new RemoveCondition());

    /**
     * Gets the diffs.
     *
     * @return the diffs
     */
    public GraphSimpleSet getDiffs() {
        // clazzDiffer.diff(matches, getClazzMatches());
        attributeDiffer.diff(this, getAttributeMatches());
        // associationDiffer.diff(matches, getAssociationMatches());
        // methodDiffer.diff(matches, getMethodMatches());
        return diffs;
    }

    private List<Match> getAttributeMatches() {
        return attributeMatches;
    }

    /**
     * Match clazzes.
     *
     * @return true, if successful
     */
    public boolean matchClazzes() {
        if (oldModel == null || newModel == null) {
            return oldModel == newModel;
        }
        ClazzSet oldClazzes = oldModel.getClazzes();
        ClazzSet newClazzes = newModel.getClazzes();

        prepareMatches(oldClazzes, false);
        prepareMatches(newClazzes, true);

        ClazzSet oldMatches = new ClazzSet();
        ClazzSet newMatches = new ClazzSet();

        for (Clazz oldClazz : oldClazzes) {
            for (Clazz newClazz : newClazzes) {
                if (newMatches.contains(newClazz)) {
                    continue;
                }
                if (Double.compare(GraphUtil.compareName(oldClazz.getName(), newClazz.getName()),
                        3) == -1) {
                    matchAttributes(oldClazz, newClazz);
                    matchAssociations(oldClazz, newClazz);
                    matchMethods(oldClazz, newClazz);
                    Match oldMatch = getClazzMatch(oldClazz);
                    Match newMatch = getClazzMatch(newClazz);
                    linkMemberMatches(oldMatch, newMatch);
                    oldMatches.add(oldClazz);
                    newMatches.add(newClazz);
                    break;
                }
            }
        }

        boolean matchEntries = true;

        SimpleKeyValueList<Double, SimpleList<Match>> potentMatches =
                preparePotentMatches(oldClazzes, newClazzes, oldMatches, newMatches);

        for (Entry<Double, SimpleList<Match>> potentEntry : potentMatches.entrySet()) {
            List<Match> potents = potentEntry.getValue();
            for (Match potentMatch : potents) {
                Clazz oldClazz = (Clazz) potentMatch.getMatch();
                if (oldMatches.contains(oldClazz)) {
                    continue;
                }

                matchEntries = true;

                Clazz newClazz = (Clazz) potentMatch.getPotentMatch();
                if (newMatches.contains(newClazz)) {
                    continue;
                }

                if (oldClazz.getAttributes().size() != newClazz.getAttributes().size()) {
                    continue;
                }
                if (oldClazz.getAssociations().size() != newClazz.getAssociations().size()) {
                    continue;
                }
                if (oldClazz.getMethods().size() != newClazz.getMethods().size()) {
                    continue;
                }

                if (!matchAttributes(oldClazz, newClazz)) {
                    matchEntries = false;
                }
                if (matchEntries && !matchAssociations(oldClazz, newClazz)) {
                    matchEntries = false;
                }
                if (matchEntries && !matchMethods(oldClazz, newClazz)) {
                    matchEntries = false;
                }

                if (matchEntries) {
                    linkMemberMatches(getClazzMatch(oldClazz), getClazzMatch(newClazz));
                    oldMatches.add(oldClazz);
                    newMatches.add(newClazz);
                } else {
                    cleanLinks(oldClazz);
                }
            }
        }

        for (Entry<Match, Match> link : getPossibleLinks().entrySet()) {
            linkMemberMatches(link.getKey(), link.getValue());
        }
        return matchEntries;
    }

    private void prepareMatches(ClazzSet clazzes, boolean isFileMatch) {
        for (Clazz clazz : clazzes) {
            Match match = Match.createMatch(this, clazz, isFileMatch);
            match.withOwner(this);
            addClazzMatch(match);
            for (Attribute attribute : clazz.getAttributes()) {
                Match attributeMatch = Match.createMatch(this, attribute, isFileMatch);
                attributeMatch.withOwner(this);
                addAttributeMatch(attributeMatch);
            }
            for (Association association : clazz.getAssociations()) {
                if (!GraphUtil.isAssociation(association)) {
                    continue;
                }
                Match associationMatch = Match.createMatch(this, association, isFileMatch);
                associationMatch.withOwner(this);
                addAssociationMatch(associationMatch);
                if (association.isSelfAssoc()) {
                    Match associationOtherMatch =
                            Match.createMatch(this, association.getOther(), isFileMatch);
                    associationOtherMatch.withOwner(this);
                    addAssociationMatch(associationOtherMatch);
                }
            }
            for (Method method : clazz.getMethods()) {
                Match methodMatch = Match.createMatch(this, method, isFileMatch);
                methodMatch.withOwner(this);
                addMethodMatch(methodMatch);
            }
        }
    }

    private void cleanLinks(Clazz oldClazz) {
        Map<Match, Match> possibleLinks = getPossibleLinks();
        for (Attribute oldAttribute : oldClazz.getAttributes()) {
            possibleLinks.remove(getAttributeMatch(oldAttribute));
        }
        for (Association oldAssociation : oldClazz.getAssociations()) {
            possibleLinks.remove(getAssociationMatch(oldAssociation));
        }
        for (Method oldMethod : oldClazz.getMethods()) {
            possibleLinks.remove(getMethodMatch(oldMethod));
        }
    }

    /**
     * Gets the possible links.
     *
     * @return the possible links
     */
    public Map<Match, Match> getPossibleLinks() {
        return possibleLinks;
    }

    private void linkMemberMatches(Match oldMatch, Match newMatch) {
        if (oldMatch != null) {
            oldMatch.withOtherMatch(newMatch);
        }
        /* COPY ALL TO NEW */
        // newMatch.withMetaParent(oldMatch.getMetaParent());
        // newMatch.withParent(oldMatch.getParent());
        // newMatch.withSourceParent(oldMatch.getSourceParent());
        // if(newMatch.getSourceParent() != null) {
        // newMatch.withSourceMatch(true);
        // }
        // if(oldMatch.isMetaMatch()) {
        // newMatch.withMetaMatch(true);
        // }
        // if(oldMatch.isSourceMatch()) {
        // newMatch.withSourceMatch(true);
        // }
        // if(oldMatch.isMetaSourceMatch()) {
        // newMatch.withMetaSourceMatch(true);
        // }
        // remove(oldMatch);
    }
    //
    // public boolean remove(Match match) {
    // GraphMember parent = match.getParent();
    // if(parent instanceof Clazz) {
    // return clazzMatches.remove(match);
    // }
    // if(parent instanceof Attribute) {
    // return attributeMatches.remove(match);
    // }
    // if(parent instanceof Association) {
    // return associationMatches.remove(match);
    // }
    // if(parent instanceof Method) {
    // return methodMatches.remove(match);
    // }
    // return false;
    // }
    //
    // private void cleanLinks(Clazz oldClazz) {
    // Map<Match, Match> possibleLinks = getPossibleLinks();
    // for (Attribute oldAttribute : oldClazz.getAttributes()) {
    // possibleLinks.remove(getAttributeMatch(oldAttribute));
    // }
    // for (Association oldAssociation : oldClazz.getAssociations()) {
    // possibleLinks.remove(getAssociationMatch(oldAssociation));
    // }
    // for (Method oldMethod : oldClazz.getMethods()) {
    // possibleLinks.remove(getMethodMatch(oldMethod));
    // }
    // }

    private boolean matchAttributes(Clazz oldClazz, Clazz newClazz) {
        if (oldClazz == null || newClazz == null) {
            return false;
        }
        AttributeSet oldAttributes = oldClazz.getAttributes();
        AttributeSet newAttributes = newClazz.getAttributes();

        AttributeSet oldMatches = new AttributeSet();
        AttributeSet newMatches = new AttributeSet();

        for (Attribute oldAttribute : oldAttributes) {
            for (Attribute newAttribute : newAttributes) {
                if (newMatches.contains(newAttribute)) {
                    continue;
                }
                if (Double.compare(GraphUtil.compareName(oldAttribute.getName(), newAttribute.getName()),
                        0) == 0) {
                    putPossibleLink(getAttributeMatch(oldAttribute), getAttributeMatch(newAttribute));
                    oldMatches.add(oldAttribute);
                    newMatches.add(newAttribute);
                    break;
                }
            }
        }

        SimpleKeyValueList<Double, SimpleList<Match>> potentMatches =
                preparePotentMatches(oldAttributes, newAttributes, oldMatches, newMatches);

        SortedSet<Double> keys = new TreeSet<Double>(potentMatches.keySet());

        for (Double key : keys) {
            List<Match> potents = potentMatches.get(key);
            for (Match potentMatch : potents) {
                Attribute oldAttribute = (Attribute) potentMatch.getMatch();
                if (oldMatches.contains(oldAttribute)) {
                    continue;
                }

                Attribute newAttribute = (Attribute) potentMatch.getPotentMatch();
                if (newMatches.contains(newAttribute)) {
                    continue;
                }

                if (GraphUtil.compareType(oldAttribute.getType().getName(true),
                        newAttribute.getType().getName(true)) == 0) {
                    putPossibleLink(getAttributeMatch(oldAttribute), getAttributeMatch(newAttribute));
                    oldMatches.add(oldAttribute);
                    newMatches.add(newAttribute);
                }
            }
        }

        return oldMatches.size() == oldAttributes.size() && newMatches.size() == newAttributes.size();
    }

    /**
     * Put possible link.
     *
     * @param sourceMatch the source match
     * @param otherMatch  the other match
     */
    public void putPossibleLink(Match sourceMatch, Match otherMatch) {
        possibleLinks.put(sourceMatch, otherMatch);
    }

    private SimpleKeyValueList<Double, SimpleList<Match>> preparePotentMatches(
            SimpleSet<? extends GraphMember> oldMembers, SimpleSet<? extends GraphMember> newMembers,
            SimpleSet<? extends GraphMember> oldMatches, SimpleSet<? extends GraphMember> newMatches) {
        SimpleKeyValueList<Double, SimpleList<Match>> potentMatches =
                new SimpleKeyValueList<Double, SimpleList<Match>>()
                        .withComparator(EntityComparator.createComparator());

        CharacterBuffer oldBuffer = new CharacterBuffer();
        CharacterBuffer newBuffer = new CharacterBuffer();

        for (GraphMember oldMember : oldMembers) {
            if (oldMatches.contains(oldMember)) {
                continue;
            }
            for (GraphMember newMember : newMembers) {
                if (newMatches.contains(newMember)) {
                    continue;
                }
                boolean executed = false;
                if (oldMember instanceof Association) {
                    Association oldAssociation = (Association) oldMember;
                    Association newAssociation = (Association) newMember;
                    if (oldAssociation.getType().equals(AssociationTypes.EDGE)
                            || newAssociation.getType().equals(AssociationTypes.EDGE)) {
                        oldBuffer.set(oldAssociation.getOther().getName());
                        newBuffer.set(newAssociation.getOther().getName());
                        executed = true;
                    }
                }
                if (!executed) {
                    oldBuffer.set(oldMember.getName());
                    newBuffer.set(newMember.getName());
                }
                Match potentMatch = Match.createPotentMatch(oldMember, newMember);
                potentMatch.withOwner(this);
                double distance = Math.abs(oldBuffer.equalsLevenshtein(newBuffer));
                if (potentMatches.containsKey(distance)) {
                    potentMatches.get(distance).add(potentMatch);
                } else {
                    SimpleList<Match> potents = new SimpleList<Match>();
                    potents.add(potentMatch);
                    potentMatches.add(distance, potents);
                }
            }
        }

        return potentMatches;
    }

    private boolean matchAssociations(Clazz oldClazz, Clazz newClazz) {
        AssociationSet oldAssociations = oldClazz.getAssociations();
        AssociationSet newAssociations = newClazz.getAssociations();

        AssociationSet oldMatches = new AssociationSet();
        AssociationSet newMatches = new AssociationSet();

        for (Association oldAssociation : oldAssociations) {
            if (!GraphUtil.isAssociation(oldAssociation)) {
                oldMatches.add(oldAssociation);
            }
        }
        for (Association newAssociation : newAssociations) {
            if (!GraphUtil.isAssociation(newAssociation)) {
                newMatches.add(newAssociation);
            }
        }

        for (Association oldAssociation : oldAssociations) {
            if (oldMatches.contains(oldAssociation)) {
                continue;
            }
            for (Association newAssociation : newAssociations) {
                if (newMatches.contains(newAssociation)) {
                    continue;
                }
                if (checkAssociationNames(oldAssociation, newAssociation, 0, 0)) {
                    if (oldAssociation.getClazz() == oldAssociation.getOtherClazz()
                            && newAssociation.getClazz() == newAssociation.getOtherClazz()) {
                        putPossibleLink(getAssociationMatch(oldAssociation.getOther()),
                                getAssociationMatch(newAssociation.getOther()));
                    }
                    putPossibleLink(getAssociationMatch(oldAssociation), getAssociationMatch(newAssociation));
                    oldMatches.add(oldAssociation);
                    newMatches.add(newAssociation);
                    break;
                }
            }
        }

        SimpleKeyValueList<Double, SimpleList<Match>> potentMatches =
                preparePotentMatches(oldAssociations, newAssociations, oldMatches, newMatches);

        SortedSet<Double> keys = new TreeSet<Double>(potentMatches.keySet());

        for (Double key : keys) {
            List<Match> potents = potentMatches.get(key);
            for (Match potentMatch : potents) {
                Association oldAssociation = (Association) potentMatch.getMatch();
                if (oldMatches.contains(oldAssociation)) {
                    continue;
                }

                Association newAssociation = (Association) potentMatch.getPotentMatch();
                if (newMatches.contains(newAssociation)) {
                    continue;
                }

                if (oldClazz.getName().equals(newClazz.getName())
                        || checkAssociationNames(oldAssociation, newAssociation, 3, -1)) {
                    if (oldAssociation.getClazz() == oldAssociation.getOtherClazz()
                            && newAssociation.getClazz() == newAssociation.getOtherClazz()) {
                        putPossibleLink(getAssociationMatch(oldAssociation.getOther()),
                                getAssociationMatch(newAssociation.getOther()));
                    }
                    putPossibleLink(getAssociationMatch(oldAssociation), getAssociationMatch(newAssociation));
                    oldMatches.add(oldAssociation);
                    newMatches.add(newAssociation);
                }
            }
        }

        return oldMatches.size() == oldAssociations.size()
                && newMatches.size() == newAssociations.size();
    }

    private boolean checkAssociationNames(Association oldAssociation, Association newAssociation,
                                          int distance, int equals) {
        if ((oldAssociation.getType() == AssociationTypes.EDGE
                && newAssociation.getType() != AssociationTypes.EDGE)) {
            if (Double.compare(GraphUtil.compareName(oldAssociation.getName(), newAssociation.getName()),
                    distance) == equals) {
                return true;
            }
        }
        if ((oldAssociation.getOther().getType() == AssociationTypes.EDGE
                && newAssociation.getOther().getType() != AssociationTypes.EDGE)) {
            return Double.compare(GraphUtil.compareName(oldAssociation.getOther().getName(),
                    newAssociation.getOther().getName()), distance) == equals;
        }
        return false;
    }

    /**
     * Gets the association match.
     *
     * @param association the association
     * @return the association match
     */
    public Match getAssociationMatch(GraphMember association) {
        for (Match associationMatch : associationMatches) {
            if (associationMatch.getMatch() == association) {
                return associationMatch;
            }
        }
        return null;
    }

    private boolean matchMethods(Clazz oldClazz, Clazz newClazz) {
        if (oldClazz == null || newClazz == null) {
            return oldClazz == newClazz;
        }
        MethodSet oldMethods = oldClazz.getMethods();
        MethodSet newMethods = newClazz.getMethods();

        MethodSet oldMatches = new MethodSet();
        MethodSet newMatches = new MethodSet();

        for (Method oldMethod : oldMethods) {
            for (Method newMethod : newMethods) {
                if (newMatches.contains(newMethod)) {
                    continue;
                }
                if (Double.compare(GraphUtil.compareName(oldMethod.getName(), newMethod.getName()),
                        0) == 0) {
                    putPossibleLink(getMethodMatch(oldMethod), getMethodMatch(newMethod));
                    oldMatches.add(oldMethod);
                    newMatches.add(newMethod);
                    break;
                }
            }
        }

        SimpleKeyValueList<Double, SimpleList<Match>> potentMatches =
                preparePotentMatches(oldMethods, newMethods, oldMatches, newMatches);

        SortedSet<Double> keys = new TreeSet<Double>(potentMatches.keySet());

        for (Double key : keys) {
            List<Match> potents = potentMatches.get(key);
            for (Match potentMatch : potents) {
                Method oldMethod = (Method) potentMatch.getMatch();
                if (oldMatches.contains(oldMethod)) {
                    continue;
                }

                Method newMethod = (Method) potentMatch.getPotentMatch();
                if (newMatches.contains(newMethod)) {
                    continue;
                }

                if (GraphUtil.compareType(oldMethod.getReturnType().getName(true),
                        newMethod.getReturnType().getName(true)) == 0) {
                    putPossibleLink(getMethodMatch(oldMethod), getMethodMatch(newMethod));
                    oldMatches.add(oldMethod);
                    newMatches.add(newMethod);
                }
            }
        }

        return oldMatches.size() == oldMethods.size() && newMatches.size() == newMethods.size();
    }

    /**
     * Gets the attribute match.
     *
     * @param attribute the attribute
     * @return the attribute match
     */
    public Match getAttributeMatch(GraphMember attribute) {
        for (Match attributeMatch : attributeMatches) {
            if (attributeMatch.getMatch() == attribute) {
                return attributeMatch;
            }
        }
        return null;
    }

    /**
     * Gets the method match.
     *
     * @param method the method
     * @return the method match
     */
    public Match getMethodMatch(GraphMember method) {
        for (Match methodMatch : methodMatches) {
            if (methodMatch.getMatch() == method) {
                return methodMatch;
            }
        }
        return null;
    }

    /**
     * Gets the clazz match.
     *
     * @param clazz the clazz
     * @return the clazz match
     */
    public Match getClazzMatch(GraphMember clazz) {
        for (Match clazzMatch : clazzMatches) {
            if (clazzMatch.getMatch() == clazz) {
                return clazzMatch;
            }
        }
        return null;
    }

    private boolean matchClazzValues(Clazz oldClazz, Clazz newClazz) {
        if (!oldClazz.getName().equals(newClazz.getName())) {
            return false;
        }

        AttributeSet oldAttributes = oldClazz.getAttributes();
        AttributeSet newAttributes = newClazz.getAttributes();
        AttributeSet metaMatchedAttributes = new AttributeSet();
        AttributeSet newMatchedAttributes = new AttributeSet();

        for (Attribute oldAttribute : oldAttributes) {
            for (Attribute newAttribute : newAttributes) {
                if (newMatchedAttributes.contains(newAttribute)) {
                    continue;
                }
                if (!newMatchedAttributes.contains(newAttribute)) {
                    if (matchAttributeValues(oldAttribute, newAttribute)) {
                        metaMatchedAttributes.add(oldAttribute);
                        newMatchedAttributes.add(newAttribute);
                    }
                }
            }
        }

        AssociationSet oldAssociations = oldClazz.getAssociations();
        AssociationSet newAssociations = newClazz.getAssociations();
        AssociationSet metaMatchedAssociations = new AssociationSet();
        AssociationSet newMatchedAssociations = new AssociationSet();

        for (Association oldAssociation : oldAssociations) {
            if (!GraphUtil.isAssociation(oldAssociation)) {
                metaMatchedAssociations.add(oldAssociation);
            }
        }
        for (Association newAssociation : newAssociations) {
            if (!GraphUtil.isAssociation(newAssociation)) {
                newMatchedAssociations.add(newAssociation);
            }
        }

        for (Association oldAssociation : oldAssociations) {
            if (metaMatchedAssociations.contains(oldAssociation)) {
                continue;
            }
            for (Association newAssociation : newAssociations) {
                if (newMatchedAssociations.contains(newAssociation)) {
                    continue;
                }
                if (!newMatchedAssociations.contains(newAssociation)) {
                    if (matchAssociationValues(oldAssociation, newAssociation)) {
                        metaMatchedAssociations.add(oldAssociation);
                        newMatchedAssociations.add(newAssociation);

                        if (oldAssociation.getClazz() == oldAssociation.getOtherClazz()
                                && newAssociation.getClazz() == newAssociation.getOtherClazz()) {
                            matchAssociationValues(oldAssociation.getOther(), newAssociation.getOther());
                        }
                    }
                }
            }
        }

        MethodSet oldMethods = oldClazz.getMethods();
        MethodSet newMethods = newClazz.getMethods();
        MethodSet metaMatchedMethods = new MethodSet();
        MethodSet newMatchedMethods = new MethodSet();

        for (Method oldMethod : oldMethods) {
            for (Method newMethod : newMethods) {
                if (newMatchedMethods.contains(newMethod)) {
                    continue;
                }
                if (!newMatchedMethods.contains(newMethod) && matchMethodValues(oldMethod, newMethod)) {
                    metaMatchedMethods.add(oldMethod);
                    newMatchedMethods.add(newMethod);
                }
            }
        }

        ClazzSet oldSuperClazzes = oldClazz.getSuperClazzes(false);
        oldSuperClazzes.addAll(oldClazz.getInterfaces(false));
        ClazzSet newSuperClazzes = newClazz.getSuperClazzes(false);
        newSuperClazzes.addAll(newClazz.getInterfaces(false));
        ClazzSet metaMatchedSuperClazzes = new ClazzSet();
        ClazzSet newMatchedSuperClazzes = new ClazzSet();

        for (Clazz oldSuperClazz : oldSuperClazzes) {
            for (Clazz newSuperClazz : newSuperClazzes) {
                if (newMatchedSuperClazzes.contains(newSuperClazz)) {
                    continue;
                }
                if (oldSuperClazz.getName().equals(newSuperClazz.getName())) {
                    metaMatchedSuperClazzes.add(oldSuperClazz);
                    newMatchedSuperClazzes.add(newSuperClazz);
                }
            }
        }

        return oldAttributes.size() == metaMatchedAttributes.size()
                && newAttributes.size() == newMatchedAttributes.size()
                && oldAssociations.size() == metaMatchedAssociations.size()
                && newAssociations.size() == newMatchedAssociations.size()
                && oldMethods.size() == metaMatchedMethods.size()
                && newMethods.size() == newMatchedMethods.size()
                && oldSuperClazzes.size() == metaMatchedSuperClazzes.size()
                && newSuperClazzes.size() == newMatchedSuperClazzes.size()
                && oldClazz.getType().equals(newClazz.getType())
                && oldClazz.getModifier().toString().equals(newClazz.getModifier().toString());
    }

    private boolean matchAttributeValues(Attribute oldAttribute, Attribute newAttribute) {
        if (oldAttribute == null || newAttribute == null) {
            return false;
        }
        if (!oldAttribute.getName().equals(newAttribute.getName())) {
            return false;
        }
        if (!oldAttribute.getType().equals(newAttribute.getType())) {
            return false;
        }
        return oldAttribute.getModifier().toString().equals(newAttribute.getModifier().toString());

        // FIXME if (matchData != null) {
        // matchData.getAttributeMatch(newAttribute)
        // .withMetaMatch(true)
        // .withMetaParent(oldAttribute);
        // }
    }

    private boolean matchAssociationValues(Association oldAssociation, Association newAssociation) {
        if (oldAssociation == null || newAssociation == null) {
            return false;
        }
        if (!oldAssociation.getName().equals(newAssociation.getName())) {
            return false;
        }
        if (!oldAssociation.getType().equals(newAssociation.getType())) {
            return false;
        }
        if (oldAssociation.getCardinality() != newAssociation.getCardinality()) {
            return false;
        }
        if (oldAssociation.getModifier() != null && newAssociation.getModifier() != null
                && (!oldAssociation.getModifier().toString()
                .equals(newAssociation.getModifier().toString()))) {
            return false;
        } else if (oldAssociation.getModifier() != null || newAssociation.getModifier() != null) {
            return false;
        }
        if (!oldAssociation.getOther().getName().equals(newAssociation.getOther().getName())) {
            return false;
        }
        if (!oldAssociation.getOther().getType().equals(newAssociation.getOther().getType())) {
            return false;
        }
        if (oldAssociation.getOther().getCardinality() != newAssociation.getOther().getCardinality()) {
            return false;
        }
        if (oldAssociation.getOther().getModifier() != null && newAssociation.getModifier() != null) {
            if (!oldAssociation.getOther().getModifier().toString()
                    .equals(newAssociation.getOther().getModifier().toString())) {
                return false;
            }
        } else if (oldAssociation.getOther().getModifier() != null
                || newAssociation.getOther().getModifier() != null) {
            return false;
        }
        return oldAssociation.getOtherClazz().getName()
                .equals(newAssociation.getOtherClazz().getName());

        // FIXME if (matchData != null) {
        // matchData.getAssociationMatch(newAssociation)
        // .withMetaMatch(true)
        // .withMetaParent(oldAssociation);
        // }
    }

    private boolean matchMethodValues(Method oldMethod, Method newMethod) {
        if (!oldMethod.getName().equals(newMethod.getName())) {
            return false;
        }
        if (!oldMethod.getModifier().toString().equals(newMethod.getModifier().toString())) {
            return false;
        }
        if (!oldMethod.getReturnType().equals(newMethod.getReturnType())) {
            return false;
        }
        ParameterSet oldParameters = oldMethod.getParameters();
        ParameterSet newParameters = newMethod.getParameters();
        if (oldParameters.size() != newParameters.size()) {
            return false;
        }
        boolean found = false;
        for (Parameter oldParameter : oldParameters) {
            found = false;
            for (Parameter newParameter : newParameters) {
                if (oldParameter.getName() != null && newParameter.getName() != null
                        && !oldParameter.getName().equals(newParameter.getName())) {
                    continue;
                }
                if (!oldParameter.getType().equals(newParameter.getType())) {
                    continue;
                }
                if (oldParameter.getModifier() == null) {
                    if (newParameter.getModifier() != null) {
                        continue;
                    }
                } else if (!oldParameter.getModifier().toString()
                        .equals(newParameter.getModifier().toString())) {
                    continue;
                }

                found = true;
                break;
            }
            if (!found) {
                return false;
            }
        }
        // if (oldMethod.getBody() == newMethod.getBody() &&
        // !oldMethod.getBody().equals(newMethod.getBody())) {
        if (oldMethod.getBody() == null) {
            return newMethod.getBody() == null;
        } else
            return oldMethod.getBody().equals(newMethod.getBody());

        // FIXME if (matchData != null) {
        // matchData.getMethodMatch(newMethod)
        // .withMetaMatch(true)
        // .withMetaParent(oldMethod);
        // }
    }

    /**
     * Adds the clazz match.
     *
     * @param clazzMatch the clazz match
     */
    public void addClazzMatch(Match clazzMatch) {
        clazzMatches.add(clazzMatch);
    }

    /**
     * Adds the attribute match.
     *
     * @param attributeMatch the attribute match
     */
    public void addAttributeMatch(Match attributeMatch) {
        attributeMatches.add(attributeMatch);
    }

    /**
     * Adds the association match.
     *
     * @param associationMatch the association match
     */
    public void addAssociationMatch(Match associationMatch) {
        associationMatches.add(associationMatch);
    }

    /**
     * Adds the method match.
     *
     * @param methodMatch the method match
     */
    public void addMethodMatch(Match methodMatch) {
        methodMatches.add(methodMatch);
    }

    /**
     * Gets the clazz matches.
     *
     * @return the clazz matches
     */
    public List<Match> getClazzMatches() {
        return clazzMatches;
    }

    /**
     * Gets the old model.
     *
     * @return the old model
     */
    public GraphModel getOldModel() {
        return oldModel;
    }

    /**
     * Gets the new model.
     *
     * @return the new model
     */
    public GraphModel getNewModel() {
        return newModel;
    }

    /**
     * Adds the diff.
     *
     * @param diff the diff
     * @return true, if successful
     */
    public boolean addDiff(Match diff) {
        return diffs.add(diff);
    }

    /**
     * Gets the meta model.
     *
     * @return the meta model
     */
    public Object getMetaModel() {
        return metaModel;
    }

    /**
     * Generate.
     *
     * @return true, if successful
     */
    public boolean generate() {
        createMatches();
        // GraphSimpleSet diffs = getDiffs();
        // GraphConverter converter = new GraphConverter();
        // TemplateResultFragment fragment =
        // converter.convertToAdvanced(TemplateResultFragment.create(diffs, true,
    // true));
    // CharacterBuffer value = fragment.getValue();
    return true;
  }
}
