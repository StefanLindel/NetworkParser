package de.uniks.networkparser.parser.java;

import de.uniks.networkparser.parser.Template;

/**
 * The Class JavaSetMethod.
 *
 * @author Stefan
 */
public class JavaSetMethod extends Template {

    /**
     * Instantiates a new java set method.
     */
    public JavaSetMethod() {
        id = "method";
        type = METHOD;
        withTemplate(
                "{{#foreach {{parameter}}}}"
                        + "{{#if {{#AND}}{{item.typeClazz.type}}==class {{file.member.name}}{{#ENDAND}}}}"
                        + "{{#import {{item.type(false)}}}}" + "{{#endif}}" + "{{#endfor}}"
                        + "	{{visibility}} {{modifiers} }{{file.member.name}}Set {{name}}{{parameterName}} {",
                "		return {{file.member.name}}Set.EMPTY_SET;", "	}", "");
    }
}
