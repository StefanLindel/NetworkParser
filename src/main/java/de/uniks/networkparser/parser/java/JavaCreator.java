package de.uniks.networkparser.parser.java;

import de.uniks.networkparser.IdMap;
import de.uniks.networkparser.graph.Association;
import de.uniks.networkparser.graph.Attribute;
import de.uniks.networkparser.graph.Feature;
import de.uniks.networkparser.graph.GraphMember;
import de.uniks.networkparser.interfaces.LocalisationInterface;
import de.uniks.networkparser.interfaces.SendableEntityCreator;
import de.uniks.networkparser.parser.Template;

/**
 * The Class JavaCreator.
 *
 * @author Stefan
 */
public class JavaCreator extends Template {

    /**
     * Instantiates a new java creator.
     */
    public JavaCreator() {
        id = TYPE_JAVA + ".creator";
        extension = "java";
        fileType = "clazz";
        path = "util";
        postfix = "Creator";
        type = TEMPLATE;

        withTemplate(
                "{{#template PACKAGE}}{{#if {{packageName}}}}package {{packageName}}.util;{{#endif}}{{#endtemplate}}",
                "",

                "{{#template IMPORT}}{{#foreach {{file.headers}}}}", "import {{item}};{{#endfor}}{{#endtemplate}}", "",
                "{{#import " + SendableEntityCreator.class.getName() + "}}" + "{{#import {{fullName}}}}",
                "{{visibility}} class {{name}}Creator implements SendableEntityCreator", "{", "",
                "   private final String[] properties = new String[]", "   {", "{{#foreach child}}",
                "{{#if {{item.className}}==" + Attribute.class.getName() + "}}",
                "      {{name}}.PROPERTY_{{item.NAME}},", "{{#endif}}",
                "{{#if {{item.className}}==" + Association.class.getName() + "}}", "{{#if {{item.other.isGenerate}}}}",
                "{{#import {{item.other.clazz.fullName}}}}", "      {{name}}.PROPERTY_{{item.other.NAME}},",
                "{{#endif}}", "{{#endif}}", "{{#endfor}}", "{{#if {{#feature DYNAMICVALUES}}}}",
                "      SendableEntityCreator.DYNAMIC", "{{#endif}}", "   };", "",

                "   @Override", "   public String[] getProperties()", "   {", "      return properties;", "   }", "",

                "   @Override", "   public Object getSendableInstance(boolean prototyp)", "   {",
                "{{#if {{#AND}}{{type}}==class {{#NOT}}{{modifiers#contains(abstract)}}{{#ENDNOT}}{{#ENDAND}}}}",
                "      return new {{name}}();", "{{#else}}", "      return null;", "{{#endif}}", "   }", "",

                "   @Override", "   public Object getValue(Object entity, String attribute)", "   {",
                "      if(attribute == null || !(entity instanceof {{name}})) {", "          return null;", "      }",
                "      {{name}} element = ({{name}})entity;", "{{#foreach child}}",
                "{{#if {{item.className}}==" + Attribute.class.getName() + "}}",
                "      if ({{name}}.PROPERTY_{{item.NAME}}.equalsIgnoreCase(attribute))", "      {",
                "         return element.{{#if {{item.type}}==boolean}}is{{#else}}get{{#endif}}{{item.Name}}();",
                "      }", "", "{{#endif}}", "{{#if {{item.className}}==" + Association.class.getName() + "}}",
                "{{#if {{item.other.isGenerate}}}}",
                "      if ({{name}}.PROPERTY_{{item.other.NAME}}.equalsIgnoreCase(attribute))", "      {",
                "         return element.get{{item.other.Name}}();", "      }", "", "{{#endif}}", "{{#endif}}",
                "{{#endfor}}", "{{#if {{#feature DYNAMICVALUES}}}}",
                "      if(SendableEntityCreator.DYNAMIC.equalsIgnoreCase(attribute)) {",
                "          return element.getDynamicValues();", "      }",
                "      return element.getDynamicValue(attribute);", "{{#else}}", "      return null;", "{{#endif}}",
                "   }", "",

                "   @Override",
                "   public boolean setValue(Object entity, String attribute, Object value, String type)", "   {",
                "      if(attribute == null || !(entity instanceof {{name}})) {", "          return false;", "      }",
                "      {{name}} element = ({{name}})entity;",
                "      if (SendableEntityCreator.REMOVE.equals(type) && value != null)", "      {",
                "         attribute = attribute + type;", "      }", "",

                "{{#foreach child}}", "{{#if {{item.className}}==" + Attribute.class.getName() + "}}",
                "{{#ifnot {{item.modifiers#contains(static)}}}}",
                "      if ({{name}}.PROPERTY_{{item.NAME}}.equalsIgnoreCase(attribute))",
                "      {" + "{{#import {{item.type(false)}}}}",
                "         element.set{{item.Name}}(({{item.type.name}}) value);", "         return true;", "      }",
                "", "{{#endif}}", "{{#endif}}", "{{#if {{item.className}}==" + Association.class.getName() + "}}",
                "{{#if {{item.other.isGenerate}}}}",
                "      if ({{name}}.PROPERTY_{{item.other.NAME}}.equalsIgnoreCase(attribute))", "      {",
                "         element.{{#if {{item.other.cardinality}}==1}}set{{#else}}with{{#endif}}{{item.other.Name}}(({{item.other.clazz.name}}) value);",
                "         return true;", "      }", "", "{{#endif}}", "{{#endif}}", "{{#endfor}}",
                "{{#if {{#feature DYNAMICVALUES}}}}", "      element.withDynamicValue(attribute, value);",
                "      return true;", "{{#else}}", "      return false;", "{{#endif}}", "   }", "", "",
                "{{#import " + IdMap.class.getName() + "}}" + "   public static IdMap createIdMap(String session) {",
                "      return CreatorCreator.createIdMap(session);", "   }",
                "{{#template TEMPLATEEND}}}{{#endtemplate}}");
    }

    protected boolean isValid(GraphMember member, LocalisationInterface parameters) {
        if (!super.isValid(member, parameters)) {
            return false;
        }
        /* Check for existing Feature Serialization */
        Feature features = getFeature(Feature.SERIALIZATION, member.getClazz());
        return features != null;
    }
}
