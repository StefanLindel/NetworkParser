package de.uniks.networkparser.parser;

/*
 * The MIT License
 *
 * Copyright (c) 2010-2016 Stefan Lindel https://www.github.com/fujaba/NetworkParser/
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

import de.uniks.networkparser.buffer.CharacterBuffer;
import de.uniks.networkparser.interfaces.*;

/**
 * Add so for Adding some Code for example getValue:Attribute or CreatorCreator.
 *
 * @author Stefan Lindel
 */
public class MethodAddCondition implements ParserCondition, SendableEntityCreator {

    /**
     * The Constant PROPERTY_CHILD.
     */
    public static final String PROPERTY_CHILD = "child";
    protected ObjectCondition child;

    /**
     * Update.
     *
     * @param value the value
     * @return true, if successful
     */
    @Override
    public boolean update(Object value) {
        return false;
    }

    /**
     * Gets the properties.
     *
     * @return the properties
     */
    @Override
    public String[] getProperties() {
        return null;
    }

    /**
     * Gets the value.
     *
     * @param entity    the entity
     * @param attribute the attribute
     * @return the value
     */
    @Override
    public Object getValue(Object entity, String attribute) {
        return null;
    }

    /**
     * Sets the value.
     *
     * @param entity    the entity
     * @param attribute the attribute
     * @param value     the value
     * @param type      the type
     * @return true, if successful
     */
    @Override
    public boolean setValue(Object entity, String attribute, Object value, String type) {
        return false;
    }

    /**
     * Gets the value.
     *
     * @param variables the variables
     * @return the value
     */
    @Override
    public Object getValue(LocalisationInterface variables) {
        return null;
    }

    /**
     * Creates the.
     *
     * @param buffer         the buffer
     * @param parser         the parser
     * @param customTemplate the custom template
     */
    @Override
    public void create(CharacterBuffer buffer, TemplateParser parser,
                       LocalisationInterface customTemplate) {
    }

    /**
     * Checks if is expression.
     *
     * @return true, if is expression
     */
    @Override
    public boolean isExpression() {
        return false;
    }

    /**
     * Gets the key.
     *
     * @return the key
     */
    @Override
    public String getKey() {
        return null;
    }

    /**
     * Gets the sendable instance.
     *
     * @param isExpression the is expression
     * @return the sendable instance
   */
  @Override
  public Object getSendableInstance(boolean isExpression) {
    return null;
  }
}
