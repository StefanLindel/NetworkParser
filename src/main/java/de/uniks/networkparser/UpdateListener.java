package de.uniks.networkparser;

/*
 * The MIT License Copyright (c) 2010-2016 Stefan Lindel
 * https://www.github.com/fujaba/NetworkParser/
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
import de.uniks.networkparser.interfaces.BaseItem;
import de.uniks.networkparser.interfaces.Entity;
import de.uniks.networkparser.interfaces.Grammar;
import de.uniks.networkparser.interfaces.MapListener;
import de.uniks.networkparser.interfaces.ObjectCondition;
import de.uniks.networkparser.interfaces.SendableEntityCreator;
import de.uniks.networkparser.list.SimpleIteratorSet;
import de.uniks.networkparser.list.SimpleList;
import java.beans.PropertyChangeEvent;
import java.util.Collection;
import java.util.Map.Entry;

/**
 * The listener interface for receiving update events. The class that is interested in processing a
 * update event implements this interface, and the object created with that class is registered with
 * a component using the component's <code>addUpdateListener</code> method. When the update event
 * occurs, that object's appropriate method is invoked.
 *
 * @author Stefan
 * @see SimpleEvent
 */
public class UpdateListener implements MapListener, ObjectCondition {
    /**
     * The Constant TYPE_OP_ADD.
     */
    public static final String TYPE_OP_ADD = "add";
    /**
     * The Constant TYPE_OP_REMOVE.
     */
    public static final String TYPE_OP_REMOVE = "remove";
    /**
     * The Constant TYPE_OP_REPLACE.
     */
    public static final String TYPE_OP_REPLACE = "replace";
    /**
     * The Constant TYPE_OP_COPY.
     */
    public static final String TYPE_OP_COPY = "copy";
    /**
     * The Constant TYPE_OP_MOVE.
     */
    public static final String TYPE_OP_MOVE = "move";
    /**
     * The Constant TYPE_OP_TEST.
     */
    public static final String TYPE_OP_TEST = "test";
    /**
     * The map.
     */
    private final SimpleMap map;
    private final Tokener factory;
    /**
     * The update listener.
     */
    protected ObjectCondition condition;
    /**
     * The suspend id list.
     */
    private SimpleList<UpdateCondition> suspendIdList;
    private Filter updateFilter = new Filter().withStrategy(SendableEntityCreator.UPDATE)
            .withConvertable(new UpdateCondition());
    private Object root;

    /**
     * Instantiates a new update listener.
     *
     * @param map     the map
     * @param factory Factory to create new Items
     */
    public UpdateListener(SimpleMap map, Tokener factory) {
        this.map = map;
        this.factory = factory;
    }

    /**
     * Suspend notification.
     *
     * @param accumulates Notification Listener
     * @return success for suspend Notification
     */
    public boolean suspendNotification(UpdateCondition... accumulates) {
        suspendIdList = new SimpleList<UpdateCondition>();
        if (accumulates == null) {
            suspendIdList.add(UpdateCondition.createAcumulateCondition(factory));
        } else {
            for (UpdateCondition item : accumulates) {
                suspendIdList.add(item);
            }
        }
        return true;
    }

    /**
     * Reset notification.
     *
     * @return the simple list
     */
    public SimpleList<UpdateCondition> resetNotification() {
        SimpleList<UpdateCondition> list = suspendIdList;
        suspendIdList = null;
        return list;
    }

    /**
     * Property change.
     *
     * @param evt the evt
     */
    /*
     * (non-Javadoc)
     *
     * @see java.beans.PropertyChangeListener#propertyChange(java.beans. PropertyChangeEvent)
     */
    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        if (evt == null) {
            return;
        }
        Object oldValue = evt.getOldValue();
        Object newValue = evt.getNewValue();

        if ((oldValue == null && newValue == null) || (oldValue != null && oldValue.equals(newValue))) {
            /* Nothing to do */
            return;
        }
        /* put changes into msg and send to receiver */
        Object source;
        if (evt instanceof SimpleEvent) {
            source = ((SimpleEvent) evt).getModelValue();
        } else {
            source = evt.getSource();
        }
        String property = evt.getPropertyName();
        SendableEntityCreator creatorClass = map.getCreatorClass(source);
        if (creatorClass == null) {
            /* this class is not supported, do nor replicate */
            return;
        }

        if (suspendIdList != null) {
            boolean notifiy = true;
            for (UpdateCondition listener : suspendIdList) {
                if (listener.changeAttribute(this, source, creatorClass, property, oldValue, newValue)) {
                    notifiy = false;
                }
            }
            if (!notifiy) {
                return;
            }
        }

        Entity jsonObject = change(property, source, creatorClass, oldValue, newValue);
        /* Add Message Value */
        ObjectCondition listener = map.getUpdateListener();
        if (listener == null) {
            return;
        }
        if (oldValue != null && newValue != null) {
            listener.update(new SimpleEvent(SendableEntityCreator.UPDATE, jsonObject, evt, map));
        } else {
            listener.update(new SimpleEvent(SendableEntityCreator.NEW, jsonObject, evt, map));
        }
    }

    /**
     * Change.
     *
     * @param property     the property
     * @param source       the source
     * @param creatorClass the creator class
     * @param oldValue     the old value
     * @param newValue     the new value
     * @return the entity
     */
    public Entity change(String property, Object source, SendableEntityCreator creatorClass,
                         Object oldValue, Object newValue) {
        if (factory == null) {
            return null;
        }
        Entity jsonObject = factory.newInstance();
        String id = map.getId(source, true);
        Grammar grammar = map.getGrammar();
        grammar.writeBasicValue(jsonObject, source.getClass().getName(), id,
                SendableEntityCreator.UPDATE, map);
        change(property, creatorClass, jsonObject, oldValue, newValue);
        return jsonObject;
    }

    /**
     * Change.
     *
     * @param property the property
     * @param creator  the creator
     * @param change   the change
     * @param oldValue the old value
     * @param newValue the new value
     * @return true, if successful
     */
    public boolean change(String property, SendableEntityCreator creator, Entity change,
                          Object oldValue, Object newValue) {
        boolean done = false;
        if (creator == null) {
            return false;
        }
        String[] properties = creator.getProperties();
        if (properties == null) {
            return false;
        }
        for (String attrName : properties) {
            if (attrName.equals(property)) {
                done = true;
                break;
            }
        }
        if (!done) {
            /*
             * this property is not part of the replicated model, do not replicate if propertyname is not
             * found and the name is REMOVE_YOU it remove it from the IdMap
             */
            if (SendableEntityCreator.REMOVE_YOU.equals(property)) {
                removeObj(oldValue, true);
            }
            return false;
        }

        SendableEntityCreator creatorClass;
        Object child = null;
        Entity entity;
        Grammar grammar = map.getGrammar();
        if (oldValue != null) {
            String key = property;
            creatorClass = map.getCreatorClass(oldValue);
            if (grammar.isFlatFormat()) {
                /* NEW VERSION */
                key = SimpleMap.ENTITYSPLITTER + SendableEntityCreator.REMOVE + SimpleMap.ENTITYSPLITTER
                        + property;
                entity = change;
            } else {
                child = change.getValue(SendableEntityCreator.REMOVE);
                if (child instanceof Entity) {
                    entity = (Entity) child;
                } else {
                    entity = factory.newInstance();
                    change.put(SendableEntityCreator.REMOVE, entity);
                }
            }

            if (creatorClass != null) {
                String oldId = map.getId(oldValue, true);
                if (oldId != null) {
                    Entity childItem = factory.newInstance();
                    childItem.put(SimpleMap.ID, oldId);
                    entity.put(key, childItem);
                }
            } else {
                entity.put(key, oldValue);
            }
        }

        if (newValue != null) {
            String key = property;
            creatorClass = map.getCreatorClass(newValue);
            if (grammar.isFlatFormat()) {
                /* NEW VERSION */
                key = SimpleMap.ENTITYSPLITTER + SendableEntityCreator.UPDATE + SimpleMap.ENTITYSPLITTER
                        + property;
                entity = change;
            } else {
                child = change.getValue(SendableEntityCreator.UPDATE);
                if (child instanceof Entity) {
                    entity = (Entity) child;
                } else {
                    entity = factory.newInstance();
                    change.put(SendableEntityCreator.UPDATE, entity);
                }
            }

            if (creatorClass != null) {
                String keys = map.getKey(newValue);
                if (keys != null) {
                    Entity item = factory.newInstance();
                    item.put(BaseItem.CLASS, newValue.getClass().getName());
                    item.put(SimpleMap.ID, keys);
                    entity.put(key, item);
                } else {
                    Entity item = (Entity) map.encode(newValue, factory, updateFilter);
                    entity.put(key, item);
                }
            } else {
                /* plain attribute */
                entity.put(key, newValue);
            }
        }
        return true;
    }

    /**
     * Execute.
     *
     * @param updateMessage the update message
     * @param filter        Filter for exclude UpdateMessages
     * @return the MasterObject, if successful
     */
    public Object execute(Entity updateMessage, Filter filter) {
        if (map == null || !updateMessage.has(SendableEntityCreator.UPDATE)
                && !updateMessage.has(SendableEntityCreator.REMOVE)) {
            return null;
        }
        String id = updateMessage.getString(SimpleMap.ID);
        /* Check for JSONPatch */
        String op = updateMessage.getString("OP");
        String path = updateMessage.getString("PATH");
        if (op.length() > 0 && path.length() > 0) {
            return executePatch(op, path, updateMessage);
        }
        Entity remove = (Entity) updateMessage.getValue(SendableEntityCreator.REMOVE);
        Entity update = (Entity) updateMessage.getValue(SendableEntityCreator.UPDATE);

        /* Object prio = updateMessage.getValue(Filter.PRIO); */
        Object masterObj = map.getObject(id);
        if (masterObj == null) {
            String masterObjClassName = (String) updateMessage.getValue(BaseItem.CLASS);
            if (masterObjClassName != null) {
                /* cool, lets make it */
                SendableEntityCreator creator = map.getCreator(masterObjClassName, true, true, null);
                masterObj = creator.getSendableInstance(false);
            }
            if (masterObj == null) {
                return null;
            }
            map.put(id, masterObj, false);
        }
        SendableEntityCreator creator = map.getCreatorClass(masterObj);
        if (remove == null && update != null) {
            /* create Message */
            Object refObject = creator.getSendableInstance(true);
            for (SimpleIteratorSet<String, Object> i = new SimpleIteratorSet<String, Object>(update); i
                    .hasNext(); ) {
                Entry<String, Object> item = i.next();
                String key = item.getKey();
                Object value = creator.getValue(masterObj, key);
                if (value == null) {
                    /* Old Value is Standard */
                    return setValue(creator, masterObj, key, item.getValue(), SendableEntityCreator.NEW);
                } else if (value instanceof Collection<?>) {
                    /*
                     * just add the element It is a to many Link
                     */
                    return setValue(creator, masterObj, key, item.getValue(), SendableEntityCreator.NEW);
                } else if (value.equals(creator.getValue(refObject, key))) {
                    /* Old Value is Standard */
                    return setValue(creator, masterObj, key, update.getValue(key), SendableEntityCreator.NEW);
                }
            }
            return true;
        } else if (update == null && remove != null) {
            /* delete Message */
            Object refObject = creator.getSendableInstance(true);
            for (int i = 0; i < remove.size(); i++) {
                String key = remove.getKeyByIndex(i);
                Object value = creator.getValue(masterObj, key);
                if (value instanceof Collection<?>) {
                    Entity removeJsonObject = (Entity) remove.getValue(key);
                    setValue(creator, masterObj, key, removeJsonObject, SendableEntityCreator.REMOVE);
                } else {
                    if (checkValue(value, key, remove)) {
                        setValue(creator, masterObj, key, creator.getValue(refObject, key),
                                SendableEntityCreator.REMOVE);
                    }
                }
                Object removeJsonObject = remove.getValue(key);
                if (removeJsonObject instanceof Entity) {
                    Entity json = (Entity) removeJsonObject;
                    map.notify(
                            new SimpleEvent(SendableEntityCreator.REMOVE, json, map, key, map.decode(json), null)
                                    .withModelValue(masterObj));
                }

            }
            return masterObj;
        } else if (update != null) {
            /* update Message */
            for (int i = 0; i < update.size(); i++) {
                String key = update.getKeyByIndex(i);
                /* CHECK WITH REMOVE key */
                Object oldValue = creator.getValue(masterObj, key);

                if (checkValue(oldValue, key, remove)) {
                    Object newValue = update.getValue(key);
                    setValue(creator, masterObj, key, newValue, SendableEntityCreator.UPDATE);

                    map.notify(
                            new SimpleEvent(SendableEntityCreator.UPDATE, update, map, key, oldValue, newValue)
                                    .withModelValue(masterObj));
                }
            }
            return masterObj;
        }
        return null;
    }

    private Entity getElement(String path, Entity element, Entity parent) {
        if (path == null) {
            return null;
        }
        int pos = path.indexOf("/");
        if (pos > 0) {
            Object child = element.getValue(path.substring(0, pos));
            if (child instanceof Entity) {
                return getElement(path.substring(pos + 1), (Entity) child, element);
            }
        } else {
            /* Last One */
            return parent;
        }
        return null;
    }

    /**
     * Executing Path Add { "op": "add", "path": "/biscuits/1", "value": { "name": "Ginger Nut" } }
     * Adds a value to an object or inserts it into an array. In the case of an array, the value is
     * inserted before the given index. The - character can be used instead of an index to insert at
     * the end of an array.
     *
     * <p>Remove { "op": "remove", "path": "/biscuits" } Removes value from an object or array.
     * { "op": "remove", "path": "/biscuits/0" } Removes the first element of the array at biscuits
     * (or just removes the "0" key if biscuits is an object)
     *
     * <p>Replace { "op": "replace", "path": "/biscuits/0/name", "value": "Chocolate Digestive" }
     * Replaces a value. Equivalent to a "remove" followed by an "add".
     *
     * <p>Copy { "op": "copy", "from": "/biscuits/0", "path": "/best_biscuit" } Copies value from one
     * location to another within the JSON document. Both from and path are JSON Pointers.
     *
     * <p>Move { "op": "move", "from": "/biscuits", "path": "/cookies" } Moves value from location
     * to the other. Both from and path are JSON Pointers.
     *
     * <p>Test { "op": "test", "path": "/best_biscuit/name", "value": "Choco Leibniz" } Tests that the
     * specified value is set in the document. If the test fails, then the patch as a whole should not
     * apply.
     *
     * @param op            The Operationkey
     * @param path          The Path
     * @param updateMessage UpdateMessage
     * @return ResultObject
     */
    public Object executePatch(String op, String path, Entity updateMessage) {
        if (root == null || path == null) {
            return null;
        }
        if (root instanceof Entity) {
            /* Check for Element */
            Entity element = getElement(path, (Entity) root, (Entity) root);
            if (element != null) {
                String key = path;
                int pos = path.lastIndexOf("/");
                if (pos > 0) {
                    key = path.substring(pos + 1);
                }
                if (TYPE_OP_ADD.equalsIgnoreCase(op)) {
                    element.put(key, updateMessage.getValue("value"));
                    return element;
                }
            }
        }
        return null;
    }

    /**
     * Check value.
     *
     * @param value         the value
     * @param key           the key
     * @param oldJsonObject the json obj
     * @return true, if successful
     */
    private boolean checkValue(Object value, String key, Entity oldJsonObject) {
        if (oldJsonObject == null) {
            return false;
        }
        Object oldValue = oldJsonObject.getValue(key);
        if (value != null) {
            if (oldValue instanceof Entity) {
                /* JUST THE TRUTH */
                String oldId = (String) ((Entity) oldValue).getValue(SimpleMap.ID);
                return oldId.equals(map.getId(value, true));
            }
            return value.equals(oldValue);
        }
        return oldValue == null;
    }

    /**
     * Sets the value.
     *
     * @param creator  the creator
     * @param element  the element
     * @param key      the key
     * @param newValue the new value
     * @param typ      type of set NEW, UPDATE, REMOVE
     * @return true, if successful
     */
    private Object setValue(SendableEntityCreator creator, Object element, String key,
                            Object newValue, String typ) {
        if (newValue instanceof Entity) {
            Entity json = (Entity) newValue;
            Object value = map.decode(json);
            if (value != null) {
                creator.setValue(element, key, value, typ);
                if (map.notify(new SimpleEvent(typ, json, map, key, null, value).withModelValue(element))) {
                    return element;
                }
            }
        } else if (creator != null) {
            creator.setValue(element, key, newValue, typ);
            if (map
                    .notify(new SimpleEvent(typ, null, map, key, null, newValue).withModelValue(element))) {
                return element;
            }
        }
        return null;
    }

    /**
     * With filter.
     *
     * @param filter the filter
     * @return the update listener
     */
    public UpdateListener withFilter(Filter filter) {
        updateFilter = filter;
        return this;
    }

    /**
     * Gets the filter.
     *
     * @return the filter
     */
    @Override
    public Filter getFilter() {
        return updateFilter;
    }

    /**
     * Remove the given object from the IdMap.
     *
     * @param oldValue Object to remove
     * @param destroy  switch for remove link from object
     * @return success
     */
    public boolean removeObj(Object oldValue, boolean destroy) {
        if (map != null) {
            return map.removeObj(oldValue, destroy);
        }
        return false;
    }

    /**
     * With condition.
     *
     * @param condition the condition
     * @return the update listener
     */
    public UpdateListener withCondition(ObjectCondition condition) {
        this.condition = condition;
        return this;
    }

    /**
     * Update.
     *
     * @param value the value
     * @return true, if successful
     */
    @Override
    public boolean update(Object value) {
        if (condition == null || !(value instanceof SimpleEvent)) {
            return false;
        }
        SimpleEvent evt = (SimpleEvent) value;
        Object oldValue = evt.getOldValue();
        Object newValue = evt.getNewValue();

        if ((oldValue == null && newValue == null) || (oldValue != null && oldValue.equals(newValue))) {
            /* Nothing to do */
            return false;
        }
        /* put changes into msg and send to receiver */
        Object source = evt.getModelValue();
        String property = evt.getPropertyName();
        SendableEntityCreator creatorClass = map.getCreatorClass(source);
    if (creatorClass == null) {
      /* this class is not supported, do nor replicate */
      return false;
    }
    condition.update(change(property, source, creatorClass, oldValue, newValue));
    return true;
  }

  /**
   * Gets the tokener.
   *
   * @return the tokener
   */
  @Override
  public Tokener getTokener() {
    return factory;
  }
}
